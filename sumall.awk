BEGIN {
	delete sum
	c = 0
	nfields = 0
}

function print_sum() {
	printf("%d", c);
	for (i = 0; i <= nfields; ++i) {
		printf(" %5d", sum[i])
	}
	printf("\n")
}

/^ clients / {
#	print $0
	if ($2 != c) {
		if (nfields > 0) print_sum()
		delete sum
		nfields = 0
		c = $2
	}
	for (i = 5; i <= NF; ++i) {
		sum[i - 5] += $(i)
	}
	if (NF - 5 > nfields) {
		nfields = NF - 5
	}
}

END {
	if (nfields > 0) print_sum()
}

