#!/bin/sh

. ./nodes

for host in ${allhosts[*]}
do
	echo build on $host
	ssh basic@$host "pkill -9 node; cd geoqueue; git pull; cmake .; make; find . -name 'core.*' -print | xargs rm -f"
done

exit 0

