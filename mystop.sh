#!/bin/sh

. ./nodes

echo Stop MariaDB on all hosts.

for host in ${allhosts[*]}
do
	ssh root@$host "systemctl stop mariadb"
#	ssh root@$host "sed -e 's/wsrep_on=.*/wsrep_on=OFF/' -i.tmp /etc/my.cnf.d/server.cnf"
done

exit 0

