#!/bin/sh

allhosts=(ams1 ams2 ams3 ams4 ams5 ams6 ams7)

for host in ${allhosts[*]}
do
	echo Start MariaDB on $host in single mode
	ssh root@$host "sed -e 's/wsrep_on=.*/wsrep_on=OFF/' -i.tmp /etc/my.cnf.d/server.cnf"
	ssh root@$host "systemctl start mariadb"
done

exit 0

