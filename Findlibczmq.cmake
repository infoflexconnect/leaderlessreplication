
if (NOT MSVC)
    include(FindPkgConfig)
    pkg_check_modules(PC_LIBCZMQ "libczmq")
    if (NOT PC_LIBCZMQ_FOUND)
        pkg_check_modules(PC_LIBCZMQ "libczmq")
    endif (NOT PC_LIBCZMQ_FOUND)
    if (PC_LIBCZMQ_FOUND)
        # add CFLAGS from pkg-config file, e.g. draft api.
        add_definitions(${PC_LIBCZMQ_CFLAGS} ${PC_LIBCZMQ_CFLAGS_OTHER})
        # some libraries install the headers is a subdirectory of the include dir
        # returned by pkg-config, so use a wildcard match to improve chances of finding
        # headers and SOs.
        set(PC_LIBCZMQ_INCLUDE_HINTS ${PC_LIBCZMQ_INCLUDE_DIRS} ${PC_LIBCZMQ_INCLUDE_DIRS}/*)
        set(PC_LIBCZMQ_LIBRARY_HINTS ${PC_LIBCZMQ_LIBRARY_DIRS} ${PC_LIBCZMQ_LIBRARY_DIRS}/*)
    endif(PC_LIBCZMQ_FOUND)
endif (NOT MSVC)

find_path (
    LIBCZMQ_INCLUDE_DIRS
    NAMES czmq.h
    HINTS ${PC_LIBCZMQ_INCLUDE_HINTS}
)

find_library (
    LIBCZMQ_LIBRARIES
    NAMES czmq
    HINTS ${PC_LIBCZMQ_LIBRARY_HINTS}
)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(
    LIBCZMQ
    REQUIRED_VARS LIBCZMQ_LIBRARIES LIBCZMQ_INCLUDE_DIRS
)
mark_as_advanced(
    LIBCZMQ_FOUND
    LIBCZMQ_LIBRARIES LIBCZMQ_INCLUDE_DIRS
)

