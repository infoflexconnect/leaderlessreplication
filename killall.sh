#!/bin/sh

# echo killing all nodes

. ./nodes

for host in ${allhosts[*]}
do
#	echo kill node on $host
	ssh -f basic@$host "pkill -9 node; pkill -9 failover"
	ssh -f basic@$host "ps -efa | grep valgrind | grep -v grep | awk '{print \$2}'" > pids
	if [ -s pids ]; then
		cat pids | xargs kill -9
	fi
done
sleep 2

exit 0

