#!/bin/sh

#allhosts=(ams1 ams2 ams3 ams4 ams5 ams6 ams7 sf1 in1 de1 ny1 ca1 uk1)
allhosts=(ams1 ams2 ams3 ams5 sf1 in1 de1 ny1 uk1)

for host in ${allhosts[*]}
do
	echo clear $host
	ssh basic@$host "pkill -9 node; rm -rf */*.ldb; cd leaderlessreplication; make clean ; rm -rf gq.log"
done

exit 0

