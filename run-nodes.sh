#!/bin/sh

messages=-1
timeout=30
between=10
clientlist=(1 2 3 4 5 6 7 8 9 10 20 30 40 50 60 70 80 90 100 200 300 400)
clist=()
minrtt=-1
failures=1
vg=
debug=
#'-q' #'-d' #'-q'

function runGeneral() {
	name=$1
#	echo name $name

	shift
	nodes=("$@")
#	echo nodes ${nodes[*]}
	declare -i nNodes=${#nodes[@]}
#	echo $nNodes nodes

	logname=run-nodes-${name}-${nNodes}.log
	echo logname $logname

	first=${nodes[0]}

	local localClients=${clientlist[*]}
	if [ ${#clist[@]} -gt 0 ]; then
		localClients=${clist[*]}
	fi

	backend=
	if [ $name = "maria" ]; then
		dbhost=${nodes[0]/ams/al}
		backend="--mariadb --host `cat $dbhost` --truncate"
		./mystop.sh
		if [ $nNodes -gt 1 ]; then
			./mycluster.sh ${nodes[@]}
		else
			./mysingle.sh
		fi
	fi
	if [ $name = "galera" ]; then
		dbhost=${nodes[0]}
		backend="--mariadb --host `cat $dbhost` --truncate"
		./mystop.sh
		./mycluster.sh --galera ${nodes[@]}
	fi
	if [ $name = "level" ]; then
		backend="--leveldb"
	fi
	./killall.sh

(for clients in ${localClients[*]}; do
	if [ $name = "maria" ]; then
# MariaDB with 3 nodes can not handle more than 10 clients from each
		if [ $nNodes -gt 2 ]; then
			if [ $clients -gt 10 ]; then
				break
			fi
		fi
	fi

	echo test $clients clients
	common="--failures $failures --minrtt $minrtt -c $clients -n $nNodes -m $messages -t $timeout $backend $debug"
	files_to_remove="gr.log node-$nodeno.ldb"
	initcmd="cd geoqueue; rm -rf $files_to_remove; ulimit -c unlimited"

	declare -i prev=0
	declare -i nodeno=1
	echo Starting the first node: ; date
	while [ $nodeno -lt $nNodes ]; do
		nodehost=${nodes[$prev]}
		host=${nodehost/al/ams}
		nexthost=${nodes[$nodeno]}
		if [ $name = "local" -o $name = "geo" ]; then
			echo start on $host between $nodehost at `cat $nodehost` and $nexthost at `cat $nexthost` #, options: $common
			ssh -f basic@$host "$initcmd ; $vg ./node $common $nodeno `cat $nodehost`:9000 `cat $nexthost`:9000"
		else
			echo start on $host
			ssh -f basic@$host "$initcmd ; $vg ./node $common $nodeno"
		fi
		prev=nodeno
		(( nodeno++ ))
	done
	nodehost=${nodes[$prev]}
	host=${nodehost/al/ams}
	if [ $name = "local" -o $name = "geo" ]; then
		nexthost=${nodes[0]}
		echo start on $host # between $nodehost and $nexthost #, options: $common
		ssh -f basic@$host "$initcmd ; $vg ./node $common $nodeno `cat $nodehost`:9000 `cat $nexthost`:9000"
	else
		echo start on $host
		ssh -f basic@$host "$initcmd ; $vg ./node $common $nodeno"
	fi
	echo The last node has now been started: ; date

	sleep $timeout; sleep $between
	sleep $between # not enough... ; echo try 1; cat $logname | grep '#clients' | grep -v per | awk -f median.awk
	sleep $between; echo try 2; cat $logname | grep '#clients' | grep -v per | awk -f median.awk
	sleep $between; echo try 3; cat $logname | grep '#clients' | grep -v per | awk -f median.awk
	./killall.sh; sleep 1
done) 2>&1 | tee $logname
	cat $logname | grep '#clients' | grep -v per | awk -f median.awk
	if [ $nNodes -gt 1 ]; then
		echo "systemwide"; cat $logname | grep '#clients' | grep -v per | awk -f sums.awk
	fi
	cat $logname | awk -f sumall.awk | grep '^[13]' | tee bwdata-${name}-${nNodes}.log
	for host in ${nodes[*]}
	do
		host=${host/al/ams}
		# ssh -f basic@$host grep medians geoqueue/gr.log
	done
}

level1=(ams1)
declare -i do_level=0

local2=(al1 al2)
local3=(al1 al2 al3)
local4=(al1 al2 al3 al4)
local5=(al1 al2 al3 al4 al5)
local6=(al1 al2 al3 al4 al5 al6)
local7=(al1 al2 al3 al4 al5 al6 al7)

# sf1 ny1 in1 uk1 ca1 sgp1

geo2=(ams1 ny1)
geo3=(ams1 ny1 sf1)
geo4=(ams1 ny1 sf1 in1)
geo5=(ams1 ny1 sf1 in1 sgp1)
geo6=(ams1 uk1 ny1 sf1 in1 sgp1)
geo7=(ams1 uk1 ny1 ca1 sf1 in1 sgp1)

declare -i do_local2=0
declare -i do_local3=0
declare -i do_local4=0
declare -i do_local5=0
declare -i do_local6=0
declare -i do_local7=0

declare -i do_geo2=0
declare -i do_geo3=0
declare -i do_geo4=0
declare -i do_geo5=0
declare -i do_geo6=0
declare -i do_geo7=0

theta3=(ams1 sf1 ny1)
theta5=(ams1 ams2 ams3 sf1 ny1)

declare -i do_theta3=0
declare -i do_theta5=0

maria1=(ams1)
maria3=(ams1 ams2 ams3)
maria5=(ams1 ams2 ams3 ams4 ams5)
maria7=(ams1 ams2 ams3 ams4 ams5 ams6 ams7)

declare -i do_maria1=0
declare -i do_maria3=0
declare -i do_maria5=0
declare -i do_maria7=0

declare -i do_galera3=0
declare -i do_galera5=0
declare -i do_galera7=0

while [ $# -gt 0  ]; do
	[ x$1 = x--help ] && {
		echo "Usage: $0 -c clientlist -t timeout [-failures v] [-minrtt v] -vg [ level | local | local[357] | geo | geo[357] | maria | maria[1357] | galera[357] ]*"
		exit 0
	}
	case $1 in
		-c)
			shift
			clist+=(`echo $1 | tr ',' ' '`)
			;;

		-t)
			shift
			timeout=$1
			;;

		-m)
			shift
			messages=$1
			timeout=10
			;;

		-failures)
			shift
			failures=$1
			;;

		-minrtt)
			shift
			minrtt=$1
			;;

		-vg)
			vg='valgrind --num-callers=30 --trace-children=yes --error-limit=no --leak-check=yes --show-reachable=yes --tool=memcheck --suppressions=zmq.supp'
			debug='-d'
			;;

		-hg)
			vg='valgrind --num-callers=12 --trace-children=yes --error-limit=no --tool=helgrind --gen-suppressions=all --suppressions=zmq.supp -s'
			debug='-d'
			;;

		-v)
			debug='-d'
			;;

		level)
			do_level=1
			;;

		local)
			do_local2=1
			do_local3=1
			do_local4=1
			do_local5=1
			do_local6=1
			do_local7=1
			;;

		local2)
			do_local2=1
			;;
		local3)
			do_local3=1
			;;
		local4)
			do_local4=1
			;;
		local5)
			do_local5=1
			;;
		local6)
			do_local6=1
			;;
		local7)
			do_local7=1
			;;

		geo)
			do_geo2=1
			do_geo3=1
			do_geo4=1
			do_geo5=1
			do_geo6=1
			do_geo7=1
			;;

		geo2)
			do_geo2=1
			;;
		geo3)
			do_geo3=1
			;;
		geo4)
			do_geo4=1
			;;
		geo5)
			do_geo5=1
			;;
		geo6)
			do_geo6=1
			;;
		geo7)
			do_geo7=1
			;;

		theta3)
			do_theta3=1
			;;
		theta5)
			do_theta5=1
			;;

		3)
			do_local3=1
			do_geo3=1
			do_maria3=1
			;;
		5)
			do_local5=1
			do_geo5=1
			do_maria5=1
			;;
		7)
			do_local7=1
			do_geo7=1
			do_maria7=1
			;;

		maria)
			do_maria1=1
			do_maria3=1
			do_maria5=1
			do_maria7=1
			;;
		maria1)
			do_maria1=1
			;;
		maria3)
			do_maria3=1
			;;
		maria5)
			do_maria5=1
			;;
		maria7)
			do_maria7=1
			;;

		galera)
			do_galera3=1
			do_galera5=1
			do_galera7=1
			;;
		galera3)
			do_galera3=1
			;;
		galera5)
			do_galera5=1
			;;
		galera7)
			do_galera7=1
			;;

		*)
			echo unknown: parameter: $1
			exit 1
			;;
	esac
	shift
done

if [ $failures -gt 2 ]; then
	do_local3=0
	do_geo3=0
fi
if [ $failures -gt 1 ]; then
	do_local2=0
	do_geo2=0
fi

if [ $do_level -gt 0 ]; then
	runGeneral level "${level1[@]}"
fi

if [ $do_local2 -gt 0 ]; then
	runGeneral local "${local2[@]}"
fi
if [ $do_local3 -gt 0 ]; then
	runGeneral local "${local3[@]}"
fi
if [ $do_local4 -gt 0 ]; then
	runGeneral local "${local4[@]}"
fi
if [ $do_local5 -gt 0 ]; then
	runGeneral local "${local5[@]}"
fi
if [ $do_local6 -gt 0 ]; then
	runGeneral local "${local6[@]}"
fi
if [ $do_local7 -gt 0 ]; then
	runGeneral local "${local7[@]}"
fi

if [ $do_geo2 -gt 0 ]; then
	runGeneral geo "${geo2[@]}"
fi
if [ $do_geo3 -gt 0 ]; then
	runGeneral geo "${geo3[@]}"
fi
if [ $do_geo4 -gt 0 ]; then
	runGeneral geo "${geo4[@]}"
fi
if [ $do_geo5 -gt 0 ]; then
	runGeneral geo "${geo5[@]}"
fi
if [ $do_geo6 -gt 0 ]; then
	runGeneral geo "${geo6[@]}"
fi
if [ $do_geo7 -gt 0 ]; then
	runGeneral geo "${geo7[@]}"
fi

if [ $do_theta3 -gt 0 ]; then
	runGeneral geo "${theta3[@]}"
fi
if [ $do_theta5 -gt 0 ]; then
	runGeneral geo "${theta5[@]}"
fi

clientlist=(1 2 3 4 5 6 7 8 9 10 20 30 40 50 60 70 80 90)

if [ $do_maria1 -gt 0 ]; then 
	runGeneral maria "${maria1[@]}"
fi

clientlist=(1 2 3 4 5 6 7 8 9)

if [ $do_maria3 -gt 0 ]; then
	runGeneral maria "${maria3[@]}"
fi
if [ $do_maria5 -gt 0 ]; then
	runGeneral maria "${maria5[@]}"
fi
if [ $do_maria7 -gt 0 ]; then
	runGeneral maria "${maria7[@]}"
fi

if [ $do_galera3 -gt 0 ]; then
	runGeneral galera "${geo3[@]}"
fi
if [ $do_galera5 -gt 0 ]; then
	runGeneral galera "${geo5[@]}"
fi
if [ $do_galera7 -gt 0 ]; then
	runGeneral galera "${geo7[@]}"
fi

./mystop.sh

exit 0

