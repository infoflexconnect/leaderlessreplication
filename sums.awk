BEGIN {
	sum=0;
	c=0;
}
{
	if ($2!=c) {
		if (c > 0) {
			printf("%3d %5d\n",c,sum);
		}
		sum=$9;
		c=$2;
	} else {
		sum+=$9;
	}
}
END {
	printf("%3d %5d\n",c,sum);
}

