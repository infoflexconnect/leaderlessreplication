#!/bin/sh

# For this script to work, the following files must contain the IP address of a host reachable by that name.
# ams1 ams1 ams2 ams3 ams4 ams5 ams6 ams7 ca1 in1 ny1 sf1 sgp1 uk1
# For the "ams*" hosts, there must also be corresponding "al*" files, containing the local IP address (at Digital Ocean, those are of the form 10.x.y.z).

# figure 9
./run-nodes.sh -c 3,30,300 -failures 1 local 2>&1 > raw-logs/throughput-local-f1.log
cat raw-logs/throughput-local-f1.log | grep -A3 systemwide

# figure 10
./run-nodes.sh -c 3,30,300 -failures 2 local 2>&1 > raw-logs/throughput-local-f2.log
cat raw-logs/throughput-local-f2.log | grep -A3 systemwide

# figure 11
./run-nodes.sh -c 1,10,100,1000 geo 2>&1 > raw-logs/throughput-geo-f1.log
cat raw-logs/throughput-geo-f1.log | grep -A4 systemwide

# figure 14
./find_rtt.sh 2>&1 | tee raw-logs/rtts.log

# figure 12
./run-nodes.sh -c 1,10,100,1000 -failures 2 geo 2>&1 > raw-logs/throughput-geo-f2.log
cat raw-logs/throughput-geo-f2.log | grep -A4 systemwide

# figure 13
./run-nodes.sh -c 1000 -failures 0 geo 2>&1 > raw-logs/throughput-geo-f0.log
cat raw-logs/throughput-geo-f0.log | grep -A1 systemwide

# figure 15
./run-nodes.sh -minrtt 1 -c 100,300,1000 -failures 1 geo7 2>&1 > raw-logs/throughput-rtt-1-f1.log
cat raw-logs/throughput-rtt-1-f1.log | grep -A3 systemwide

./run-nodes.sh -minrtt 20 -c 100,300,1000 -failures 1 geo7 2>&1 > raw-logs/throughput-rtt-20-f1.log
cat raw-logs/throughput-rtt-20-f1.log | grep -A3 systemwide

./run-nodes.sh -minrtt 100 -c 100,300,1000 -failures 1 geo7 2>&1 > raw-logs/throughput-rtt-100-f1.log
cat raw-logs/throughput-rtt-100-f1.log | grep -A3 systemwide

# figure 16
./run-nodes.sh -minrtt 1 -c 100,300,1000 -failures 2 geo7 2>&1 > raw-logs/throughput-rtt-1-f2.log
cat raw-logs/throughput-rtt-1-f2.log | grep -A3 systemwide

./run-nodes.sh -minrtt 20 -c 100,300,1000 -failures 2 geo7 2>&1 > raw-logs/throughput-rtt-20-f2.log
cat raw-logs/throughput-rtt-20-f2.log | grep -A3 systemwide

./run-nodes.sh -minrtt 100 -c 100,300,1000 -failures 2 geo7 2>&1 > raw-logs/throughput-rtt-100-f2.log
cat raw-logs/throughput-rtt-100-f2.log | grep -A3 systemwide

exit 0

