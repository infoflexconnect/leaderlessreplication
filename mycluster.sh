#!/bin/sh

local=1

while [ $# -gt 0 ]; do
	if [ x$1 == x--galera ]; then
		local=0
		shift
	fi
	if [ x${1:0:2} == x-- ]; then
		echo Unknown parameter to $0: $1
		exit 1
	fi
	break
done

declare -a allhosts=("$@")
declare -i n=${#allhosts[@]}
echo starting cluster with $n nodes

declare -i i=0
if [ $local -gt 0 ]; then
	first=${allhosts[0]/ams/al}
	second=${allhosts[1]/ams/al}
else
	first=${allhosts[0]}
	second=${allhosts[1]}
fi
echo cluster master is $first

while [ $i -lt $n ]; do
	nodehost=${allhosts[$i]}
	if [ $local -gt 0 ]; then
		host=${nodehost/ams/al}
	else
		host=${nodehost}
	fi

	echo "s/wsrep_on=.*/wsrep_on=ON/" > mycnf
	echo "s/wsrep_node_address=.*/wsrep_node_address=gcomm:\\/\\/`cat $host`/" >> mycnf
	echo "s/wsrep_sst_receive_address=.*/wsrep_sst_receive_address=`cat $host`/" >> mycnf
	if [ $i = 0 ]; then
		echo "s/wsrep_cluster_address=.*/wsrep_cluster_address=gcomm:\\/\\/`cat $second`/" >> mycnf
	else
		echo "s/wsrep_cluster_address=.*/wsrep_cluster_address=gcomm:\\/\\/`cat $first`/" >> mycnf
	fi

#	echo Patching MariaDB config on $nodehost
	scp -q mycnf root@$nodehost:/tmp/mycnf.sed
	rm -f mycnf
	ssh root@$nodehost "sed -f /tmp/mycnf.sed -i.tmp /etc/my.cnf.d/server.cnf; rm -f /var/lib/mysql/grastate.dat"
	echo Start MariaDB on $nodehost
#	ssh root@$nodehost "grep '^wsrep' /etc/my.cnf.d/server.cnf"
	if [ $i = 0 ]; then
		ssh root@$nodehost "galera_new_cluster"
	else
		ssh root@$nodehost "systemctl start mariadb"
	fi
	(( i++ ))
done

sleep 5
ssh root@${allhosts[0]} "echo \"SHOW STATUS LIKE 'wsrep_cluster_size%';\" | mysql -uroot"

exit 0

sleep 1
i=0
while [ $i -lt $n ]; do
	nodehost=${allhosts[$i]}
#	ssh root@$nodehost "systemctl status mariadb"
	ssh root@$nodehost "echo \"SHOW STATUS LIKE 'wsrep_cluster%';\" | mysql -uroot"
	(( i++ ))
done

exit 0

