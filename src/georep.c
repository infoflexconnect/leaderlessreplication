
#include "georep.h"
#include "peer.h"
#include "event.h"
#include "log.h"
#include "median.h"
#include "pbuf.h"
#include "net-zeromq.h"

#ifndef SKIP_LEVELDB
#include <leveldb/c.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#define GR_FLAG_NEW_PEER 1
#define GR_FLAG_PLAIN_PN 2
#define GR_FLAG_ORDER_BY_ID 4

struct georep_t {
	int flags;
	uint_least8_t tolerated_failures;
	uint_least32_t high_watermark;
	int32_t min_rtt;
	uint64_t batch_seqno;
	char* address;
	char* nodeid;
	void* loop;
	void* net_data;
	gr_net_t* config;
	gr_net_t default_config[1];

	pthread_mutex_t flags_lock[1];
	pthread_mutex_t seqno_lock[1];

	// TODO: split into "active" and "unactive"?
	pthread_mutex_t peers_lock[1];
	zlistx_t* peers;

	pthread_mutex_t conds_lock[1];
	zhashx_t* conds;

	pthread_mutex_t replica_lock[1];
	zhashx_t* replicas;	// message id => pbuf_t with gr_peer_t (or peer id?)

#ifndef SKIP_LEVELDB
	leveldb_t* ldb;
	leveldb_readoptions_t* readoptions;
	leveldb_writeoptions_t* writeoptions;
#endif

	gr_adopted_cb_t* adopted_cb;
	void* adopted_arg;

	pthread_mutex_t counter_lock[1];
	zhashx_t* counters;	// gr_counter_t
};

static georep_t*
georep_create(const char* address, const char* id)
{
	if (!address) {
		gr_log(GR_LOG_INFO, "%s: address can not be NULL\n", __FUNCTION__);
		return NULL;
	}

	georep_t* queue = malloc(sizeof(*queue));
	*queue = (georep_t) {
		.tolerated_failures = 2,
		.high_watermark = UINT32_MAX,
		.address = strdup(address),
		.min_rtt = -1,
		.batch_seqno = 1,
		.peers = gr_peer_list_create(),
		.conds = zhashx_new(),
		.replicas = zhashx_new(),
		.counters = zhashx_new(),
		.nodeid = id ? strdup(id) : NULL,
	};

#ifndef SKIP_LEVELDB
	if (id) {
		char* errptr = NULL;
		char ldb_filename[256];
		leveldb_options_t* options = leveldb_options_create();
		leveldb_options_set_create_if_missing(options, 1);
		sprintf(ldb_filename, "node-%s.ldb", id);
		queue->ldb = leveldb_open(options, ldb_filename, &errptr);
		leveldb_options_destroy(options);
		if (errptr) {
			fprintf(stderr, "node %s: could not open `%s': %s\n",
				id, ldb_filename, errptr);
			free(errptr);
			free(queue);
			return NULL;
		}
		queue->readoptions = leveldb_readoptions_create();
		queue->writeoptions = leveldb_writeoptions_create();
	}
#endif

	pthread_mutex_init(queue->flags_lock, NULL);
	pthread_mutex_init(queue->seqno_lock, NULL);
	pthread_mutex_init(queue->peers_lock, NULL);
	pthread_mutex_init(queue->conds_lock, NULL);
	zhashx_set_destructor(queue->replicas, pbuf_free_p);
	pthread_mutex_init(queue->replica_lock, NULL);
	pthread_mutex_init(queue->counter_lock, NULL);
	zhashx_set_destructor(queue->counters, gr_counter_free);

	// TODO: load any records from the previous execution

	return queue;
}

static void
georep_destroy(georep_t** queuep)
{
	if (!queuep)
		return;
	georep_t* obj = *queuep;
	if (!obj)
		return;
	*queuep = NULL;
#if 1
	gr_log(GR_LOG_INFO, "%s: queue at %p sizeof(replicas) = %ld\n",
		__FUNCTION__, obj, zhashx_size(obj->replicas));
#endif

	if (obj->config && obj->config->net_close)
		obj->config->net_close(&obj->net_data);

	gr_log(GR_LOG_INFO, "%s: queue at %p deletes locks\n",
		__FUNCTION__, obj);

#ifndef SKIP_LEVELDB
	if (obj->ldb)
		leveldb_close(obj->ldb);
	if (obj->readoptions)
		leveldb_readoptions_destroy(obj->readoptions);
	if (obj->writeoptions)
		leveldb_writeoptions_destroy(obj->writeoptions);
#endif

	pthread_mutex_destroy(obj->flags_lock);
	pthread_mutex_destroy(obj->seqno_lock);

	pthread_mutex_lock(obj->peers_lock);
	zlistx_destroy(&obj->peers);
	pthread_mutex_unlock(obj->peers_lock);
	pthread_mutex_destroy(obj->peers_lock);

	pthread_mutex_lock(obj->conds_lock);
	zhashx_destroy(&obj->conds);
	pthread_mutex_unlock(obj->conds_lock);
	pthread_mutex_destroy(obj->conds_lock);

	pthread_mutex_lock(obj->replica_lock);

	// TODO: explicit delete of the values
	zhashx_destroy(&obj->replicas);

	pthread_mutex_unlock(obj->replica_lock);
	pthread_mutex_destroy(obj->replica_lock);

	pthread_mutex_lock(obj->counter_lock);
	zhashx_destroy(&obj->counters);
	pthread_mutex_unlock(obj->counter_lock);
	pthread_mutex_destroy(obj->counter_lock);

	free(obj->address);
	free(obj->nodeid);
	free(obj);
}

const char*
gr_my_id(georep_t* queue)
{
	return queue ? queue->nodeid : NULL;
}

void
gr_tolerate_failures(georep_t* queue, uint_least8_t f)
{
	queue->tolerated_failures = f;
}

void
gr_set_high_watermark(georep_t* queue, uint_least8_t n)
{
	queue->high_watermark = n > 0 ? n : 1;
}

uint_least8_t
gr_high_watermark(georep_t* queue)
{
	return queue->high_watermark;
}

void
gr_set_min_rtt(georep_t* queue, int32_t n)
{
	queue->min_rtt = n * 1000;
}

static void
gr_set_flag(georep_t* queue, int flag, uint_least8_t value)
{
	if (!queue)
		return;
	pthread_mutex_lock(queue->flags_lock);
	if (value)
		queue->flags |= flag;
	else
		queue->flags &= ~flag;
	pthread_mutex_unlock(queue->flags_lock);
}

static int
gr_get_flag(georep_t* queue, int flag)
{
	if (!queue)
		return 0;
	pthread_mutex_lock(queue->flags_lock);
	int value = queue->flags & flag;
	pthread_mutex_unlock(queue->flags_lock);
	return value;
}

static int
gr_get_and_set_flag(georep_t* queue, int flag, int value)
{
	if (!queue)
		return 0;
	pthread_mutex_lock(queue->flags_lock);
	int old_value = queue->flags & flag;
	if (value)
		queue->flags |= flag;
	else
		queue->flags &= ~flag;
	pthread_mutex_unlock(queue->flags_lock);
	return old_value;
}

void
gr_replicate_by_id(georep_t* queue)
{
	gr_set_flag(queue, GR_FLAG_ORDER_BY_ID, 1);
}

void
gr_use_plain_pn(
	georep_t* queue,
	uint_least8_t flag)
{
	gr_set_flag(queue, GR_FLAG_PLAIN_PN, flag);
}

uint_least8_t
gr_using_plain_pn(georep_t* queue)
{
	return gr_get_flag(queue, GR_FLAG_PLAIN_PN) != 0;
}

uint64_t
gr_next_seqno(georep_t* queue)
{
	if (!queue) return -1;
	pthread_mutex_lock(queue->seqno_lock);
	uint64_t v = queue->batch_seqno++;
	pthread_mutex_unlock(queue->seqno_lock);
	return v;
}

static gr_peer_t*
gr_find_or_create_peer(georep_t* queue,
	const char* address, const char* id,
	int got_data)
{
	if (!queue || !address)
		return NULL;

	gr_peer_t* peer = gr_peer_by_address(queue->peers, address);

	if (peer) {
		gr_peer_set_id(peer, id);
	} else {
		peer = gr_peer_create_with_id(queue, address, id);
		if (!peer) {
			gr_log(GR_LOG_INFO,
				"%s: queue at `%s' can not create peer at `%s'\n",
				__FUNCTION__, queue->address, address);
			return NULL;
		}
		zlistx_add_end(queue->peers, peer);
		gr_set_flag(queue, GR_FLAG_NEW_PEER, 1);
		gr_log(GR_LOG_INFO, "%s: queue at `%s' adds new peer at `%s'\n",
			__FUNCTION__, queue->address, address);
	}

	if (got_data) {
		int became_active = 0;
		gr_peer_got_data(peer, &became_active, queue->peers);
		if (became_active) {
			gr_set_flag(queue, GR_FLAG_NEW_PEER, 1);
			gr_log(GR_LOG_INFO, "%s: queue at `%s' has confirmed peer at `%s'\n",
				__FUNCTION__, queue->address, address);
		}
	}

	return peer;
}

static gr_counter_t*
gr_lookup_or_create_counter(georep_t* queue, const char* name)
{
	gr_counter_t* counter = zhashx_lookup(queue->counters, name);
	if (!counter) {
		gr_log(GR_LOG_DEBUG, "%s: queue at `%s' creates replicated counter `%s'\n",
			__FUNCTION__, queue->address, name);
		counter = gr_counter_create();
		zhashx_insert(queue->counters, name, counter);
	}
	return counter;
}

static void
gr_merge_counters(georep_t* queue, zhashx_t* counters, gr_peer_t* peer)
{
	if (!counters)
		return;
	const char* nodeid = gr_peer_get_id(peer);
	gr_counter_t* cinfo = zhashx_first(counters);
	pthread_mutex_lock(queue->counter_lock);
	while (cinfo) {
		const char* name = zhashx_cursor(counters);
		gr_log(GR_LOG_DEBUG,
			"%s: queue at `%s' merges info about counter `%s' from node `%s'\n",
			__FUNCTION__, queue->address, name, nodeid);
		gr_counter_t* counter = gr_lookup_or_create_counter(queue, name);
		gr_counter_merge(counter, cinfo, peer, queue->nodeid);
		cinfo = zhashx_next(counters);
	}
	pthread_mutex_unlock(queue->counter_lock);
}

static void
georep_handle_event(georep_t* queue, gr_event_t* event)
{
	size_t nobjects;
	long npeers;
	gr_peer_t* peer;

	if (!queue || !event)
		return;

	pthread_mutex_lock(queue->peers_lock);
	peer = gr_find_or_create_peer(queue, event->address, event->id, 1);
	gr_peer_update_theta(peer, queue->peers);
	pthread_mutex_unlock(queue->peers_lock);

	switch (event->cmd) {
		case GR_CMD_HELLO:
			npeers = event->peers ? zlistx_size(event->peers) : 0;
			gr_log(GR_LOG_INFO,
				"%s: queue at `%s' got a hello from a peer at `%s' with id `%s', with %ld extra peer%s\n",
				__FUNCTION__, queue->address, event->address, event->id,
				npeers, npeers == 1 ? "" : "s");

			// update peer list
			peer = event->peers ? zlistx_first(event->peers) : NULL;
			pthread_mutex_lock(queue->peers_lock);
			while (peer) {
				const char* address = gr_peer_get_address(peer);
				if (address && (strcmp(queue->address, address) == 0)) {
					gr_log(GR_LOG_INFO, "%s: good, peer `%s' knows about us (`%s')\n",
						__FUNCTION__, event->address, address);
				} else {
					(void) gr_find_or_create_peer(queue,
						gr_peer_get_address(peer), gr_peer_get_id(peer), 0);
				}
				peer = zlistx_next(event->peers);
			}
			pthread_mutex_unlock(queue->peers_lock);
			break;

		case GR_CMD_HEARTBEAT:
			nobjects = event->items ? zlistx_size(event->items) : 0;
			if (nobjects > 0) {
				gr_log(GR_LOG_INFO, "%s: got %ld objects to store as batch %d\n",
					__FUNCTION__, nobjects, event->batch_id);
				gr_peer_save_items(peer, event->batch_id, event->items);
			}
			gr_peer_confirmed_batches(peer, event->batch_ids);
			break;

		case GR_CMD_COUNTER:
			nobjects = event->counters ? zhashx_size(event->counters) : 0;
			if (nobjects > 0) {
				gr_log(GR_LOG_INFO, "%s: got information about %ld counters\n",
					__FUNCTION__, nobjects);
				gr_merge_counters(queue, event->counters, peer);
			}
			break;

		default:
			;
	}
#if 0
	// TODO: this needs to be in the gr_net_t api
	struct timeval now;
	gettimeofday(&now, NULL);
	gr_peer_update(peer, &now, queue->flags & GR_FLAG_NEW_PEER, -1, NULL);
#endif
}

int
gr_handle_incoming(georep_t* queue, const byte** cpp, int* sp)
{
	int csize = sp ? *sp : 0;
	const byte* cdata = cpp ? *cpp : NULL;

	gr_event_t* event = gr_event_parse(&cdata, &csize);
	if (cpp)
		*cpp = cdata;
	if (sp)
		*sp = csize;

	if (!event) {
		// TODO: close socket, we can not continue reading anyway
		return -1;
	}
	georep_handle_event(queue, event);
	gr_event_free(&event);
	return 0;
}

void
gr_unreachable_peer(georep_t* queue, gr_peer_t* peer)
{
	const char* key;
	zhashx_t* items;
	gr_peer_object_info_t* info;

	gr_log(GR_LOG_INFO, "%s: queue at `%s' thinks peer at `%s' is dead\n",
		__FUNCTION__, queue->address, gr_peer_get_address(peer));

	if (!queue->adopted_cb)
		return;

	// possible dead, wait for majority, etc...

	// TODO: move this loop to peer.c? of course, it's not supposed to exist anyway
	//
	items = gr_peer_get_items(peer);
	info = zhashx_first(items);
	while (info) {
		key = zhashx_cursor(items);
		gr_log(GR_LOG_INFO, "%s: queue at `%s' adopts `%s' from peer `%s'\n",
			__FUNCTION__, queue->address, key, gr_peer_get_address(peer));
		queue->adopted_cb(queue->adopted_arg, key, info->value);
		info = zhashx_next(items);
	}
}

int
georep_sync(georep_t* queue)
{
	struct timeval now;
	gr_peer_t* peer;
	pbuf_t peers[1];	// contains gr_peer_t*

	gettimeofday(&now, NULL);
	pthread_mutex_lock(queue->peers_lock);
	if (!queue->peers) {
		pthread_mutex_unlock(queue->peers_lock);
		return 0;
	}
	size_t npeers = zlistx_size(queue->peers);
	int nactive = 0;
	pbuf_init(peers, npeers, 0, PBUF_NONE);
	peer = zlistx_first(queue->peers);
	while (peer) {
		pbuf_append(peers, peer);
		gr_peer_increment_if_active(peer, &nactive);
		peer = zlistx_next(queue->peers);
	}
#if 1
	gr_log(GR_LOG_INFO,
		"%s: enter for queue at `%s' having a total of %ld peers, %d being active\n",
		__FUNCTION__,
		queue->address,
		npeers,
		nactive);
#endif
	pthread_mutex_unlock(queue->peers_lock);
	int new_peer_flag = gr_get_and_set_flag(queue, GR_FLAG_NEW_PEER, 0);
	pthread_mutex_lock(queue->counter_lock);
	PBUF_FOREACH(peer, peers)
		// TODO: broadcast all new prospects?
		gr_peer_update(peer, &now, new_peer_flag, nactive, queue->counters);
	PBUF_FOREACH_END
	pthread_mutex_unlock(queue->counter_lock);

	pbuf_destroy(peers);
	return 0;
}

georep_t*
gr_init2(const char* address, const char* nodeid, gr_net_t* queueconfig)
{
	georep_t* queue = georep_create(address, nodeid);
	if (!queue)
		return NULL;

	if (!queueconfig) {
		gr_zeromq_setup(queue->default_config);
		queueconfig = queue->default_config;
	}
	queue->config = queueconfig;

	void* p = NULL;
	if (queueconfig->net_init)
		p = queueconfig->net_init(queue);
	if (!p) {
		georep_destroy(&queue);
		return NULL;
	}
	queue->net_data = p;

	return queue;
}

georep_t*
gr_init(const char* address, const char* nodeid)
{
	return gr_init2(address, nodeid, NULL);
}

void
gr_shutdown(georep_t** queuep)
{
	georep_destroy(queuep);
}

const char*
gr_get_address(georep_t* queue)
{
	return queue ? queue->address : NULL;
}

int
gr_contact(georep_t* queue, const char* address, int* clustersize)
{
	if (address) {
		gr_log(GR_LOG_INFO, "%s: node at `%s' contacts node at `%s'\n",
			__FUNCTION__, queue->address, address);
		pthread_mutex_lock(queue->peers_lock);
		(void) gr_find_or_create_peer(queue, address, NULL, 0);
		pthread_mutex_unlock(queue->peers_lock);
	}

	if (clustersize) {
		int nactive = 0;

		pthread_mutex_lock(queue->peers_lock);
		gr_peer_t* peer = zlistx_first(queue->peers);
		while (peer) {
			gr_peer_increment_if_active(peer, &nactive);
			peer = zlistx_next(queue->peers);
		}
		pthread_mutex_unlock(queue->peers_lock);

#if 1
		gr_log(GR_LOG_INFO, "%s: queue at `%s' has %d active peer%s\n",
			__FUNCTION__, queue->address, nactive, nactive == 1 ? "" : "s");
#endif
		*clustersize = nactive + 1;
	}

	return 0;
}

void
georep_append_event_params(georep_t* queue, zchunk_t* chunk, gr_cmd_t cmd)
{
	pbuf_t active[1];

	gr_event_append_string(chunk, queue->address, -1);
	switch (cmd) {
		case GR_CMD_HELLO:
			gr_event_append_string(chunk, queue->nodeid, -1);
			pbuf_init(active, 10, 0, PBUF_NONE);
			pthread_mutex_lock(queue->peers_lock);
			gr_peer_only_active(queue->peers, active, NULL, NULL);
			pthread_mutex_unlock(queue->peers_lock);
			gr_event_append_pbuf(chunk, active, gr_peer_serialize);
			pbuf_destroy(active);
			break;
		case GR_CMD_HEARTBEAT:
			break;
		case GR_CMD_COUNTER:
			break;
	}
}

void
gr_set_adopted_cb(georep_t* queue, gr_adopted_cb_t* func, void* arg)
{
	queue->adopted_cb = func;
	queue->adopted_arg = arg;
}

typedef struct {
	int32_t tm;
	gr_peer_t* peer;
} gr_peer_with_median_t;

static int
cmp_tm(const void* obj1, const void* obj2)
{
	const gr_peer_with_median_t* p1 = obj1;
	const gr_peer_with_median_t* p2 = obj2;
#if 0
	gr_log(GR_LOG_DEBUG, "%s: p1 at %p has peer at %p with median %d\n",
		__FUNCTION__, p1, p1->peer, p1->tm);
	gr_log(GR_LOG_DEBUG, "%s: p2 at %p has peer at %p with median %d\n",
		__FUNCTION__, p2, p2->peer, p2->tm);
#endif
	return (int)p1->tm - (int)p2->tm;
}

static int
cmp_id(const void* obj1, const void* obj2)
{
	const gr_peer_with_median_t* p1 = obj1;
	const gr_peer_with_median_t* p2 = obj2;
	return strcmp(gr_peer_get_id(p1->peer), gr_peer_get_id(p2->peer));
}

static void
gr_find_confirmed_peers(
	georep_t* queue,
	pbuf_t* peers,
	size_t f)
{
	if (f < 1)
		return;

	pbuf_t active[1];

	pbuf_init(active, 10, 0, PBUF_NONE);
	pthread_mutex_lock(queue->peers_lock);
	size_t npeers = gr_peer_only_active(queue->peers, active, NULL, NULL);
	pthread_mutex_unlock(queue->peers_lock);
	gr_peer_with_median_t peers_with_median[npeers];
	if (npeers < f) {
		gr_log(GR_LOG_INFO,
			"%s: queue at `%s' has only %ld confirmed peer%s of the required %d\n",
			__FUNCTION__, queue->address, npeers, npeers == 1 ? "" : "s",
			queue->tolerated_failures);
		goto done;
	}

	int pno = 0;
	gr_peer_t* peer;
#if 0
	gr_log(GR_LOG_DEBUG, "%s: found %ld active peer%s\n",
		__FUNCTION__, npeers, npeers == 1 ? "" : "s");
#endif
	PBUF_FOREACH(peer, active)
		const char* id = gr_peer_get_id(peer);
		if (!id) {
#if 0
			gr_log(GR_LOG_DEBUG, "%s: peer at `%s' has no id?\n",
				__FUNCTION__, gr_peer_get_address(peer));
#endif
			continue;
		}
#if 0
		int level = 0;
		int tm = gr_peer_get_median(peer, &level);
		gr_log(GR_LOG_DEBUG, "%s: peer at `%s' median %d at level %d\n",
			__FUNCTION__, gr_peer_get_address(peer), tm, level);
#else
		int tm = gr_peer_get_min_rtt(peer);
#endif
		peers_with_median[pno].peer = peer;
		peers_with_median[pno].tm = tm;
		pno++;
	PBUF_FOREACH_END
#if 0
	gr_log(GR_LOG_DEBUG, "%s: found %d peer%s with an id\n",
		__FUNCTION__, pno, pno == 1 ? "" : "s");
#endif
	if (pno < 1)
		goto done;

	int i;
	int next = -1;
	if (gr_get_flag(queue, GR_FLAG_ORDER_BY_ID)) {
		qsort(peers_with_median, pno, sizeof(peers_with_median[0]), cmp_id);
	} else if (queue->min_rtt < 0) {
		next = random() % pno;
	} else {
		qsort(peers_with_median, pno, sizeof(peers_with_median[0]), cmp_tm);
		for (i = 0; i < pno; ++i) {
#if 1
			gr_log(GR_LOG_DEBUG, "%s: peer %d at `%s' with median %d\n",
				__FUNCTION__, i,
				gr_peer_get_address(peers_with_median[i].peer),
				peers_with_median[i].tm);
#endif
			if ((next < 0) && (peers_with_median[i].tm >= queue->min_rtt))
				next = i;
		}
		// If all of them are too fast, use the ones as far away as possible.
		if ((next < 0) && (queue->min_rtt > 0))
			next = pno;

		// Avoid RTT=0
		if (peers_with_median[0].tm == 0)
			next = 0;
	}

#if 0
	gr_log(GR_LOG_DEBUG, "%s: start at index %d\n", __FUNCTION__, next);
#endif
	if (next > -1) {
		for (i = next; i < pno; ++i) {
			peer = peers_with_median[i].peer;
			pbuf_append(peers, peer);
			if (pbuf_size(peers) >= f)
				goto done;
		}
		for (i = next - 1; i >= 0; --i) {
			peer = peers_with_median[i].peer;
			pbuf_append(peers, peer);
			if (pbuf_size(peers) >= f)
				goto done;
		}
	} else {
		for (i = 0; i < pno; ++i) {
			peer = peers_with_median[i].peer;
			pbuf_append(peers, peer);
			if (pbuf_size(peers) >= f)
				goto done;
		}
	}

done:
	pbuf_destroy(active);
}

static void
gr_store_locally(
	georep_t* queue,
	const char* key,
	gr_peer_object_state_t state,
	const char* value,
	int value_len,
	pbuf_t* other_nodes)
{
	if (state == GR_OBJECT_STATE_TERMINATED) {
		gr_persist(queue, key, -1, NULL, -1, 1);
		return;
	}

	gr_peer_object_info_t info = {
		.state = state,
		.value = value ? zchunk_new(value, value_len) : NULL,
		.other_nodes = { *other_nodes },
	};
	zchunk_t* chunk = zchunk_new(NULL, 256);
	gr_peer_object_info_serialize(&info, key, chunk);
	gr_persist(queue, key, -1, (const char *) zchunk_data(chunk), zchunk_size(chunk), 1);
	zchunk_destroy(&chunk);
	zchunk_destroy(&info.value);
}

gr_cond_t*
gr_replicate(georep_t* queue, const char* key, const char* value, int value_len)
{
	if (!queue || !key || !value)
		return NULL;

	if (value_len < 0)
		value_len = strlen(value);

	gr_log(GR_LOG_DEBUG, "%s: queue at `%s' stores key `%s' value `%.*s'\n",
		__FUNCTION__, queue->address, key, value_len, value);

	size_t f = queue->tolerated_failures;

	pbuf_t peers[1];
	pbuf_init(peers, f, 0, PBUF_NONE);

	gr_cond_t* cond = NULL;

	gr_find_confirmed_peers(queue, peers, f);
	if (pbuf_size(peers) < f) {
		gr_log(GR_LOG_INFO, "%s: queue at `%s' can not replicate key `%s', too few active peers\n",
			__FUNCTION__, queue->address, key);
		goto done;
	}

	gr_peer_t* peer;
	pbuf_t* other_nodes = pbuf_create(f, 0, NULL);
	PBUF_FOREACH(peer, peers)
		const char* id = gr_peer_get_id(peer);
		pbuf_append(other_nodes, strdup(id));
	PBUF_FOREACH_END

	gr_store_locally(queue, key, GR_OBJECT_STATE_NEW, value, value_len, other_nodes);
	if (f < 1) {
		// No replication, just local storage.
		goto done;
	}

	cond = gr_cond_create();
	gr_cond_lock(cond, f);
	gr_cond_unlock(cond);

	pthread_mutex_lock(queue->conds_lock);
	zhashx_insert(queue->conds, key, cond);
	pthread_mutex_unlock(queue->conds_lock);
	gr_log(GR_LOG_INFO, "%s: queue at `%s' replicates key `%s' to %ld peers with cond at %p\n",
		__FUNCTION__, queue->address, key, f, cond);

	pthread_mutex_lock(queue->replica_lock);
	zhashx_insert(queue->replicas, key, other_nodes);
	pthread_mutex_unlock(queue->replica_lock);

	struct timeval now;
	gettimeofday(&now, NULL);
	PBUF_FOREACH(peer, peers)
		gr_peer_replicate(peer, cond, key, GR_OBJECT_STATE_NEW, &now,
			value, value_len, other_nodes);
	PBUF_FOREACH_END

	// Maybe not for every data tuple?
	georep_sync(queue);

done:
	pbuf_destroy(peers);
	return cond;
}

gr_peer_t*
gr_by_id(georep_t* queue, const char* id)
{
	pthread_mutex_lock(queue->peers_lock);
	gr_peer_t* peer = gr_peer_by_id(queue->peers, id);
	pthread_mutex_unlock(queue->peers_lock);
	return peer;
}

static gr_cond_t*
gr_update_peers(
	georep_t* queue,
	const char* key,
	gr_peer_object_state_t state,
	const char* value, int value_len)
{
	pthread_mutex_lock(queue->replica_lock);
	pbuf_t* ids = zhashx_lookup(queue->replicas, key);
	const size_t ids_size = pbuf_size(ids);
	pthread_mutex_unlock(queue->replica_lock);
	gr_log(GR_LOG_DEBUG, "%s: queue at `%s' previously sent `%s' to %ld other nodes\n",
		__FUNCTION__, queue->address, key, ids_size);

	const char* id;
	gr_peer_t* peer;
	pbuf_t peers[1];
	pbuf_init(peers, queue->tolerated_failures, 0, PBUF_NONE);
	pthread_mutex_lock(queue->peers_lock);
	PBUF_FOREACH(id, ids)
		peer = gr_peer_by_id(queue->peers, id);
		if (peer)
			pbuf_append(peers, peer);
	PBUF_FOREACH_END
	pthread_mutex_unlock(queue->peers_lock);

	pthread_mutex_lock(queue->replica_lock);
	if (state == GR_OBJECT_STATE_TERMINATED)
		zhashx_delete(queue->replicas, key);
	pthread_mutex_unlock(queue->replica_lock);

	gr_store_locally(queue, key, state, value, value_len, ids);

	gr_cond_t* cond = NULL;
	size_t npeers = pbuf_size(peers);
	if (npeers < 1) {
		gr_log(GR_LOG_INFO, "%s: queue at `%s' has no peers for key `%s'\n",
			__FUNCTION__, queue->address, key);
		goto done;
	}

	cond = gr_cond_create();
	pthread_mutex_lock(queue->conds_lock);
	gr_cond_lock(cond, npeers);
	gr_cond_unlock(cond);
	gr_cond_t* old_cond = zhashx_lookup(queue->conds, key);
	if (old_cond) {
		// Can this actually happen?
		// Under which circumstances?
		// If so, what should happen now?
		gr_log(GR_LOG_INFO, "%s: queue at `%s' already has a cond at %p for key `%s'\n",
			__FUNCTION__, queue->address, cond, key);
	} else {
		zhashx_insert(queue->conds, key, cond);
	}
	pthread_mutex_unlock(queue->conds_lock);

	gr_log(GR_LOG_INFO,
		"%s: queue at `%s' removes item with key `%s' from %ld peers with cond at %p\n",
		__FUNCTION__, queue->address, key, pbuf_size(peers), cond);
	struct timeval now;
	gettimeofday(&now, NULL);
	PBUF_FOREACH(peer, peers)
		gr_peer_replicate(peer, cond, key, state, &now, value, value_len, NULL);
	PBUF_FOREACH_END

done:
	pbuf_destroy(peers);
	return cond;
}

gr_cond_t*
gr_update(georep_t* queue, const char* key, const char* value, int value_len)
{
	if (!queue || !key || !value)
		return NULL;

	if (value_len < 0)
		value_len = strlen(value);
	gr_log(GR_LOG_DEBUG, "%s: queue at `%s' updates key `%s' value `%.*s'\n",
		__FUNCTION__, queue->address, key, value_len, value);

	return gr_update_peers(queue, key, GR_OBJECT_STATE_UPDATED, value, value_len);
}

gr_cond_t*
gr_clear(georep_t* queue, const char* key)
{
	if (!queue || !key)
		return NULL;

	return gr_update_peers(queue, key, GR_OBJECT_STATE_TERMINATED, NULL, 0);
}

void
gr_signal_keys(georep_t* queue, pbuf_t* keys)
{
	const char* key;

	gr_log(GR_LOG_DEBUG, "%s: queue at `%s' got confirmation for %ld keys\n",
		__FUNCTION__, queue->address, pbuf_size(keys));
	pthread_mutex_lock(queue->conds_lock);
	PBUF_FOREACH(key, keys)
		gr_cond_t* cond = zhashx_lookup(queue->conds, key);
		if (!cond)
			continue;
		if (gr_cond_signal(cond)) {
			gr_log(GR_LOG_DEBUG, "%s: queue at `%s' signals cond at %p\n",
				__FUNCTION__, gr_get_address(queue), cond);
			zhashx_delete(queue->conds, key);
			pbuf_putat(keys, pbuf_i, NULL);	// will also free 'key'
		}
	PBUF_FOREACH_END
	pthread_mutex_unlock(queue->conds_lock);
}

void
gr_adopt(georep_t* queue, gr_peer_object_info_t** infop)
{
	gr_peer_object_info_t* info = infop ? *infop : NULL;
	if (!info)
		return;

	gr_log(GR_LOG_INFO, "%s: queue at `%s' adopts object `%s'\n",
		__FUNCTION__, queue->address, info->key);
	//zhashx_insert(queue->replicas, key, pbuf_dup(info->other_nodes));
	if (queue->adopted_cb) {
		queue->adopted_cb(queue->adopted_arg, info->key, info->value);
	}
	gr_peer_object_info_free_p(infop);
}

void
gr_persist(georep_t* queue, const char* key, int keylen, const char* value, int valuelen, int create_if_new)
{
#ifndef SKIP_LEVELDB
	int klen = keylen;
	int vlen = valuelen;
	char* errptr = NULL;

	if (key && (klen < 0))
		klen = strlen(key);
	if (klen < 0)
		return;

	if (value) {
		if (vlen < 0)
			vlen = strlen(value);
	} else {
		vlen = -1;
	}

	if (vlen > 0) {
		if (!create_if_new) {
			size_t current_value_len = 0;
			char* current_value = leveldb_get(queue->ldb, queue->readoptions,
				key, klen, &current_value_len, &errptr);
			if (errptr) {
				fprintf(stderr, "node %s: could not find key `%s': %s\n",
					queue->nodeid, key, errptr);
				free(errptr);
			}
			if (current_value) {
				free(current_value);
			} else {
				return;
			}
		}
		gr_log(GR_LOG_INFO, "%s: queue at `%s' stores key `%.*s' value `%.*s'\n",
				__FUNCTION__, queue->address, klen, key, vlen, value);
		leveldb_put(queue->ldb, queue->writeoptions, key, klen, value, vlen, &errptr);
	} else {
		gr_log(GR_LOG_INFO, "%s: queue at `%s' deletes key `%.*s'\n",
			__FUNCTION__, queue->address, klen, key);
		leveldb_delete(queue->ldb, queue->writeoptions, key, klen, &errptr);
	}
	if (errptr) {
		fprintf(stderr, "node %s: could not save/update/delete key `%s': %s\n",
			queue->nodeid, key, errptr);
		free(errptr);
	}
#else
	(void) queue;
	(void) key;
	(void) keylen;
	(void) value;
	(void) valuelen;
	(void) create_if_new;
#endif
}

gr_counter_t*
gr_create_counter(georep_t* queue, const char* name)
{
	if (!queue)
		return NULL;

	gr_log(GR_LOG_INFO, "%s: queue at `%s' creates counter `%s'\n",
		__FUNCTION__, queue->address, name);
	gr_counter_t* counter = gr_counter_create();
	pthread_mutex_lock(queue->counter_lock);
	zhashx_insert(queue->counters, name, counter);
	pthread_mutex_unlock(queue->counter_lock);
	return counter;
}

static gr_counter_t*
gr_get_counter(georep_t* queue, const char* name)
{
	pthread_mutex_lock(queue->counter_lock);
	gr_counter_t* counter = zhashx_lookup(queue->counters, name);
	if (!counter) {
		counter = gr_counter_create();
		zhashx_insert(queue->counters, name, counter);
	}
	pthread_mutex_unlock(queue->counter_lock);
	return counter;
}

void
gr_set_counter(
	georep_t* queue,
	const char* name,
	const char* key,
	uint64_t value_p,
	uint64_t value_n)
{
	if (!queue || !name || !key)
		return;

	gr_counter_t* counter = gr_get_counter(queue, name);
	gr_counter_set(counter, key, queue->nodeid, value_p, value_n);
}

void
gr_delta_counter(georep_t* queue, const char* name, const char* key, int64_t diff)
{
	if (!queue || !name || !key || !diff)
		return;

	gr_counter_t* counter = gr_get_counter(queue, name);
	gr_counter_delta(counter, key, queue->nodeid, diff);
}

int64_t
gr_get_counter_value(georep_t* queue, const char* name, const char* key)
{
	if (!queue || !name || !key)
		return 0;

	gr_counter_t* counter = gr_get_counter(queue, name);
	if (!counter)
		return 0;
	return gr_counter_value(counter, key);
}

const gr_net_t*
gr_net_config(georep_t* queue)
{
	return queue ? queue->config : NULL;
}

