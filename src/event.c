
#include "event.h"
#include "peer.h"
#include "counter.h"
#include "log.h"

#include <netinet/in.h>

void
gr_event_append_byte(zchunk_t* chunk, byte value)
{
	zchunk_extend(chunk, &value, 1);
}

void
gr_event_append_int(zchunk_t* chunk, int value)
{
	uint32_t neutral_value = htonl(value);
	zchunk_extend(chunk, &neutral_value, 4);
}

void
gr_event_append_int64(zchunk_t* chunk, int64_t value)
{
#ifdef OS_Linux
	uint64_t neutral_value = htobe64(value);
#else
	uint64_t neutral_value = htonll(value);
#endif
	zchunk_extend(chunk, &neutral_value, 8);
}

void
gr_event_append_string(zchunk_t* chunk, const char* str, int len)
{
	if (len < 0)
		len = str ? strlen(str) : 0;
	gr_event_append_int(chunk, len);
	if (len > 0)
		zchunk_extend(chunk, str, len);
}

void
gr_event_append_zchunk(zchunk_t* chunk, zchunk_t* data)
{
	int len = data ? zchunk_size(data) : 0;
	gr_event_append_int(chunk, len);
	if (len > 0)
		zchunk_extend(chunk, zchunk_data(data), len);
}

void
gr_event_append_list(zchunk_t* chunk, zlistx_t* items,
	gr_event_append_element_t* append_element)
{
	int n = items ? zlistx_size(items) : 0;
	const void* element;

	gr_event_append_int(chunk, n);
	element = items ? zlistx_first(items) : NULL;
	while (element) {
		append_element(element, chunk);
		element = zlistx_next(items);
	}
}

void
gr_event_append_pbuf(
	zchunk_t* chunk,
	pbuf_t* items,
	gr_event_append_element_t* append_element)
{
	int n = pbuf_size(items);
	gr_event_append_int(chunk, n);

	const void* element;
	PBUF_FOREACH(element, items);
		append_element(element, chunk);
	PBUF_FOREACH_END
}

zchunk_t*
gr_event_create_chunk(georep_t* queue, byte b)
{
	zchunk_t* chunk = zchunk_new(NULL, 256);
	zchunk_extend(chunk, &b, 1);
	georep_append_event_params(queue, chunk, b);
	return chunk;
}

zchunk_t*
gr_event_create_hello(georep_t* queue)
{
	return gr_event_create_chunk(queue, GR_CMD_HELLO);
}

zchunk_t*
gr_event_create_heartbeat(georep_t* queue)
{
	return gr_event_create_chunk(queue, GR_CMD_HEARTBEAT);
}

gr_event_t*
gr_event_create(gr_cmd_t cmd)
{
	gr_event_t* obj = malloc(sizeof(*obj));
	gr_log(GR_LOG_DEBUG, "%s: obj at %p cmd %d\n",
		__FUNCTION__, obj, cmd);
	*obj = (gr_event_t) { .cmd = cmd };
	return obj;
}

void
gr_event_free(gr_event_t** ep)
{
	if (!ep) return;
	gr_event_t* obj = *ep;
	if (!obj) return;
	size_t nitems = obj->items ? zlistx_size(obj->items) : 0;
	if (nitems > 0) {
		gr_log(GR_LOG_DEBUG, "%s: obj at %p items at %p size %ld\n",
			__FUNCTION__, obj, obj->items, nitems);
	} else {
		gr_log(GR_LOG_DEBUG, "%s: obj at %p no items\n",
			__FUNCTION__, obj);
	}
	free(obj->address);
	free(obj->id);
	zlistx_destroy(&obj->peers);
	zlistx_destroy(&obj->items);
	pbuf_free(obj->batch_ids);
	zhashx_destroy(&obj->counters);
	free(obj);
	*ep = NULL;
}

int
gr_event_deserialize_byte(const byte** cpp, int* sp, byte* destp)
{
	int csize = sp ? *sp : 0;
	const byte* cdata = cpp ? *cpp : NULL;

	if (csize < 1) {
#if 0
		gr_log(GR_LOG_DEBUG, "%s: return at %d\n", __FUNCTION__, __LINE__);
#endif
		return -1;
	}
	memcpy(destp, cdata, 1);
	*cpp = cdata + 1;
	*sp = csize - 1;

	return 0;
}

int
gr_event_deserialize_int(const byte** cpp, int* sp, int* destp)
{
	uint32_t slen;
	int csize = sp ? *sp : 0;
	const byte* cdata = cpp ? *cpp : NULL;

	if (csize < 4) {
#if 1
		gr_log(GR_LOG_DEBUG, "%s: return at %d, csize = %d\n", __FUNCTION__, __LINE__, csize);
#endif
		return -1;
	}
	memcpy(&slen, cdata, 4);
	*destp = ntohl(slen);
	*cpp = cdata + 4;
	*sp = csize - 4;

	return 0;
}

int
gr_event_deserialize_int64(const byte** cpp, int* sp, int64_t* destp)
{
	uint64_t val;
	int csize = sp ? *sp : 0;
	const byte* cdata = cpp ? *cpp : NULL;

	if (csize < 8) {
#if 1
		gr_log(GR_LOG_DEBUG, "%s: return at %d, csize = %d\n", __FUNCTION__, __LINE__, csize);
#endif
		return -1;
	}
	memcpy(&val, cdata, 8);
#ifdef OS_Linux
	*destp = be64toh(val);
#else
	*destp = ntohll(val);
#endif
	*cpp = cdata + 8;
	*sp = csize - 8;

	return 0;
}

int
gr_event_deserialize_string(const byte** cpp, int* sp, char** destp)
{
	int len;
	int csize = sp ? *sp : 0;
	const byte* cdata = cpp ? *cpp : NULL;
	char* cp;

#if 0
	gr_log(GR_LOG_DEBUG, "%s: at %d\n", __FUNCTION__, __LINE__);
#endif
	if (destp) {
		free(*destp);
		*destp = NULL;
	}

	if (gr_event_deserialize_int(&cdata, &csize, &len) < 0) {
#if 0
		gr_log(GR_LOG_DEBUG, "%s: return at %d\n", __FUNCTION__, __LINE__);
#endif
		return -1;
	}

	if (len < 1) {
		*cpp = cdata;
		*sp = csize;
		return 0;
	}
	if (csize < len) {
		return -1;
	}
	cp = malloc(len + 1);
	memcpy(cp, cdata, len);
	cp[len] = 0;
#if 1
	gr_log(GR_LOG_DEBUG, "%s: parsed string with length %d: `%s'\n",
		__FUNCTION__, len, cp);
#endif
	*destp = cp;
	*cpp = cdata + len;
	*sp = csize - len;
	return len;
}

int
gr_event_deserialize_zchunk(const byte** cpp, int* sp, zchunk_t** destp)
{
	int len;
	int csize = sp ? *sp : 0;
	const byte* cdata = cpp ? *cpp : NULL;

#if 0
	gr_log(GR_LOG_DEBUG, "%s: at %d\n", __FUNCTION__, __LINE__);
#endif
	zchunk_destroy(destp);

	if (gr_event_deserialize_int(&cdata, &csize, &len) < 0) {
#if 0
		gr_log(GR_LOG_DEBUG, "%s: return at %d\n", __FUNCTION__, __LINE__);
#endif
		return -1;
	}

	if (len < 1) {
		*cpp = cdata;
		*sp = csize;
		return 0;
	}
	if (csize < len) {
		return -1;
	}
	*destp = zchunk_new(cdata, len);
	*cpp = cdata + len;
	*sp = csize - len;
	return len;
}

typedef zlistx_t* gr_event_creator_t(void);

typedef int gr_deserialize_element_t(const byte** cpp, int* sp, void** objp);

static int
gr_event_deserialize_array(const byte** cpp, int* sp, zlistx_t** listp,
	gr_event_creator_t* create_list_func,
	gr_deserialize_element_t* deserialize_func)
{
	int i;
	int nitems;
	int csize = sp ? *sp : 0;
	void* obj;
	const byte* cdata = cpp ? *cpp : NULL;
	zlistx_t* list;

	*listp = NULL;
	if (gr_event_deserialize_int(&cdata, &csize, &nitems) < 0)
		return -1;
	if (nitems > 0) {
		gr_log(GR_LOG_DEBUG, "%s: deserialize %d item%s\n",
			__FUNCTION__, nitems, nitems == 1 ? "" : "s");
		list = create_list_func();
		for (i = 0; i < nitems; ++i) {
			if (deserialize_func(&cdata, &csize, &obj) < 0) {
				zlistx_destroy(&list);
				return -1;
			}
			zlistx_add_end(list, obj);
		}
		*listp = list;
	}

	*cpp = cdata;
	*sp = csize;
	return nitems;
}

static int
gr_event_deserialize_counter(const byte** cpp, int* sp, zhashx_t** counters)
{
	int rc;
	int csize = sp ? *sp : 0;
	const byte* cdata = cpp ? *cpp : NULL;
	char* name = NULL;

	rc = gr_event_deserialize_string(&cdata, &csize, &name);
	if (rc <= 0) {
		free(name);
		return rc;
	}

	gr_log(GR_LOG_DEBUG, "%s: looking for counters\n", __FUNCTION__);

	*counters = zhashx_new();
	zhashx_set_destructor(*counters, gr_counter_free);
	for (;;) {
		gr_log(GR_LOG_DEBUG, "%s: found counter `%s'\n", __FUNCTION__, name);
		gr_counter_t* counter = gr_counter_create();
		if (gr_counter_deserialize(counter, &cdata, &csize) < 0) {
			gr_counter_free((void**) &counter);
			free(name);
			return -1;
		}
		zhashx_insert(*counters, name, counter);
		rc = gr_event_deserialize_string(&cdata, &csize, &name);
		if (rc < 0) {
			return rc;
		}
		if (rc == 0) {
			break;
		}
	}
	free(name);

	*cpp = cdata;
	*sp = csize;
	return 0;
}

gr_event_t*
gr_event_parse(const byte** cdata_p, int* csize_p)
{
	byte isok = 0;
	int batch_ids;
	int i;
	int csize = *csize_p;
	int nitems;
	const byte* cdata = *cdata_p;

#if 1
	gr_log(GR_LOG_DEBUG, "%s: parsing chunk with %d bytes\n", __FUNCTION__, csize);
#endif

	if (csize < 1) {
		gr_log(GR_LOG_DEBUG, "%s: return at %d\n", __FUNCTION__, __LINE__);
		return NULL;
	}

	gr_event_t* obj = gr_event_create((gr_cmd_t) cdata[0]);
	cdata++;
	csize--;
	switch (obj->cmd) {
		case GR_CMD_HELLO:
		case GR_CMD_HEARTBEAT:
		case GR_CMD_COUNTER:
			break;
		default:
			gr_log(GR_LOG_INFO, "%s: unknown cmd %d\n", __FUNCTION__, obj->cmd);
			goto done;
	}

	// added by georep_append_event_params()
	if (gr_event_deserialize_string(&cdata, &csize, &obj->address) < 0)
		goto done;

	// all branches must end with "isok = 1"
	switch (obj->cmd) {
		case GR_CMD_HELLO:
			if (gr_event_deserialize_string(&cdata, &csize, &obj->id) < 0)
				break;
			if (gr_event_deserialize_array(&cdata, &csize, &obj->peers,
				gr_peer_list_create, (gr_deserialize_element_t*) gr_peer_deserialize) < 0)
				break;
			isok = 1;
			break;

		case GR_CMD_HEARTBEAT:
			if (gr_event_deserialize_int(&cdata, &csize, &obj->batch_id) < 0)
				break;
			if (gr_event_deserialize_array(&cdata, &csize, &obj->items,
				zlistx_new, // gr_peer_object_info_list_creator
				gr_peer_object_info_deserialize) < 0)
				break;
			nitems = obj->items ? zlistx_size(obj->items) : 0;
			gr_log(GR_LOG_DEBUG,
				"%s: heartbeat contains %d item%s for replication as batch %d\n",
				__FUNCTION__, nitems, nitems == 1 ? "" : "s", obj->batch_id);
			if (gr_event_deserialize_int(&cdata, &csize, &batch_ids) < 0)
				break;
			gr_log(GR_LOG_DEBUG, "%s: got %d batch id%s\n",
				__FUNCTION__, batch_ids, batch_ids == 1 ? "" : "s");
			if (batch_ids > 0) {
				obj->batch_ids = pbuf_create(batch_ids, 0, NULL);
				for (i = 0; i < batch_ids; ++i) {
					int* ip = malloc(sizeof(int));
					if (gr_event_deserialize_int(&cdata, &csize, ip) < 0) {
						free(ip);
						break;
					}
					gr_log(GR_LOG_DEBUG, "%s: found batch id %d\n", __FUNCTION__, *ip);
					pbuf_append(obj->batch_ids, ip);
				}
			}
			isok = 1;
			break;

		case GR_CMD_COUNTER:
			if (gr_event_deserialize_counter(&cdata, &csize, &obj->counters) < 0)
				break;
			isok = 1;
			break;

		default:
			gr_log(GR_LOG_INFO, "%s: unknown cmd %d\n", __FUNCTION__, obj->cmd);
	}

done:
	gr_log(GR_LOG_DEBUG, "%s: done parsing chunk, %d bytes remain\n", __FUNCTION__, csize);
	*cdata_p = cdata;
	*csize_p = csize;
	if (!isok) {
		gr_event_free(&obj);
	}

	return obj;
}

