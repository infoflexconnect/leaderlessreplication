#ifndef _EMG_PBUF_H
#define _EMG_PBUF_H

#include <stddef.h>

typedef void (*pbuf_func_free_t)(void*);
typedef int (*pbuf_func_find_t)(void* obj, const void* arg);
typedef int (*pbuf_func_find_rw_t)(void* obj, void* arg);
typedef void (*pbuf_func_foreach_t)(void* obj, void* arg);
typedef void (*pbuf_func_foreach_ro_t)(void* obj, const void* arg);
typedef int (*pbuf_func_sort_t)(const void* obj1, const void* obj2);

// A special "free" function that doesn't do anything,
// for arrays where the pbuf_t doesn't own the data.
//
#define PBUF_NONE ((void*)-1)

typedef struct {
	int used;
	int allocated;
	int chunksize;
	void** ptr;
	pbuf_func_free_t func_free;
} pbuf_t;

// Initiate a pbuf_t on the stack.
extern void pbuf_init(pbuf_t* pbuf, int size, int chunksize,
		pbuf_func_free_t func_free);

// Allocate a pbuf_t on the heap, and initialize it.
extern pbuf_t* pbuf_create(int size, int chunksize, pbuf_func_free_t func_free);

// Remove all objects in the pbuf_t, calling func_free if it's defined.
extern void pbuf_clear(pbuf_t *pbuf);

// Add a new item at the beginning of the pbuf_t.
extern void pbuf_insert(pbuf_t* pbuf, void* ptr);

// Add a new item at the end of the pbuf_t.
extern void pbuf_append(pbuf_t* pbuf, void* ptr);

// Add all items in "tail" to "pbuf".
extern void pbuf_appendall(pbuf_t* pbuf, const pbuf_t* tail);

// For a pbuf_t initiated with pbuf_init: clear everything used by this pbuf_t.
// After this, the contents of *pbuf_t is undefined.
extern void pbuf_destroy(pbuf_t* pbuf);

// For a pbuf_t created with pbuf_create: clear everything used by this pbuf_t,
// including the pbuf_t structure.
// After this, the contents of *pbuf_t is undefined.
extern void pbuf_free(pbuf_t* pbuf);

// zhashx_destructor_fn version
extern void pbuf_free_p(void** pbuf_p);

// Return the index + 1 of the last item.
extern size_t pbuf_size(const pbuf_t* pbuf);

extern void* pbuf_find(const pbuf_t* pbuf, pbuf_func_find_t func, const void* arg);
extern void* pbuf_find_rw(const pbuf_t* pbuf, pbuf_func_find_rw_t func, void* arg);
extern void pbuf_foreach(const pbuf_t* pbuf, pbuf_func_foreach_t func, void* arg);
extern void pbuf_foreach_ro(const pbuf_t* pbuf, pbuf_func_foreach_ro_t func, const void* arg);
extern int pbuf_removebyfunc(pbuf_t* pbuf, pbuf_func_find_rw_t func, void* arg);
extern int pbuf_removebyfunc2(pbuf_t* pbuf, pbuf_func_find_rw_t func, void* arg,
	pbuf_func_free_t freefunc, int dopack);
extern int pbuf_remove(pbuf_t* pbuf, void* arg);
extern int pbuf_remove2(pbuf_t* pbuf, void* arg, pbuf_func_free_t freefunc);

// Remove an item from the pbuf. The func_free callback is called if set,
// otherwise free() is used.
extern int pbuf_clearat(pbuf_t* pbuf, int ix);

extern void* pbuf_extractfirst(pbuf_t* pbuf);
extern void* pbuf_extractlast(pbuf_t* pbuf);

// Remove any empty slots.
extern int pbuf_compress(pbuf_t* pbuf);

extern void* pbuf_getat(const pbuf_t* pbuf, int ix);

// Store an item. If the slot is already occupied, pbuf_clearat() is used to clear it.
extern void pbuf_putat(pbuf_t* pbuf, int ix, void* obj);

extern void pbuf_sort(pbuf_t* pbuf, pbuf_func_sort_t func);
extern void pbuf_unique(pbuf_t* pbuf, pbuf_func_sort_t func);
extern int pbuf_cmp_pointer(const void* obj, const void* arg);
extern int pbuf_diff_pointer(const void* obj, const void* arg);
extern int pbuf_diff_pointer2(const void* obj, const void* arg);

// Move all items "amount" steps to the right if "amount" is positive,
// and to the left if it's negative. In the latter case, the removed
// items are cleared using pbuf_clearat().
extern void pbuf_move(pbuf_t* pbuf, int amount);

#define PBUF_FOREACH(element, list) { \
	int pbuf_i; \
	if ((void*)list != NULL) for (pbuf_i = 0; pbuf_i < (list)->used; ++pbuf_i) { \
		element = (list)->ptr[pbuf_i];

#define PBUF_FOREACH_END } }

#endif

