#ifndef _COND_H
#define _COND_H

typedef struct gr_cond_t gr_cond_t;

gr_cond_t*
gr_cond_create(void);

void
gr_cond_free(gr_cond_t* obj);

int
gr_cond_signal(gr_cond_t* cond);

void
gr_cond_signal_locked(gr_cond_t* cond);

void
gr_cond_lock(gr_cond_t* cond, int neededBeforeSignal);

void
gr_cond_unlock(gr_cond_t* cond);

int
gr_cond_wait(gr_cond_t* cond, int msecs);

void
gr_cond_release(gr_cond_t* cond);

#endif

