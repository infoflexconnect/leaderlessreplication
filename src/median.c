
#include "median.h"
#include "log.h"

#include <stdlib.h>

struct gr_median_t {
	int filled;
	int next_ix;
	int width;
	int current;
	int* values;
	int* values2;  // for sorting
};

gr_median_t*
gr_median_create(int width)
{
	gr_median_t* obj = malloc(sizeof(*obj));
	obj->width = width;
	if (obj->width < 3)
		obj->width = 3;
	if (obj->width > 9)
		obj->width = 9;
    obj->next_ix = 0;
    obj->filled = 0;
    obj->current = -1.0;
	obj->values = malloc(width * sizeof(obj->values[0]));
	obj->values2 = malloc(width * sizeof(obj->values[0]));
	return obj;
}

void
gr_median_free(gr_median_t* obj)
{
	if (!obj)
		return;
	free(obj->values);
	free(obj->values2);
	free(obj);
}

static int
cmp_int(const void* obj1, const void* obj2)
{
    int diff = *((const int*) obj1) - *((const int*) obj2);
	return diff;
}

void
gr_median_update(gr_median_t* obj, int value, int* filled, int* median)
{
	*filled = 0;
	obj->values[obj->next_ix++] = value;
	if (obj->next_ix >= obj->width) {
		*filled = obj->filled = 1;
		obj->next_ix = 0;
	}
	if (obj->filled) {
		int i;
		for (i = 0; i < obj->width; ++i) {
			obj->values2[i] = obj->values[i];
		}
		qsort(obj->values2, obj->width, sizeof(int), cmp_int);
#if 0
		for (i = 0; i < obj->width; ++i) {
			fprintf(stdout, "%s: ix %d value %.0f\n", __FUNCTION__, i, obj->values2[i]);
		}
#endif
		obj->current = obj->values2[obj->width / 2];
		if (median)
			*median = obj->current;
	}
}

void
gr_median_update_all(gr_median_t** objs, int depth, int value, int* medians)
{
	int filled = 0;
	int m_ix = 0;
	int median = 0;

#if 0
	gr_log(GR_LOG_DEBUG, "%s: update median at %p with new value %d\n",
		__FUNCTION__, objs[0], value);
#endif
	gr_median_update(objs[0], value, &filled, &median);
	while (filled && (m_ix < (depth - 1))) {
		medians[m_ix] = median;
		++m_ix;
		gr_median_update(objs[m_ix], median, &filled, &median);
	}
	while (m_ix < depth) {
		medians[m_ix] = objs[m_ix]->current;
		++m_ix;
	}
}

int
gr_median_current(gr_median_t** objs, int depth, int* level)
{
	int i;
	int m = 0;

	if (level)
		*level = 0;
	if (!objs)
		return 0;

	for (i = 0; i < depth; ++i) {
		gr_median_t* obj = objs[i];
#if 0
		gr_log(GR_LOG_DEBUG, "%s: median at %p level %d has current %d\n",
			__FUNCTION__, obj, i, obj ? obj->current : 0);
#endif
		if (!obj || !obj->filled)
			break;
		if (level)
			*level = i;
		m = obj->current;
	}
	return m;
}

