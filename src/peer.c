
#include "peer.h"
#include "counter.h"
#include "event.h"
#include "log.h"
#include "median.h"
#include "georep.h"
#include "net-base.h"

#include "pbuf.h"

#include <string.h>

typedef struct {
	struct timeval sent;
	pbuf_t keys[1];
} gr_batch_t;

static gr_batch_t*
gr_batch_create(
	int n,
	const struct timeval* now)
{
	gr_batch_t* obj = malloc(sizeof(*obj));
	*obj = (gr_batch_t) { .sent = *now };
	pbuf_init(obj->keys, n, 0, NULL);
	return obj;
}

static void
gr_batch_free(gr_batch_t* obj)
{
	if (!obj)
		return;
	pbuf_destroy(obj->keys);
	free(obj);
}

static void
gr_batch_free_p(void** o_p)
{
	if (!o_p)
		return;
	gr_batch_t** obj_p = (gr_batch_t**) o_p;
	gr_batch_free(*obj_p);
	*obj_p = NULL;
}

typedef enum {
	GR_PEER_STATE_PROSPECT,
	GR_PEER_STATE_CONTACTED,
	GR_PEER_STATE_ACTIVE,
	GR_PEER_STATE_COMATOSE,
	GR_PEER_STATE_TERMINATED,
} gr_peer_state_t;

#define GR_PEER_FLAG_NEED_PEER_LIST 1

#define GR_PEER_MEDIAN_COUNT 9
#define GR_PEER_MEDIAN_WIDTH 3 /* 5 is probably better for production */

struct gr_peer_t {
	gr_peer_state_t state;
	int flags;
	int min_rtt;
	int theta;
	size_t nactive;			// number of active nodes when this node disappeared
	time_t unreachable_since;
	struct timeval last_send;
	struct timeval last_read;
	struct timeval last_counter;
	char* address;
	char* id;
	void* net_data;
	zhashx_t* pending;
	pthread_mutex_t pending_lock[1];
	zhashx_t* sent;		// batch id => pbuf_t<gr_batch_t*>
	zhashx_t* items;	// objects belonging to a remote peer
	zhashx_t* confirmed;	// local object id's stored by this peer
	zhashx_t* still_alive;
	pbuf_t* batch_ids;
	gr_median_t* medians[GR_PEER_MEDIAN_COUNT];
	gr_median_t* thetas[GR_PEER_MEDIAN_COUNT];
	georep_t* queue;
	zhashx_t* counter_replies;
	const gr_net_t* config;
};

static gr_peer_t*
gr_peer_new(void)
{
	gr_peer_t* obj = malloc(sizeof(*obj));
	*obj = (gr_peer_t) {
		.state = GR_PEER_STATE_PROSPECT,
		.pending = gr_peer_object_info_hash_creator(),
		.sent = zhashx_new(),
		.batch_ids = pbuf_create(4, 0, NULL),
		.items = zhashx_new(),
		.counter_replies = zhashx_new(),
	};
	pthread_mutex_init(obj->pending_lock, NULL);
	zhashx_set_destructor(obj->sent, gr_batch_free_p);
	zhashx_set_destructor(obj->counter_replies, gr_counter_free);

	int i;
	for (i = 0; i < GR_PEER_MEDIAN_COUNT; ++i) {
		obj->medians[i] = gr_median_create(GR_PEER_MEDIAN_WIDTH);
		obj->thetas[i] = gr_median_create(GR_PEER_MEDIAN_WIDTH);
	}

	gr_log(GR_LOG_DEBUG, "%s: obj at %p items at %p pending at %p\n",
		__FUNCTION__, obj, obj->items, obj->pending);
	return obj;
}

static void
gr_peer_free(void* o)
{
	gr_peer_t* obj = o;

	if (!o)
		return;
#if 1
	gr_log(GR_LOG_DEBUG, "%s: free obj at %p items at %p with size %ld\n",
		__FUNCTION__, obj, obj->items, zhashx_size(obj->items));
#endif
	free(obj->address);
	free(obj->id);
	if (obj->config && obj->config->net_peer_free)
		obj->config->net_peer_free(&obj->net_data);
	pthread_mutex_lock(obj->pending_lock);
	zhashx_destroy(&obj->pending);
	pthread_mutex_unlock(obj->pending_lock);
	pthread_mutex_destroy(obj->pending_lock);
	zhashx_destroy(&obj->sent);

	zlistx_t* keys = zhashx_keys(obj->items);
	const char* key = zlistx_first(keys);
	while (key) {
		gr_peer_object_info_t* info = zhashx_lookup(obj->items, key);
		if (info) {
			zhashx_delete(obj->items, key);
			gr_peer_object_info_free_p(&info);
		}
		key = zlistx_next(keys);
	}
	zhashx_destroy(&obj->items);
	zlistx_destroy(&keys);

	zhashx_destroy(&obj->still_alive);
	pbuf_free(obj->batch_ids);

	int i;
	for (i = 0; i < GR_PEER_MEDIAN_COUNT; ++i) {
		gr_median_free(obj->medians[i]);
		gr_median_free(obj->thetas[i]);
	}

	zhashx_destroy(&obj->counter_replies);

	free(obj);
}

void
gr_peer_free_p(void** op)
{
	if (!op)
		return;
	gr_peer_free(*op);
	*op = NULL;
}

gr_peer_t*
gr_peer_create(georep_t* queue, const char* address)
{
	if (!address) {
		gr_log(GR_LOG_DEBUG, "%s: address is null\n",
			__FUNCTION__);
		return NULL;
	}

	gr_peer_t* obj = gr_peer_new();
	obj->queue = queue;
	obj->address = strdup(address);

	obj->config = gr_net_config(queue);
	if (obj->config && obj->config->net_peer_init)
		obj->net_data = obj->config->net_peer_init(obj);
	if (!obj->net_data) {
		gr_peer_free(obj);
		obj = NULL;
	}

#if 0
	gr_log(GR_LOG_DEBUG, "%s: return obj at %p for `%s'\n",
		__FUNCTION__, obj, address);
#endif
	return obj;
}

gr_peer_t*
gr_peer_create_with_id(georep_t* queue, const char* address, const char* id)
{
	gr_peer_t* obj = gr_peer_create(queue, address);
	if (!obj)
		return NULL;
	obj->id = id ? strdup(id) : NULL;
	return obj;
}

void
gr_peer_set_queue(gr_peer_t* peer, georep_t* queue)
{
	peer->queue = queue;
}

zlistx_t*
gr_peer_list_create(void)
{
	zlistx_t* list = zlistx_new();
	zlistx_set_comparator(list, gr_peer_compare);
	zlistx_set_destructor(list, gr_peer_free_p);
	return list;
}

void
gr_peer_close(gr_peer_t* obj)
{
	if (!obj)
		return;

	if (obj->config && obj->config->net_peer_close)
		obj->config->net_peer_close(obj->net_data);
}

void
gr_peer_serialize(const void* element, zchunk_t* chunk)
{
	const gr_peer_t* obj = element;

	gr_event_append_string(chunk, obj->address, -1);
	gr_event_append_string(chunk, obj->id, -1);
}

int
gr_peer_deserialize(const byte** cpp, int* sp, gr_peer_t** objp)
{
	int csize = sp ? *sp : 0;
	const byte* cdata = cpp ? *cpp : NULL;
	gr_peer_t* obj = gr_peer_new();

	if (gr_event_deserialize_string(&cdata, &csize, &obj->address) < 0) {
		gr_peer_free(obj);
		return -1;
	}
	if (gr_event_deserialize_string(&cdata, &csize, &obj->id) < 0) {
		gr_peer_free(obj);
		return -1;
	}
#if 0
	gr_log(GR_LOG_DEBUG, "%s: address `%s' id `%s'\n",
		__FUNCTION__, obj->address, obj->id);
#endif
	*objp = obj;
	*cpp = cdata;
	*sp = csize;
	return 0;
}

const char*
gr_peer_get_address(const gr_peer_t* obj)
{
	return obj ? obj->address : NULL;
}

const char*
gr_peer_get_id(const gr_peer_t* obj)
{
	return obj ? obj->id : NULL;
}

zhashx_t*
gr_peer_get_items(const gr_peer_t* obj)
{
	return obj ? obj->items : NULL;
}

int
gr_peer_compare(const void* p1, const void* p2)
{
	const gr_peer_t* peer1 = p1;
	const gr_peer_t* peer2 = p2;

	if (!peer1 || !peer1->address)
		return -1;
	if (!peer2 || !peer2->address)
		return 1;
	return strcmp(peer1->address, peer2->address);
}

int
gr_peer_has_address(const gr_peer_t* obj, const char* address)
{
	if (!obj || !address)
		return 0;
	return strcmp(obj->address, address) == 0;
}

gr_peer_t*
gr_peer_by_address(zlistx_t* list, const char* address)
{
	if (!address)
		return NULL;
	gr_peer_t* peer = zlistx_first(list);
	while (peer) {
		if (strcmp(peer->address, address) == 0) {
			return peer;
		}
		peer = zlistx_next(list);
	}
	return NULL;
}

gr_peer_t*
gr_peer_by_id(zlistx_t* list, const char* id)
{
	if (!id)
		return NULL;
	gr_peer_t* peer = zlistx_first(list);
	while (peer) {
		if (peer->id && (strcmp(peer->id, id) == 0)) {
			return peer;
		}
		peer = zlistx_next(list);
	}
	return NULL;
}

static int
gr_peer_send_chunk(
	gr_peer_t* obj,
	zchunk_t* chunk,
	const struct timeval* now)
{
	if (!obj || !chunk)
		return -1;

	int rc = 0;
	if (obj->config && obj->config->net_peer_send_chunk)
		rc = obj->config->net_peer_send_chunk(obj->net_data, chunk);
	if (rc == 0) {
#if 1
		gr_log(GR_LOG_DEBUG, "%s: successfully sent %ld bytes to peer at `%s'\n",
			__FUNCTION__, zchunk_size(chunk), obj->address);
#endif
		if (now) {
			pthread_mutex_lock(obj->pending_lock);
			obj->last_send = *now;
			pthread_mutex_unlock(obj->pending_lock);
		} else {
			gettimeofday(&obj->last_send, NULL);
		}
	}
	return rc;
}

static void
gr_peer_send_heartbeat(
	gr_peer_t* obj,
	const struct timeval* now)
{
	int* ip;
	char key[32];
	georep_t* queue = obj->queue;

#if 0
	if (timercmp(&obj->last_send, &obj->last_read, >)) {
#if 1
		gr_log(GR_LOG_DEBUG, "%s: not another heartbeat to `%s', waiting for a reply\n",
			__FUNCTION__, obj->address);
#endif
		return;
	}
#endif

	pthread_mutex_lock(obj->pending_lock);
	int nitems = pbuf_size(obj->batch_ids);
	size_t npending = zhashx_size(obj->pending);
	gr_log(GR_LOG_DEBUG, "%s: queue at `%s' npending %d nitems %d\n",
		__FUNCTION__, gr_get_address(queue), (int) npending, nitems);

	if (!npending && !nitems && (now->tv_sec <= obj->last_send.tv_sec)) {
#if 1
		gr_log(GR_LOG_DEBUG, "%s: already sent a heartbeat to `%s' this second\n",
			__FUNCTION__, obj->address);
#endif
		pthread_mutex_unlock(obj->pending_lock);
		return;
	}

	zchunk_t* chunk = gr_event_create_heartbeat(queue);

	// send pending objects

	// list of objects
	int batch_id = gr_next_seqno(queue);
	gr_log(GR_LOG_INFO, "%s: queue at `%s' sends %ld objects as batch %d to peer at `%s'\n",
		__FUNCTION__, gr_get_address(queue), npending, batch_id, obj->address);
	gr_event_append_int(chunk, batch_id);

	gr_event_append_int(chunk, npending);
	if (npending > 0) {
		gr_batch_t* batch = gr_batch_create(npending, now);
		gr_peer_object_info_t* info = zhashx_first(obj->pending);
		while (info) {
			const char* key = zhashx_cursor(obj->pending);
			gr_log(GR_LOG_INFO,
				"%s: sending key `%s' value `%.*s' state %d to peer at `%s', cond at %p\n",
				__FUNCTION__, key,
				info->value ? (int) zchunk_size(info->value) : 0,
				info->value ? (const char*) zchunk_data(info->value) : "",
				info->state,
				obj->address, info->message_cond);
			pbuf_append(batch->keys, strdup(key));
			gr_peer_object_info_serialize(info, key, chunk);
			info = zhashx_next(obj->pending);
		}
		zhashx_purge(obj->pending);
		sprintf(key, "%d", batch_id);
		gr_log(GR_LOG_DEBUG, "%s: store batch with %ld items with key `%s' at %p\n",
			__FUNCTION__, npending, key, batch);
		zhashx_insert(obj->sent, key, batch);
	}

	gr_log(GR_LOG_DEBUG, "%s: confirming %d batch id%s\n",
		__FUNCTION__, nitems, nitems == 1 ? "" : "s");
	gr_event_append_int(chunk, pbuf_size(obj->batch_ids));
	PBUF_FOREACH(ip, obj->batch_ids)
		gr_log(GR_LOG_INFO, "%s: queue at `%s' confirms batch id %d to peer at `%s'\n",
			__FUNCTION__, gr_get_address(queue), *ip, obj->address);
		gr_event_append_int(chunk, *ip);
	PBUF_FOREACH_END
	pbuf_clear(obj->batch_ids);
	pthread_mutex_unlock(obj->pending_lock);

	(void) gr_peer_send_chunk(obj, chunk, now);
	zchunk_destroy(&chunk);
}

static void
gr_peer_send_counters(
	gr_peer_t* obj,
	const struct timeval* now,
	zhashx_t* counters)
{
	int any = 0;
	gr_counter_t* counter;
	georep_t* queue = obj->queue;
	const char* target_peer_id = gr_using_plain_pn(queue) ? NULL : obj->id;

	if (!counters)
		return;

	// build data to be parsed by gr_event_parse()
	//
	gr_log(GR_LOG_DEBUG,
		"%s: queue at `%s' sends counter updates to node `%s'\n",
		__FUNCTION__, gr_get_address(queue), target_peer_id);
	zchunk_t* chunk = gr_event_create_chunk(queue, GR_CMD_COUNTER);
	counter = zhashx_first(counters);
	while (counter) {
		const char* counter_name = zhashx_cursor(counters);
		any |= gr_counter_serialize(counter, chunk, counter_name, target_peer_id);
		counter = zhashx_next(counters);
	}
	gr_event_append_string(chunk, "", 0);	// last counter name

	obj->last_counter = *now;
	if (any) {
		(void) gr_peer_send_chunk(obj, chunk, now);
	}
	zchunk_destroy(&chunk);
}

static const char*
gr_peer_state_name(gr_peer_state_t state)
{
	switch (state) {
		case GR_PEER_STATE_PROSPECT:
			return "PROSPECT";
		case GR_PEER_STATE_CONTACTED:
			return "CONTACTED";
		case GR_PEER_STATE_ACTIVE:
			return "ACTIVE";
		case GR_PEER_STATE_COMATOSE:
			return "COMATOSE";
		case GR_PEER_STATE_TERMINATED:
			return "TERMINATED";
	}
	return "unknown";
}

void
gr_peer_got_data(gr_peer_t* obj, int* became_active, zlistx_t* peers)
{
	if (became_active)
		*became_active = 0;
	if (!obj)
		return;

	gettimeofday(&obj->last_read, NULL);

	gr_peer_t* other = zlistx_first(peers);
	while (other) {
		if ((obj != other) &&
			(other->state == GR_PEER_STATE_COMATOSE)) {
			gr_log(GR_LOG_DEBUG,
				"%s: comatose peer at `%s' is informed that peer `%s' is alive\n",
				__FUNCTION__, other->address, obj->id);
			zhashx_update(other->still_alive, obj->id, (char*) "");
			size_t nalive = zhashx_size(other->still_alive);
#if 0
			gr_log(GR_LOG_DEBUG, "%s: we have seen %ld of %ld nodes\n",
				__FUNCTION__, nalive, other->nactive);
#endif
			if ((nalive + 1) > ((other->nactive + 1) / 2)) {
				gr_log(GR_LOG_INFO,
					"%s: we have seen %ld of %ld nodes, so we are in the majority\n",
					__FUNCTION__, nalive, other->nactive);
				other->state = GR_PEER_STATE_TERMINATED;
			}
		}
		other = zlistx_next(peers);
	}

	if (obj->state == GR_PEER_STATE_ACTIVE)
		return;
	gr_log(GR_LOG_DEBUG, "%s: peer at `%s' changes from state %s to %s\n",
		__FUNCTION__, obj->address,
		gr_peer_state_name(obj->state), gr_peer_state_name(GR_PEER_STATE_ACTIVE));
	if (became_active)
		*became_active = 1;
	obj->state = GR_PEER_STATE_ACTIVE;

}

static void
gr_peer_send_hello(
	gr_peer_t* obj,
	const struct timeval* now)
{
	georep_t* queue = obj->queue;
	zchunk_t* chunk = gr_event_create_hello(queue);
	gr_log(GR_LOG_DEBUG, "%s: queue at `%s' says hello to peer at `%s'\n",
		__FUNCTION__, gr_get_address(queue), obj->address);
	gr_peer_send_chunk(obj, chunk, now);
	zchunk_destroy(&chunk);
	obj->flags &= ~GR_PEER_FLAG_NEED_PEER_LIST;
}

static int
gr_peer_unresponsive(
	gr_peer_t* obj,
	const struct timeval* now,
	int nactive)
{
	if (!obj || !now)
		return 0;

#if 0
	int tm = 0;
	int level = 0;

	tm = gr_median_current(obj->thetas, GR_PEER_MEDIAN_COUNT, &level);
	gr_log(GR_LOG_DEBUG,
		"%s: peer at `%s' out of %d, theta %d -- median %d at level %d, now is %ld, last sent %ld, last read %ld\n",
		__FUNCTION__,
		obj->address,
		nactive,
		obj->theta, tm, level,
		now->tv_sec, obj->last_send.tv_sec, obj->last_read.tv_sec);
	// more than x times the system size and level > y ==> elvis
	// -- but what is the system size?
	// -- for counters we also need the "first" node - with the lowest node id
	// -- adjust x dynamically
	// -- use the y with the highest median?
	if ((nactive > 0) && /* (level > 1) && */ (obj->theta > 3 * nactive)) {
		gr_log(GR_LOG_INFO, "%s: peer at %s seems slow\n", __FUNCTION__, obj->address);
		return 1;
	}
#else
	(void) nactive;
#endif

	pthread_mutex_lock(obj->pending_lock);
	time_t last_sent = obj->last_send.tv_sec;
	time_t last_read = obj->last_read.tv_sec;
	pthread_mutex_unlock(obj->pending_lock);

	time_t sent_ago = now->tv_sec - last_sent;
	time_t read_ago = now->tv_sec - last_read;

	gr_log(GR_LOG_DEBUG, "%s: we last sent something to `%s' %ld seconds ago\n",
		__FUNCTION__, obj->address, sent_ago);

	gr_log(GR_LOG_DEBUG, "%s: we last got something from `%s' %ld seconds ago\n",
		__FUNCTION__, obj->address, read_ago);

	if (last_sent == 0) {
		gr_log(GR_LOG_DEBUG, "%s: I have not sent anything yet\n", __FUNCTION__);
		return 0;
	}

	if (!last_read) {
		gr_log(GR_LOG_DEBUG, "%s: has not gotten anything from `%s' yet\n",
			__FUNCTION__, obj->address);
		if (sent_ago > 10) {
			gr_log(GR_LOG_DEBUG, "%s: still nothing from `%s' after %ld seconds, giving up\n",
				__FUNCTION__, obj->address, sent_ago);
			return 1;
		}
		return 0;
	}

	if ((read_ago - sent_ago) < 5) {
		gr_log(GR_LOG_DEBUG, "%s: we got something less than 5 seconds ago\n", __FUNCTION__);
		return 0;
	}

#if 0
	// TODO: limit based on real data
	if ((obj->last_send.tv_sec == 0) || (now->tv_sec - obj->last_send.tv_sec < 5)) {
		gr_log(GR_LOG_DEBUG, "%s: we either has not sent anything, or less than 5 seconds ago\n",
			__FUNCTION__);
		return 0;
	}
	if ((obj->last_read.tv_sec > 0) && (now->tv_sec - obj->last_read.tv_sec < 5)) {
		gr_log(GR_LOG_DEBUG, "%s: received something less than 5 seconds ago\n",
			__FUNCTION__);
		return 0;
	}
#endif

	return 1;
}

static void
gr_peer_is_comatose(
	gr_peer_t* obj,
	const struct timeval* now,
	int nactive)
{
	if (nactive < 0) return;
	obj->state = GR_PEER_STATE_COMATOSE;
	obj->unreachable_since = now->tv_sec;
	zhashx_destroy(&obj->still_alive);
	obj->still_alive = zhashx_new();
	obj->nactive = nactive;
	gr_peer_close(obj);
}

static void
gr_peer_move_my_messages(gr_peer_t* obj)
{
	size_t nitems = zhashx_size(obj->items);
	gr_log(GR_LOG_DEBUG, "%s: dead peer at `%s' has %ld items in hash at %p\n",
		__FUNCTION__, obj->address, nitems, obj->items);
	georep_t* queue = obj->queue;
	const char* me = gr_my_id(queue);
	zlistx_t* keys = zhashx_keys(obj->items);
	const char* key = zlistx_first(keys);
	while (key) {
		gr_peer_object_info_t* info = zhashx_lookup(obj->items, key);
		if (!info)
			goto next;
		char* id = pbuf_extractfirst(info->other_nodes);
		gr_log(GR_LOG_DEBUG,
			"%s: key `%s' can move to %ld other nodes, the first one is `%s'\n",
			__FUNCTION__, info->key, pbuf_size(info->other_nodes),
			id ? id : "");
		zhashx_delete(obj->items, key);

		if (me && id && (strcmp(me, id) == 0)) {
			gr_adopt(queue, &info);
			goto next;
		}

		gr_peer_t* other = gr_by_id(queue, id);
		if (!other) {
			gr_log(GR_LOG_DEBUG, "%s: can not find peer `%s'\n",
				__FUNCTION__, id);
			gr_peer_object_info_free_p(&info);
		} else {
			zhashx_update(other->items, key, info);
		}
next:
		free(id);
		key = zlistx_next(keys);
	}
	zlistx_destroy(&keys);
}

void
gr_peer_update(
	gr_peer_t* obj,
	const struct timeval* now,
	int needs_peer_list,
	int nactive,
	zhashx_t* counters)
{
	if (!obj || !now)
		return;

	georep_t* queue = obj->queue;
	gr_log(GR_LOG_DEBUG, "%s: queue at `%s' has peer at `%s' in state %s\n",
		__FUNCTION__, gr_get_address(queue), obj->address, gr_peer_state_name(obj->state));

	if (needs_peer_list)
		obj->flags |= GR_PEER_FLAG_NEED_PEER_LIST;

	switch (obj->state) {
		case GR_PEER_STATE_CONTACTED:
		case GR_PEER_STATE_ACTIVE:
			if (gr_peer_unresponsive(obj, now, nactive)) {
				gr_log(GR_LOG_INFO, "%s: queue at `%s' has not seen peer at `%s' for a while\n",
					__FUNCTION__, gr_get_address(queue), obj->address);
				gr_peer_is_comatose(obj, now, nactive);
				return;
			}
			break;
		default: ;
	}

	switch (obj->state) {
		case GR_PEER_STATE_PROSPECT:
			gr_peer_send_hello(obj, now);
			obj->state = GR_PEER_STATE_CONTACTED;
			break;
		case GR_PEER_STATE_CONTACTED:
			break;
		case GR_PEER_STATE_ACTIVE:
			if (obj->flags & GR_PEER_FLAG_NEED_PEER_LIST) {
				gr_peer_send_hello(obj, now);
				break;
			}
			gr_peer_send_heartbeat(obj, now);
			gr_peer_send_counters(obj, now, counters);
			break;
		case GR_PEER_STATE_COMATOSE:
			gr_log(GR_LOG_DEBUG,
				"%s: peer at `%s' has been unreachable for %ld seconds\n",
				__FUNCTION__, obj->address, 
				now->tv_sec - obj->unreachable_since);
			if (now->tv_sec - obj->unreachable_since > 5) {
				gr_peer_move_my_messages(obj);
				obj->state = GR_PEER_STATE_TERMINATED;
			}
			break;
		case GR_PEER_STATE_TERMINATED:
			gr_peer_move_my_messages(obj);
			// TODO: more intelligent interval
			if (now->tv_sec - obj->unreachable_since > 15) {
				obj->unreachable_since = 0;
				obj->state = GR_PEER_STATE_PROSPECT;
			}
			break;
	}
}

void
gr_peer_set_id(gr_peer_t* obj, const char* id)
{
	if (!id)
		return;
	if (obj->id && (strcmp(obj->id, id) == 0))
		return;
	free(obj->id);
	obj->id = strdup(id);
	gr_log(GR_LOG_DEBUG, "%s: peer at `%s' now gets id `%s'\n",
		__FUNCTION__, obj->address, id);
}

void
gr_peer_increment_if_active(gr_peer_t* obj, int* counter)
{
	if (!obj || !counter || (obj->state != GR_PEER_STATE_ACTIVE))
		return;
	*counter += 1;
}

size_t
gr_peer_only_active(
	zlistx_t* list,
	pbuf_t* active,
	pbuf_t* undead,
	pbuf_t* inactive)
{
	size_t n_active = 0;

	if (!list)
		return n_active;

#if 0
	gr_log(GR_LOG_DEBUG, "%s: examine list of %ld peers\n",
		__FUNCTION__, zlistx_size(list));
#endif
	gr_peer_t* peer = zlistx_first(list);
	while (peer) {
#if 0
		gr_log(GR_LOG_DEBUG, "%s: examine peer at `%s' id `%s' state %d (is it %d?)\n",
			__FUNCTION__, peer->address, peer->id ? peer->id : "", peer->state, GR_PEER_STATE_ACTIVE);
#endif
		switch (peer->state) {
			case GR_PEER_STATE_PROSPECT:
			case GR_PEER_STATE_CONTACTED:
				pbuf_append(undead, peer);
				break;
			case GR_PEER_STATE_ACTIVE:
				n_active++;
				pbuf_append(active, peer);
				break;
			case GR_PEER_STATE_COMATOSE:
			case GR_PEER_STATE_TERMINATED:	// emgd -stop
				pbuf_append(inactive, peer);
		}
		peer = zlistx_next(list);
	}
	return n_active;
}

int
gr_peer_nactive(zlistx_t* list)
{
	int nactive = 0;
	gr_peer_t* peer = list ? zlistx_first(list) : NULL;
	while (peer) {
		gr_peer_increment_if_active(peer, &nactive);
		peer = zlistx_next(list);
	}
	return nactive;
}

int
gr_peer_replicate(
	gr_peer_t* obj,
	gr_cond_t* message_cond,
	const char* key,
	gr_peer_object_state_t state,
	const struct timeval* now,
	const char* value, int value_len,
	const pbuf_t* other_nodes)
{
#if 0
	gr_log(GR_LOG_DEBUG, "%s: enter for peer at `%s' lock at %p\n",
		__FUNCTION__, obj->address, obj->pending_lock);
#endif

	pthread_mutex_lock(obj->pending_lock);
	if (state == GR_OBJECT_STATE_NEW) {
		gr_peer_object_info_t* info =
			gr_peer_object_info_create(message_cond, state, value, value_len, other_nodes);
		if (zhashx_insert(obj->pending, key, info) < 0) {
			gr_log(GR_LOG_INFO,
				"%s: queue at `%s' already has key `%s' on queue for peer at `%s'\n",
				__FUNCTION__, gr_get_address(obj->queue),
				key, obj->address);
		}
	} else {
		//new + update => new
		//new + terminate => remove
		gr_peer_object_info_t* current = zhashx_lookup(obj->pending, key);
		if (current) {
			gr_log(GR_LOG_DEBUG, "%s: peer at `%s' key `%s' state %d => %d\n",
				__FUNCTION__, obj->address, key, current->state, state);
			if (current->state == GR_OBJECT_STATE_NEW) {
				switch (state) {
					case GR_OBJECT_STATE_NEW:
						// helping the compiler, letting it know this cover all cases
						break;
					case GR_OBJECT_STATE_UPDATED:
						current->state = GR_OBJECT_STATE_NEW;
						gr_log(GR_LOG_DEBUG, "%s: message updated, cond at %p\n",
							__FUNCTION__, current->message_cond);
						break;
					case GR_OBJECT_STATE_TERMINATED:
						gr_log(GR_LOG_DEBUG, "%s: message terminated, cond at %p\n",
							__FUNCTION__, current->message_cond);
						zhashx_delete(obj->pending, key);
				}
			}
		} else {
			// already sent, now updated or terminated
			gr_peer_object_info_t* info =
				gr_peer_object_info_create(message_cond, state, value, value_len, other_nodes);
			zhashx_insert(obj->pending, key, info);
		}
	}
	size_t npending = zhashx_size(obj->pending);
	pthread_mutex_unlock(obj->pending_lock);
	if (npending >= gr_high_watermark(obj->queue)) {
		gr_log(GR_LOG_DEBUG, "%s: queue at `%s' has %ld pending objects for peer at `%s', updating\n",
			__FUNCTION__, gr_get_address(obj->queue), npending, obj->address);
		gr_peer_update(obj, now, 0, -1, NULL);
	}

	return 0;
}

void
gr_peer_save_items(gr_peer_t* peer, int batch_id, zlistx_t* items)
{
	if (!items || (zlistx_size(items) < 1))
		return;
	gr_peer_object_info_t* object = zlistx_first(items);
	gr_peer_object_info_t* old_object;
	georep_t* queue = peer->queue;
	const char* my_id = gr_my_id(queue);
	while (object) {
		int valuelen = object->value ? (int) zchunk_size(object->value) : 0;
		const char* valueptr = object->value ? (const char*) zchunk_data(object->value) : "";
		gr_log(GR_LOG_INFO,
			"%s: queue at `%s' got key `%s' value `%.*s' state %d from peer at `%s' with info at %p\n",
			__FUNCTION__,
			gr_get_address(queue),
			object->key,
			valuelen, valueptr,
			object->state,
			peer->address, object);
		gr_log(GR_LOG_DEBUG, "%s: node `%s' gets key `%s' stored on %ld other nodes\n",
			__FUNCTION__, my_id, object->key, pbuf_size(object->other_nodes));
		switch (object->state) {
			case GR_OBJECT_STATE_NEW:
				zhashx_update(peer->items, object->key, object);
				const char* id = pbuf_getat(object->other_nodes, pbuf_size(object->other_nodes) - 1);
				if (my_id && id && (strcmp(my_id, id) == 0)) {
					gr_persist(queue, object->key, -1, valueptr, valuelen, 1);
				}
				break;
			case GR_OBJECT_STATE_UPDATED:
				// update value and/or node list separately
				gr_persist(queue, object->key, -1, valueptr, valuelen, 0);
				gr_peer_object_info_free_p(&object);
				break;
			case GR_OBJECT_STATE_TERMINATED:
				gr_persist(queue, object->key, -1, NULL, -1, 0);
				old_object = zhashx_lookup(peer->items, object->key);
				zhashx_delete(peer->items, object->key);
				gr_peer_object_info_free_p(&old_object);
				gr_peer_object_info_free_p(&object);
				break;
		}
		object = zlistx_next(items);
	}
#if 1
	gr_log(GR_LOG_INFO, "%s: queue at `%s' holds %d items for peer at `%s' @ %p\n",
		__FUNCTION__, gr_get_address(queue), (int) zhashx_size(peer->items), peer->address, peer);
#endif
	int* ip = malloc(sizeof(int));
	*ip = batch_id;
	pthread_mutex_lock(peer->pending_lock);
	pbuf_append(peer->batch_ids, ip);
	pthread_mutex_unlock(peer->pending_lock);
}

static void
gr_peer_sent_reply(gr_peer_t* peer, struct timeval* elapsed)
{
	if (!peer)
		return;

	int rtt = elapsed->tv_sec * 1000 * 1000 + elapsed->tv_usec;
	if (!peer->min_rtt || (rtt < peer->min_rtt)) {
		gr_log(GR_LOG_INFO, "%s: peer at `%s' gets new min rtt %d us\n",
			__FUNCTION__, peer->address, rtt);
		peer->min_rtt = rtt;
	}

	int medians[GR_PEER_MEDIAN_COUNT] = { 0 };
	gr_median_update_all(
		peer->medians,
		GR_PEER_MEDIAN_COUNT,
		rtt,
		medians);
	gr_log(GR_LOG_DEBUG,
		"%s: peer `%s' got response medians %d %d %d  %d %d %d  %d %d %d\n",
		__FUNCTION__, peer->address,
		medians[0],
		medians[1],
		medians[2],
		medians[3],
		medians[4],
		medians[5],
		medians[6],
		medians[7],
		medians[8]);
}

typedef struct {
	struct timeval now;
	gr_peer_t* peer;
} gr_batch_info_t;

static void
gr_peer_confirm_batch(void* obj, void* arg)
{
	int* ip = obj;
	char key[32];
	gr_batch_t* batch;
	pbuf_t* keys;
	gr_batch_info_t* info = arg;
	gr_peer_t* peer = info->peer;
	georep_t* queue = peer->queue;
	struct timeval elapsed;

	sprintf(key, "%d", *ip);
	pthread_mutex_lock(peer->pending_lock);
	batch = zhashx_lookup(peer->sent, key);
	pthread_mutex_unlock(peer->pending_lock);
	if (!batch)
		return;
	keys = batch->keys;
	timersub(&info->now, &batch->sent, &elapsed);
	gr_log(GR_LOG_DEBUG,
		"%s: queue at `%s' got confirmation of batch `%s' from peer at `%s' with %ld message%s, reply after %d.%06ds\n",
		__FUNCTION__,
		gr_get_address(queue),
		key,
		peer->address,
		pbuf_size(keys), pbuf_size(keys) == 1 ? "" : "s",
		(int) elapsed.tv_sec, (int) elapsed.tv_usec);

	gr_peer_sent_reply(peer, &elapsed);

	// TODO: update local status of which objects are now stored at this peer
	gr_signal_keys(queue, keys);

	// anything left?
	if (pbuf_compress(keys) == 0) {
		gr_log(GR_LOG_DEBUG, "%s: queue at `%s' has now gotten all replies for batch `%s'\n",
			__FUNCTION__, gr_get_address(queue), key);
		pthread_mutex_lock(peer->pending_lock);
		zhashx_delete(peer->sent, key);
		pthread_mutex_unlock(peer->pending_lock);
		gr_log(GR_LOG_DEBUG, "%s: queue at `%s' now has %d active batches\n",
			__FUNCTION__, gr_get_address(queue), (int) zhashx_size(peer->sent));
	}
}

void
gr_peer_confirmed_batches(gr_peer_t* peer, pbuf_t* batch_ids)
{
	gr_batch_info_t info;

	gettimeofday(&info.now, NULL);
	info.peer = peer;
	pbuf_foreach(batch_ids, gr_peer_confirm_batch, &info);
}

void
gr_peer_update_theta(gr_peer_t* peer, zlistx_t* peers)
{
	gr_log(GR_LOG_DEBUG, "%s: queue at `%s' reset theta for peer `%s' from %d\n",
		__FUNCTION__,
		gr_get_address(peer->queue),
		peer->id,
		peer->theta);

	int medians[GR_PEER_MEDIAN_COUNT] = { -1, -1, -1,  -1, -1, -1,  -1, -1, -1 };
	gr_median_update_all(peer->thetas, GR_PEER_MEDIAN_COUNT, peer->theta, medians);
	gr_log(GR_LOG_DEBUG,
		"%s: peer `%s' got theta medians %d %d %d  %d %d %d  %d %d %d\n",
		__FUNCTION__, peer->address,
		medians[0], medians[1], medians[2],
		medians[3], medians[4], medians[5],
		medians[6], medians[7], medians[8]);

	peer->theta = -1;
	gr_peer_t* other = zlistx_first(peers);
	while (other) {
		other->theta++;
		other = zlistx_next(peers);
	}
}

gr_counter_t*
gr_peer_get_or_create_reply_counter(gr_peer_t* peer, const char* name)
{
	if (!peer)
		return NULL;
	gr_counter_t* counter = zhashx_lookup(peer->counter_replies, name);
	if (!counter) {
		counter = gr_counter_create();
        zhashx_insert(peer->counter_replies, name, counter);
	}
	return counter;
}

void
gr_peer_clear_reply_counters(gr_peer_t* peer)
{
	if (!peer)
		return;
	zhashx_purge(peer->counter_replies);
}

int
gr_peer_get_median(gr_peer_t* peer, int* levelp)
{
	return gr_median_current(peer->medians, GR_PEER_MEDIAN_COUNT, levelp);
}

int
gr_peer_get_min_rtt(gr_peer_t* peer)
{
	return peer ? peer->min_rtt : 0;
}

