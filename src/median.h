#ifndef GR_MEDIAN_H
#define GR_MEDIAN_H 1

typedef struct gr_median_t gr_median_t;

gr_median_t*
gr_median_create(int width);

void
gr_median_free(gr_median_t* obj);

void
gr_median_update(gr_median_t* obj, int value, int* filled, int* median);

void
gr_median_update_all(gr_median_t** objs, int depth, int value, int* medians);

int
gr_median_current(gr_median_t** objs, int depth, int* level);

#endif

