#ifndef GR_PEER_H
#define GR_PEER_H 1

#include "czmq.h"

#include "georep.h"
#include "peer_object_info.h"

#include "pbuf.h"

typedef struct gr_peer_t gr_peer_t;

gr_peer_t*
gr_peer_create(georep_t* queue, const char* address);

gr_peer_t*
gr_peer_create_with_id(georep_t* queue, const char* address, const char* id);

void
gr_peer_free_p(void** obj);

void
gr_peer_set_queue(gr_peer_t* peer, georep_t* queue);

zlistx_t*
gr_peer_list_create(void);

void
gr_peer_close(gr_peer_t* obj);

void
gr_peer_serialize(const void* obj, zchunk_t* chunk);

int
gr_peer_deserialize(const byte** cpp, int* sp, gr_peer_t** objp);

const char*
gr_peer_get_address(const gr_peer_t* obj);

const char*
gr_peer_get_id(const gr_peer_t* obj);

int
gr_peer_has_address(const gr_peer_t* obj, const char* address);

gr_peer_t*
gr_peer_by_address(zlistx_t* list, const char* address);

gr_peer_t*
gr_peer_by_id(zlistx_t* list, const char* id);

zhashx_t*
gr_peer_get_items(const gr_peer_t* obj);

int
gr_peer_compare(const void* p1, const void* p2);

void
gr_peer_got_data(gr_peer_t* obj, int* became_active, zlistx_t* peers);

void
gr_peer_update(
	gr_peer_t* obj,
	const struct timeval* now,
	int needs_peer_list,
	int nactive,
	zhashx_t* counters);

void
gr_peer_set_id(gr_peer_t* obj, const char* id);

void
gr_peer_increment_if_active(gr_peer_t* obj, int* counter);

int
gr_peer_nactive(zlistx_t* list);

size_t
gr_peer_only_active(
	zlistx_t* list,
	pbuf_t* active,
	pbuf_t* undead,
	pbuf_t* inactive);

int
gr_peer_replicate(
	gr_peer_t* obj,
	gr_cond_t* cond,
	const char* key,
	gr_peer_object_state_t state,
	const struct timeval* now,
	const char* value, int value_len,
	const pbuf_t* other_nodes);

zlistx_t*
gr_peer_object_info_list_creator(void);

zhashx_t*
gr_peer_object_info_hash_creator(void);

int
gr_peer_object_info_deserialize(const byte** cpp, int* sp, void** objp);

void
gr_peer_save_items(gr_peer_t* peer, int batch_id, zlistx_t* items);

void
gr_peer_confirmed_batches(gr_peer_t* peer, pbuf_t* batch_ids);

void
gr_peer_update_theta(gr_peer_t* peer, zlistx_t* peers);

gr_counter_t*
gr_peer_get_or_create_reply_counter(gr_peer_t* peer, const char* name);

void
gr_peer_clear_reply_counters(gr_peer_t* peer);

int
gr_peer_get_median(gr_peer_t* peer, int* levelp);

int
gr_peer_get_min_rtt(gr_peer_t* peer);

#endif

