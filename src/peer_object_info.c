
#include "peer_object_info.h"
#include "peer.h"
#include "event.h"
#include "log.h"

gr_peer_object_info_t*
gr_peer_object_info_create(gr_cond_t* message_cond,
	gr_peer_object_state_t state, const char* value, int value_len,
	const pbuf_t* other_nodes)
{
	char* id;
	gr_peer_object_info_t* obj = malloc(sizeof(*obj));
	gr_log(GR_LOG_DEBUG, "%s: obj at %p state %d cond at %p\n",
		__FUNCTION__, obj, state, message_cond);
	*obj = (gr_peer_object_info_t) {
		.state = state,
		.message_cond = message_cond,
		.value = value ? zchunk_new(value, value_len) : NULL,
	};
	pbuf_init(obj->other_nodes, pbuf_size(other_nodes), 0, NULL);
	PBUF_FOREACH(id, other_nodes)
		pbuf_append(obj->other_nodes, strdup(id));
	PBUF_FOREACH_END
	return obj;
}

static void
gr_peer_object_info_free(gr_peer_object_info_t* obj)
{
	if (!obj) return;
	gr_log(GR_LOG_DEBUG, "%s: obj at %p state %d cond at %p\n",
		__FUNCTION__, obj, obj->state, obj->message_cond);
	free(obj->key);
	if (obj->value)
		zchunk_destroy(&obj->value);
	pbuf_destroy(obj->other_nodes);
	free(obj);
}

void
gr_peer_object_info_free_p(gr_peer_object_info_t** p)
{
	if (!p || !*p)
		return;
	gr_peer_object_info_free(*p);
	*p = NULL;
}

zlistx_t*
gr_peer_object_info_list_creator(void)
{
	zlistx_t* list = zlistx_new();
	//zlistx_set_comparator(list, gr_peer_compare);
	zlistx_set_destructor(list, (void (*)(void **)) gr_peer_object_info_free_p);
	return list;
}

zhashx_t*
gr_peer_object_info_hash_creator(void)
{
	zhashx_t* hash = zhashx_new();
	zhashx_set_destructor(hash, (void (*)(void **)) gr_peer_object_info_free_p);
	return hash;
}

void
gr_peer_object_info_serialize(gr_peer_object_info_t* obj, const char* key,
	zchunk_t* chunk)
{
	if (!obj || !key || !chunk)
		return;

	gr_log(GR_LOG_DEBUG, "%s: state %d key `%s' value `%.*s', %ld other nodes\n",
		__FUNCTION__, obj->state, key,
		obj->value ? (int) zchunk_size(obj->value) : 0,
		obj->value ? (const char*) zchunk_data(obj->value) : "",
		pbuf_size(obj->other_nodes));
	gr_event_append_byte(chunk, obj->state);
	gr_event_append_string(chunk, key, -1);
	gr_event_append_zchunk(chunk, obj->value);

	gr_event_append_int(chunk, pbuf_size(obj->other_nodes));
	const char* id;
	PBUF_FOREACH(id, obj->other_nodes)
		gr_event_append_string(chunk, id, -1);
	PBUF_FOREACH_END
}

int
gr_peer_object_info_deserialize(const byte** cpp, int* sp, void** objp)
{
	byte b;
	int ival;
	int csize = sp ? *sp : 0;
	const byte* cdata = cpp ? *cpp : NULL;
	gr_peer_object_info_t* obj = gr_peer_object_info_create(NULL,
		GR_OBJECT_STATE_NEW, NULL, 0, NULL);

	if (gr_event_deserialize_byte(&cdata, &csize, &b) < 0) {
		gr_peer_object_info_free(obj);
		return -1;
	}
	obj->state = b;
	if (gr_event_deserialize_string(&cdata, &csize, &obj->key) < 0) {
		gr_peer_object_info_free(obj);
		return -1;
	}
	if (gr_event_deserialize_zchunk(&cdata, &csize, &obj->value) < 0) {
		gr_peer_object_info_free(obj);
		return -1;
	}

	if (gr_event_deserialize_int(&cdata, &csize, &ival) < 0) {
		gr_peer_object_info_free(obj);
		return -1;
	}
	int i;
	for (i = 0; i < ival; ++i) {
		char* id = NULL;
		if (gr_event_deserialize_string(&cdata, &csize, &id) < 0) {
			gr_peer_object_info_free(obj);
			return -1;
		}
		pbuf_append(obj->other_nodes, id);
	}

	gr_log(GR_LOG_DEBUG,
		"%s: obj at %p state %d key `%s' value `%.*s' %ld other nodes\n",
		__FUNCTION__, obj, obj->state, obj->key,
		obj->value ? (int) zchunk_size(obj->value) : 0,
		obj->value ? (const char*) zchunk_data(obj->value) : "",
		pbuf_size(obj->other_nodes));
	*objp = obj;
	*cpp = cdata;
	*sp = csize;
	return 0;
}

