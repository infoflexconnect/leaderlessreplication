
#include "log.h"
#include "monotime.h"

#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>

static pthread_mutex_t* mutex = NULL;
static FILE* fp = NULL;
static gr_log_level_t minlevel = GR_LOG_INFO;

static void
gr_log_close(void)
{
//	fprintf(stderr, "%s: enter\n", __FUNCTION__);
	if (fp) {
		fclose(fp);
		fp = NULL;
	}
	if (mutex) {
		pthread_mutex_destroy(mutex);
		free(mutex);
		mutex = NULL;
	}
}

void
gr_log_init(void)
{
	mutex = malloc(sizeof(*mutex));
	pthread_mutex_init(mutex, NULL);
	atexit(gr_log_close);
}

void
gr_log_level(gr_log_level_t level)
{
	minlevel = level;
}

int
gr_log_enabled(gr_log_level_t level)
{
	return level >= minlevel;
}

void
gr_log(gr_log_level_t level, const char* fmt, ...)
{
	int len;
	va_list args;
	struct timeval tv[1];
	struct tm tm[1];
	char fmtbuf[1024];

	if (level < minlevel)
		return;

	if (gettimeofday_monotonic(tv) != 0)
		return;

	pthread_mutex_lock(mutex);
	if (!fp) {
		fp = fopen("gr.log", "a");
	}

	localtime_r(&tv->tv_sec, tm);
	pthread_t self = pthread_self();
	sprintf(fmtbuf, "%4d-%02d-%02d %02d:%02d:%02d.%06d |%08x| %s",
		tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
		tm->tm_hour, tm->tm_min, tm->tm_sec,
		(int) tv->tv_usec,
		(int) (long long) self,
		fmt);

	va_start(args, fmt);
	len = vfprintf(fp, fmtbuf, args);
	(void) len;
	va_end(args);
	pthread_mutex_unlock(mutex);
	fflush(fp);
}

