
#include "net-zeromq.h"
#include "peer.h"

#include "log.h"

typedef struct {
	georep_t* queue;
	void* context;
	zsock_t* socket;
	zsock_t* internal_push_socket;
	zsock_t* internal_pull_socket;
	void* loop;
} gr_zeromq_queue_t;

typedef struct {
	zsock_t* socket;
	char* address;
} gr_zeromq_peer_t;

static void
gr_zeromq_loop(void* arg);

void*
gr_zeromq_init(georep_t* queue)
{
	gr_zeromq_queue_t* obj;
	const char* address = gr_get_address(queue);

	if (!queue || !address)
		return NULL;

	obj = malloc(sizeof(*obj));
	*obj = (gr_zeromq_queue_t) {
		.queue = queue,
		.context = zmq_ctx_new(),
	};

	char zaddress[strlen(address) + 7]; // must be before the goto

	if (!obj->context) {
		gr_log(GR_LOG_INFO, "%s: zmq_ctx_new returned NULL\n", __FUNCTION__);
		goto failed;
	}

	sprintf(zaddress, "tcp://%s", address);
	obj->socket = zsock_new_pull(zaddress);
	if (!obj->socket) {
		gr_log(GR_LOG_INFO, "%s: zsock_new_pull `%s' returned NULL\n",
			__FUNCTION__, zaddress);
		goto failed;
	}

	char inproc_name[64];
	sprintf(inproc_name, "inproc://%llx", (long long unsigned int) queue);
	obj->internal_pull_socket = zsock_new_pull(inproc_name);
	obj->internal_push_socket = zsock_new_push(inproc_name);

	obj->loop = zmq_threadstart(gr_zeromq_loop, obj);

	return obj;

failed:
	if (obj->context)
		zmq_ctx_term(obj->context);
	free(obj);
	return NULL;
}

void
gr_zeromq_close(void** op)
{
	gr_zeromq_queue_t* obj = op ? *op : NULL;

	if (!obj)
		return;
	*op = NULL;

	zmq_ctx_term(obj->context);
	zframe_t* frame = zframe_new("X", 1);
	/* int rc = */ zframe_send(&frame, obj->internal_push_socket, 0);
	sleep(1); // TODO: fix this!

	zsock_destroy(&obj->socket);
	zsock_destroy(&obj->internal_push_socket);
	zsock_destroy(&obj->internal_pull_socket);

	// can we do this? it was previously done at the end of the georep destructor
	zmq_threadclose(obj->loop);

	free(obj);
}

static int
gr_zeromq_reader(zloop_t* loop, zsock_t* reader, void* arg)
{
	int rc = 0;
	gr_zeromq_queue_t* obj = arg;
	georep_t* queue = obj->queue;

	(void) loop;

#if 1
	gr_log(GR_LOG_DEBUG,
		"%s: data available to node `%s' on `%s' to reader %p\n",
		__FUNCTION__, gr_my_id(queue), gr_get_address(queue), reader);
#endif

	zframe_t* frame = zframe_recv(reader);
	if (!frame)
		return 0;

	if (gr_log_enabled(GR_LOG_DEBUG)) {
		char* hf = zframe_strhex(frame);
		gr_log(GR_LOG_DEBUG, "%s: queue at `%s' read: %s\n",
			__FUNCTION__, gr_get_address(queue), hf);
		free(hf);
	}
	int csize = zframe_size(frame);
	const byte* cdata = zframe_data(frame);
	if (reader == obj->socket) {
		while (csize > 0) {
			if (gr_handle_incoming(queue, &cdata, &csize) < 0)
				break;
		}
	} else {
		if (reader == obj->internal_pull_socket) {
			if ((csize > 0) && (cdata[0] == 'X'))
				rc = -1;
		}
	}

	zframe_destroy(&frame);

	if (rc < 0) {
		gr_log(GR_LOG_INFO, "%s: queue at `%s' will exit\n",
			__FUNCTION__, gr_get_address(queue));
	}
	return rc;
}

static int
gr_zeromq_sync(zloop_t* loop, int loop_id, void* arg)
{
	gr_zeromq_queue_t* obj = arg;
	georep_t* queue = obj->queue;

	(void) loop;
	(void) loop_id;

	return georep_sync(queue);
}

static void
gr_zeromq_loop(void* arg)
{
	int rc;
	gr_zeromq_queue_t* obj = arg;
	georep_t* queue = obj->queue;

	gr_log(GR_LOG_INFO, "%s: enter for node `%s' at `%s'\n",
		__FUNCTION__, gr_my_id(queue), gr_get_address(queue));

	zloop_t* loop = zloop_new();

	// start readers
	rc = zloop_reader(loop, obj->socket, gr_zeromq_reader, obj);
	if (rc < 0) {
		gr_log(GR_LOG_INFO, "%s: reader on `%s', returned %d\n",
			__FUNCTION__, gr_get_address(queue), rc);
	}
	rc = zloop_reader(loop, obj->internal_pull_socket, gr_zeromq_reader, obj);
	if (rc < 0) {
		gr_log(GR_LOG_INFO, "%s: internal reader returned %d\n",
			__FUNCTION__, rc);
	}

	zloop_timer(loop, 3 /* ms */, 0, gr_zeromq_sync, obj);

	zloop_start(loop);

	zloop_destroy(&loop);
#if 1
	gr_log(GR_LOG_INFO, "%s: leave for node `%s' at `%s'\n",
		__FUNCTION__, gr_my_id(queue), gr_get_address(queue));
#endif
}

void*
gr_zeromq_peer_init(gr_peer_t* peer)
{
	const char* address = gr_peer_get_address(peer);

	if (!peer || !address)
		return NULL;

	gr_zeromq_peer_t* obj = malloc(sizeof(*obj));
	*obj = (gr_zeromq_peer_t) {
		.address = malloc(strlen(address) + 7),
	};

	sprintf(obj->address, "tcp://%s", address);

	obj->socket = zsock_new_push(obj->address);
	if (!obj->socket) {
		gr_log(GR_LOG_DEBUG, "%s: socket is null for socket `%s'\n",
			__FUNCTION__, obj->address);
		goto failed;
	}

	return obj;

failed:
	gr_zeromq_peer_free((void**) &obj);
	return NULL;
}

void
gr_zeromq_peer_free(void** op)
{
	gr_zeromq_peer_t* obj = op ? *op : NULL;

	if (!obj)
		return;
	*op = NULL;

	free(obj->address);

	if (obj->socket)
		zsock_destroy(&obj->socket);

	free(obj);
}

void
gr_zeromq_peer_close(void* p)
{
	gr_zeromq_peer_t* obj = p;

	if (obj) {
		if (obj->socket)
			zsock_destroy(&obj->socket);
	}
}

int
gr_zeromq_peer_send_chunk(void* p, zchunk_t* chunk)
{
	int rc;
	gr_zeromq_peer_t* obj = p;

	if (!obj->socket) {
		obj->socket = zsock_new_push(obj->address);
		if (!obj->socket) {
			return -1;
		}
	}
	zframe_t* frame = zframe_new(zchunk_data(chunk), zchunk_size(chunk));
	if (gr_log_enabled(GR_LOG_DEBUG)) {
		char* hf = zframe_strhex(frame);
		gr_log(GR_LOG_DEBUG, "%s: send to peer at `%s': %s\n", __FUNCTION__, obj->address, hf);
		free(hf);
	}
	rc = zframe_send(&frame, obj->socket, 0);
	if (rc < 0) {
		gr_log(GR_LOG_DEBUG, "%s: zframe_send returned %d\n", __FUNCTION__, rc);
	} else {
#if 1
		gr_log(GR_LOG_DEBUG, "%s: successfully sent %ld bytes to peer at `%s'\n",
			__FUNCTION__, zchunk_size(chunk), obj->address);
#endif
	}
	return rc;
}

void
gr_zeromq_setup(gr_net_t* config)
{
	config->net_init = gr_zeromq_init;
	config->net_close = gr_zeromq_close;
	config->net_peer_init = gr_zeromq_peer_init;
	config->net_peer_free = gr_zeromq_peer_free;
	config->net_peer_close = gr_zeromq_peer_close;
	config->net_peer_send_chunk = gr_zeromq_peer_send_chunk;
}

