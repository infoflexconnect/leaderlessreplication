
#include "monotime.h"

#include <sys/time.h>
#include <stdio.h>

int
gettimeofday_monotonic(struct timeval* tvp)
{
	int rc;
	static __thread struct timeval last[1] = { { 0, 0 } };
	struct timeval now[1];
	struct timeval diff[1];

	rc = gettimeofday(now, NULL);
	if (rc < 0)
		return rc;

	timersub(now, last, diff);
	if ((diff->tv_sec > 0) ||
		((diff->tv_sec == 0) && ((diff->tv_usec > 0)))) {
		*last = *now;
	}

	*tvp = *last;

	return 0;
}

