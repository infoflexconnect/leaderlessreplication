/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF
Variable buffers that increases size when adding data.
* * * * * * * * * * * * * * * * * * * * * * * * */

#include "pbuf.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

static void*
xrealloc(void* oldp, int oldsize, int newsize)
{
	void** pp = calloc(1, newsize);
	memcpy(pp, oldp, oldsize);
	free(oldp);
	return pp;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_INIT
Initialize an already allocated pbuf.
* * * * * * * * * * * * * * * * * * * * * * * * */
void
pbuf_init(pbuf_t* pbuf, int size, int chunksize, pbuf_func_free_t func_free)
{
	if (!pbuf) return;

	pbuf->used = 0;
	if (size <= 0) size = 1;
	pbuf->allocated = size;
	if (chunksize <= 0) chunksize = size / 2;
	if (chunksize <= 8) chunksize = 8;
	pbuf->chunksize = chunksize;
	pbuf->ptr = calloc(pbuf->allocated, sizeof(pbuf->ptr[0]));
	pbuf->func_free = func_free;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_CREATE
* * * * * * * * * * * * * * * * * * * * * * * * */
pbuf_t *
pbuf_create(int size, int chunksize, pbuf_func_free_t func_free)
{
	pbuf_t* pbuf = malloc(sizeof(pbuf_t));
	pbuf_init(pbuf, size, chunksize, func_free);
	return pbuf;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_DESTROY
* * * * * * * * * * * * * * * * * * * * * * * * */
void
pbuf_destroy(pbuf_t *pbuf)
{
	if (!pbuf) return;

	pbuf_clear(pbuf);
	free(pbuf->ptr);
}

static void
pbuf_clear_range(pbuf_t* pbuf, int first, int firstNot)
{
	int i;

	if (!pbuf) return;

	for (i = first; i < firstNot; ++i) {
		void* obj = pbuf->ptr[i];
		if (!obj) continue;
		if (pbuf->func_free != PBUF_NONE) {
			if (pbuf->func_free)
				pbuf->func_free(obj);
			else
				free(obj);
		}
		pbuf->ptr[i] = NULL;
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_CLEAR
* * * * * * * * * * * * * * * * * * * * * * * * */
void
pbuf_clear(pbuf_t *pbuf)
{
	if (!pbuf)
		return;

	pbuf_clear_range(pbuf, 0, pbuf->used);
	pbuf->used = 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_FREE
* * * * * * * * * * * * * * * * * * * * * * * * */
void
pbuf_free(pbuf_t *pbuf)
{
	pbuf_destroy(pbuf);
	free(pbuf);
}

void
pbuf_free_p(void** pbuf_p)
{
	if (!pbuf_p) return;
	pbuf_t* pbuf = *((pbuf_t**) pbuf_p);	// C syntax as its finest
	pbuf_free(pbuf);
	*pbuf_p = NULL;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_ENSURE
Ensure that a number of entries can be added safely.
* * * * * * * * * * * * * * * * * * * * * * * * */
static void
pbuf_ensure(pbuf_t *pbuf, int amount)
{
	int blocks;
	int needed = pbuf->used + amount;
	int current = pbuf->allocated;

	if (needed < pbuf->allocated) return;
	blocks = needed / pbuf->chunksize + 1;
	pbuf->allocated = blocks * pbuf->chunksize;
	pbuf->ptr = xrealloc(pbuf->ptr,
		current * sizeof(pbuf->ptr[0]),
		pbuf->allocated * sizeof(pbuf->ptr[0]));
	memset(pbuf->ptr + current, 0,
		(pbuf->allocated - current) * sizeof(pbuf->ptr[0]));
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_SIZE
* * * * * * * * * * * * * * * * * * * * * * * * */
size_t
pbuf_size(const pbuf_t* pbuf)
{
	if (!pbuf) return 0;

	return pbuf->used;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_APPEND
* * * * * * * * * * * * * * * * * * * * * * * * */
void
pbuf_append(pbuf_t *pbuf, void* ptr)
{
	/* Invalid pbuf specified, do nothing */
	if (!pbuf) return;

	pbuf_ensure(pbuf, 1);
	pbuf->ptr[pbuf->used++] = ptr;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_APPENDALL
* * * * * * * * * * * * * * * * * * * * * * * * */
void
pbuf_appendall(pbuf_t* pbuf, const pbuf_t* tail)
{
	/* Do we really have to do anything? */
	if (!pbuf || !tail || (tail->used == 0))
		return;

	pbuf_ensure(pbuf, tail->used);
	memcpy(pbuf->ptr + pbuf->used, tail->ptr, tail->used * sizeof(void*));
	pbuf->used += tail->used;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_INSERT
Insert an entry before the current contents.
* * * * * * * * * * * * * * * * * * * * * * * * */
void
pbuf_insert(pbuf_t* pbuf, void* ptr)
{
	if (!pbuf) return;

	pbuf_ensure(pbuf, 1);
	memmove(pbuf->ptr + 1, pbuf->ptr, pbuf->used * sizeof(pbuf->ptr[0]));
	pbuf->ptr[0] = ptr;
	pbuf->used++;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_FIND
Look for an entry where the given function returns true.
* * * * * * * * * * * * * * * * * * * * * * * * */
void*
pbuf_find(const pbuf_t* pbuf, pbuf_func_find_t func_find, const void* arg)
{
	int i;

	if (!pbuf || !func_find) return NULL;
	for (i = 0; i < pbuf->used; ++i) {
		void* obj = pbuf->ptr[i];
		if (!obj) continue;
		if (func_find(obj, arg))
			return obj;
	}
	return NULL;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_FIND_RW
Look for an entry where the given function returns true.
Used when the arg can be modified by the callback function.
* * * * * * * * * * * * * * * * * * * * * * * * */
void*
pbuf_find_rw(const pbuf_t* pbuf, pbuf_func_find_rw_t func_find, void* arg)
{
	int i;

	if (!pbuf || !func_find) return NULL;
	for (i = 0; i < pbuf->used; ++i) {
		void* obj = pbuf->ptr[i];
		if (!obj) continue;
		if (func_find(obj, arg))
			return obj;
	}
	return NULL;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_FOREACH
* * * * * * * * * * * * * * * * * * * * * * * * */
void
pbuf_foreach(const pbuf_t* pbuf, pbuf_func_foreach_t func, void* arg)
{
	int i;

	if (!pbuf || !func) return;
	for (i = 0; i < pbuf->used; ++i) {
		void* obj = pbuf->ptr[i];
		if (!obj) continue;
		func(obj, arg);
	}
}

void
pbuf_foreach_ro(const pbuf_t* pbuf, pbuf_func_foreach_ro_t func, const void* arg)
{
	int i;

	if (!pbuf || !func) return;
	for (i = 0; i < pbuf->used; ++i) {
		void* obj = pbuf->ptr[i];
		if (!obj) continue;
		func(obj, arg);
	}
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_REMOVEBYFUNC
* * * * * * * * * * * * * * * * * * * * * * * * */
int
pbuf_removebyfunc2(pbuf_t* pbuf, pbuf_func_find_rw_t func, void* arg,
		pbuf_func_free_t freefunc, int dopack)
{
	int i;
	int j;
	int count = 0;

	// Is there anything to do?
	//
	if (!pbuf || (pbuf->used == 0))
		return 0;
	for (i = 0, j = 0; i < pbuf->used; ++i) {
		void* obj = pbuf->ptr[i];
		if (!obj) continue;
		pbuf->ptr[i] = NULL;
		if (func && func(obj, arg)) {
			++count;
			if (freefunc != PBUF_NONE) {
				if (freefunc)
					freefunc(obj);
				else
					free(obj);
			}
		} else {
			// j might be equal to j here, but that doesn't really matter.
			// The important thing is that j never will be greater than i.
			//
			if (!dopack) j = i;
			pbuf->ptr[j++] = obj;
		}
	}
	pbuf->used = j;
	return count;
}

int
pbuf_removebyfunc(pbuf_t* pbuf, pbuf_func_find_rw_t func, void* arg)
{
	if (!pbuf) return 0;
	return pbuf_removebyfunc2(pbuf, func, arg, pbuf->func_free, 1);
}

int
pbuf_cmp_pointer(const void* obj, const void* arg)
{
	return obj == arg;
}

int
pbuf_diff_pointer(const void* obj, const void* arg)
{
	unsigned long o = (unsigned long)obj;
	unsigned long a = (unsigned long)arg;

//	xlog(SMSLOG_DEBUG2, "%s: got o %08lx, a %08lx\n", __FUNCTION__, o, a);
	if (o > a) return 1;
	if (o < a) return -1;
	return 0;
}

int
pbuf_diff_pointer2(const void* obj, const void* arg)
{
	const unsigned long o = (unsigned long)*(const void**)obj;
	const unsigned long a = (unsigned long)*(const void**)arg;

	if (o > a) return 1;
	if (o < a) return -1;
	return 0;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_REMOVE2
* * * * * * * * * * * * * * * * * * * * * * * * */
int
pbuf_remove2(pbuf_t* pbuf, void* arg, pbuf_func_free_t freefunc)
{
	return pbuf_removebyfunc2(pbuf, (pbuf_func_find_rw_t) pbuf_cmp_pointer, arg, freefunc, 1);
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_REMOVE
* * * * * * * * * * * * * * * * * * * * * * * * */
int
pbuf_remove(pbuf_t* pbuf, void* arg)
{
	if (!pbuf) return 0;
	return pbuf_removebyfunc(pbuf, (pbuf_func_find_rw_t) pbuf_cmp_pointer, arg);
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_CLEARAT
* * * * * * * * * * * * * * * * * * * * * * * * */
int
pbuf_clearat(pbuf_t* pbuf, int ix)
{
	void* obj;

	if (!pbuf || (pbuf->used <= ix))
		return 0;
	obj = pbuf->ptr[ix];
	if (!obj)
		return 0;
	pbuf->ptr[ix] = NULL;
	if (pbuf->func_free == PBUF_NONE)
		return 1;
	if (pbuf->func_free)
		pbuf->func_free(obj);
	else
		free(obj);
	return 1;
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_GETAT
* * * * * * * * * * * * * * * * * * * * * * * * */
void*
pbuf_getat(const pbuf_t* pbuf, int ix)
{
	if (!pbuf || (pbuf->used <= ix) || (ix < 0))
		return NULL;
	return pbuf->ptr[ix];
}

/* * * * * * * * * * * * * * * * * * * * * * * * *
PBUF_PUTAT
* * * * * * * * * * * * * * * * * * * * * * * * */
void
pbuf_putat(pbuf_t* pbuf, int ix, void* obj)
{
	if (!pbuf)
		return;
	pbuf_ensure(pbuf, ix + 1 - pbuf->used);
	pbuf_clearat(pbuf, ix);
	pbuf->ptr[ix] = obj;

	// Increase 'used' if necessary
	//
	if (ix + 1 > pbuf->used)
		pbuf->used = ix + 1;

	// Clear empty slots at the end, if any
	//
	while (pbuf->used > 0) {
		if (pbuf->ptr[pbuf->used - 1])
			break;
		pbuf->used--;
	}
}

void*
pbuf_extractfirst(pbuf_t* pbuf)
{
	void* obj;

	if (!pbuf)
		return NULL;
	obj = pbuf_getat(pbuf, 0);
	if (!obj)
		return NULL;
	pbuf_remove2(pbuf, obj, PBUF_NONE);
	return obj;
}

void*
pbuf_extractlast(pbuf_t* pbuf)
{
	void* obj;

	if (!pbuf || !pbuf->used)
		return NULL;
	pbuf->used--;
	obj = pbuf->ptr[pbuf->used];
	pbuf_clearat(pbuf, pbuf->used);
	return obj;
}

// Remove all NULL items.
//
int
pbuf_compress(pbuf_t* pbuf)
{
	return pbuf_removebyfunc(pbuf, NULL, NULL);
}

void
pbuf_sort(pbuf_t* pbuf, pbuf_func_sort_t func)
{
	if (!pbuf || (pbuf->used < 2)) return;
	qsort(pbuf->ptr, pbuf->used, sizeof(pbuf->ptr[0]), func);
}

