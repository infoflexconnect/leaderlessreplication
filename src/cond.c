
#include "cond.h"
#include "log.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>

struct gr_cond_t {
	int neededBeforeSignal;
	pthread_cond_t cond;
	pthread_mutex_t mutex;
	pthread_mutex_t neededBeforeSignal_lock;
};

gr_cond_t*
gr_cond_create(void)
{
	gr_cond_t* obj = calloc(1, sizeof(*obj));
	gr_log(GR_LOG_DEBUG, "%s: obj at %p\n", __FUNCTION__, obj);
	pthread_cond_init(&obj->cond, NULL);
	pthread_mutex_init(&obj->mutex, NULL);
	pthread_mutex_init(&obj->neededBeforeSignal_lock, NULL);
	return obj;
}

void
gr_cond_free(gr_cond_t* obj)
{
	if (!obj) return;
	gr_log(GR_LOG_DEBUG, "%s: obj at %p\n", __FUNCTION__, obj);
	pthread_cond_destroy(&obj->cond);
	pthread_mutex_destroy(&obj->mutex);
	pthread_mutex_destroy(&obj->neededBeforeSignal_lock);
	free(obj);
}

void
gr_cond_signal_locked(gr_cond_t* cond)
{
	if (!cond) return;
	pthread_cond_signal(&cond->cond);
}

int
gr_cond_signal(gr_cond_t* cond)
{
	int rc = 0;

	if (!cond)
		return 0;
	gr_log(GR_LOG_DEBUG, "%s: cond at %p need %d before lock\n",
		__FUNCTION__, cond, cond->neededBeforeSignal);
	pthread_mutex_lock(&cond->neededBeforeSignal_lock);
	cond->neededBeforeSignal -= 1;
	if (cond->neededBeforeSignal < 1) {
		pthread_mutex_unlock(&cond->neededBeforeSignal_lock);
		rc = 1;
		pthread_mutex_lock(&cond->mutex);
		pthread_cond_signal(&cond->cond);
		pthread_mutex_unlock(&cond->mutex);
	} else {
		pthread_mutex_unlock(&cond->neededBeforeSignal_lock);
	}

	return rc;
}

void
gr_cond_lock(gr_cond_t* cond, int neededBeforeSignal)
{
	if (!cond) return;
	gr_log(GR_LOG_DEBUG, "%s: cond at %p need %d before lock\n",
		__FUNCTION__, cond, neededBeforeSignal);
	pthread_mutex_lock(&cond->neededBeforeSignal_lock);
	cond->neededBeforeSignal = neededBeforeSignal;
	pthread_mutex_unlock(&cond->neededBeforeSignal_lock);
	pthread_mutex_lock(&cond->mutex);
}

void
gr_cond_unlock(gr_cond_t* cond)
{
	if (!cond) return;
	gr_log(GR_LOG_DEBUG, "%s: cond at %p before unlock\n",
		__FUNCTION__, cond);
	pthread_mutex_unlock(&cond->mutex);
}

int
gr_cond_wait(gr_cond_t* cond, int msecs)
{
	int ret = 0;

	if (!cond) return 0;

	gr_log(GR_LOG_DEBUG, "%s: cond at %p waits for %d ms\n",
		__FUNCTION__, cond, msecs);

	pthread_mutex_lock(&cond->mutex);
	if (msecs > 0) {
		struct timeval tp;
		gettimeofday(&tp, NULL);
		time_t secs = tp.tv_sec + msecs / 1000;
		int newmsecs = tp.tv_usec / 1000 + msecs % 1000;
		if (newmsecs >= 1000) {
			secs++;
			newmsecs %= 1000;
		}
		struct timespec ts = (struct timespec) {
			.tv_sec = secs,
			.tv_nsec = newmsecs * 1000000,
		};
		for (;;) {
			ret = pthread_cond_timedwait(&cond->cond, &cond->mutex, &ts);
			if (!ret || (errno != EAGAIN))
				break;
		}
	} else {
		for (;;) {
			ret = pthread_cond_wait(&cond->cond, &cond->mutex);
			if (!ret || (errno != EAGAIN))
				break;
		}
	}
	pthread_mutex_unlock(&cond->mutex);
	if (ret) {
		gr_log(GR_LOG_DEBUG, "%s: cond at %p returned %d and errno %d\n",
			__FUNCTION__, cond, ret, errno);
	}

	return ret;
}

