
#include "counter.h"
#include "event.h"
#include "log.h"
#include "peer.h"

#include "czmq.h"

#include <stdlib.h>

typedef struct gr_entry_t gr_entry_t;

#define GR_PN_FLAG_SENT 1
#define GR_PN_FLAG_RECEIVED 2

typedef struct {
	uint8_t flags;
} gr_pn_flags_t;

struct gr_pn_t {
	uint64_t p;
	uint64_t n;
	zhashx_t* flags;	// nodeid => gr_pn_flags_t
};

struct gr_entry_t {
	zhashx_t* values;	// nodeid => gr_pn_t
};

struct gr_counter_t {
	pthread_mutex_t entries_lock[1];
	zhashx_t* entries;	// username => gr_entry_t
};

static gr_pn_t*
gr_pn_create(void)
{
	gr_pn_t* obj = malloc(sizeof(*obj));
	*obj = (gr_pn_t) { .flags = zhashx_new() };
	return obj;
}

static void
gr_pn_free(void** p)
{
	if (!p)
		return;
	gr_pn_t* obj = *p;
	if (!obj)
		return;
	zhashx_destroy(&obj->flags);
	*p = NULL;
	free(obj);
}

static gr_pn_flags_t*
gr_pn_flags_or_create(
	gr_pn_t* pn,
	const char* nodeid)
{
	gr_pn_flags_t* fp = zhashx_lookup(pn->flags, nodeid);
	if (!fp) {
		fp = calloc(1, sizeof(*fp));
		zhashx_insert(pn->flags, nodeid, fp);
	}
	return fp;
}

static void
gr_pn_merge(
	gr_pn_t* target,
	const gr_pn_t* other,
	const char* nodeid)
{
	if (!target || !other)
		return;
	gr_log(GR_LOG_DEBUG,
		"%s: target at %p has %llu - %llu, other at %p is %llu - %llu\n",
		__FUNCTION__,
		target,
		(long long) target->p,
		(long long) target->n,
		other,
		(long long) other->p,
		(long long) other->n);

	gr_pn_flags_t* fp = gr_pn_flags_or_create(target, nodeid);

	fp->flags |= GR_PN_FLAG_RECEIVED;
	if ((target->p == other->p) && (target->n == other->n))
		return;

	fp =  zhashx_first(target->flags);
	while (fp) {
		fp->flags &= ~GR_PN_FLAG_SENT;
		fp = zhashx_next(target->flags);
	}

	if (target->p < other->p)
		target->p = other->p;
	if (target->n < other->n)
		target->n = other->n;
}

static void
gr_pn_delta(
	gr_pn_t* pn,
	int64_t delta)
{
	if (!pn)
		return;
	if (delta > 0)
		pn->p += delta;
	else
		pn->n -= delta;
	zhashx_purge(pn->flags);
}

static gr_entry_t*
gr_entry_create(void)
{
	gr_entry_t* obj = malloc(sizeof(*obj));
	*obj = (gr_entry_t) { .values = zhashx_new() };
	zhashx_set_destructor(obj->values, gr_pn_free);
	return obj;
}

static void
gr_entry_free(void** p)
{
	if (!p)
		return;
	gr_entry_t* obj = *p;
	if (!obj)
		return;
	*p = NULL;
	zhashx_destroy(&obj->values);
	free(obj);
}

static int64_t
gr_entry_value(
	gr_entry_t* entry)
{
	if (!entry)
		return 0;

	int64_t value = 0;
	gr_pn_t* pn = zhashx_first(entry->values);
	while (pn) {
		const char* peer_id =  zhashx_cursor(entry->values);
		value += pn->p - pn->n;
		gr_log(GR_LOG_DEBUG, "%s: node `%s' has value %llu - %llu = %lld\n",
			__FUNCTION__,
			(const char*) peer_id,
			(long long) pn->p,
			(long long) pn->n,
			(long long) (pn->p - pn->n));
		pn = zhashx_next(entry->values);
	}
	gr_log(GR_LOG_DEBUG, "%s: entry at %p has total value %lld\n",
		__FUNCTION__, entry, (long long) value);
	return value;
}

static gr_pn_t*
gr_entry_pn_or_create(gr_entry_t* entry, const char* nodeid)
{
	gr_pn_t* pn = zhashx_lookup(entry->values, nodeid);
	if (!pn) {
		pn = gr_pn_create();
		zhashx_insert(entry->values, nodeid, pn);
	}
	return pn;
}

gr_counter_t*
gr_counter_create(void)
{
	gr_counter_t* obj = malloc(sizeof(*obj));
	*obj = (gr_counter_t) { .entries = zhashx_new() };
	pthread_mutex_init(obj->entries_lock, NULL);
	zhashx_set_destructor(obj->entries, gr_entry_free);
//	gr_log(GR_LOG_DEBUG, "%s: obj at %p entries at %p\n", __FUNCTION__, obj, obj->entries);
	return obj;
}

void
gr_counter_free(void** p)
{
	if (!p)
		return;
	gr_counter_t* obj = *p;
	if (!obj)
		return;
	*p = NULL;
//	gr_log(GR_LOG_DEBUG, "%s: obj at %p entries at %p\n", __FUNCTION__, obj, obj->entries);
	pthread_mutex_lock(obj->entries_lock);
	zhashx_destroy(&obj->entries);
	pthread_mutex_unlock(obj->entries_lock);
	pthread_mutex_destroy(obj->entries_lock);
	free(obj);
}

static gr_entry_t*
gr_counter_lookup_or_create(gr_counter_t* counter, const char* key)
{
	gr_entry_t* entry = zhashx_lookup(counter->entries, key);
	if (!entry) {
		entry = gr_entry_create();
		zhashx_insert(counter->entries, key, entry);
	}
	gr_log(GR_LOG_DEBUG, "%s: counter at %p returns entry at %p for key `%s'\n",
		__FUNCTION__, counter, entry, key);
	return entry;
}

static void
gr_counter_set_locked(
	gr_counter_t* counter,
	const char* key,
	const char* nodeid,
	gr_pn_t* value)
{
	if (!counter || !key || !value || !nodeid)
		return;

	gr_entry_t* entry = gr_counter_lookup_or_create(counter, key);
	gr_log(GR_LOG_DEBUG, "%s: entry at %p for node `%s' gets local value %llu - %llu = %lld\n",
		__FUNCTION__, entry,
		nodeid,
		(long long) value->p,
		(long long) value->n,
		(long long) (value->p - value->n));
	gr_pn_t* pn = gr_entry_pn_or_create(entry, nodeid);
	pn->p = value->p;
	pn->n = value->n;
	zhashx_purge(pn->flags);
}

void
gr_counter_set(
	gr_counter_t* counter,
	const char* key,
	const char* nodeid,
	uint64_t value_p,
	uint64_t value_n)
{
	gr_pn_t value;

	if (!counter || !key || !nodeid)
		return;

	value.p = value_p;
	value.n = value_n;
	pthread_mutex_lock(counter->entries_lock);
	gr_counter_set_locked(counter, key, nodeid, &value);
	pthread_mutex_unlock(counter->entries_lock);
}

void
gr_counter_delta(
	gr_counter_t* counter,
	const char* key,
	const char* nodeid,
	int64_t diff)
{
	if (!counter || !key || !nodeid || !diff)
		return;

	pthread_mutex_lock(counter->entries_lock);
	gr_entry_t* entry = gr_counter_lookup_or_create(counter, key);
	gr_log(GR_LOG_DEBUG, "%s: entry for `%s' at %p\n", __FUNCTION__, key, entry);
	gr_pn_t* pn = gr_entry_pn_or_create(entry, nodeid);
	gr_log(GR_LOG_DEBUG, "%s: pn for `%s' at %p\n", __FUNCTION__, nodeid, pn);
	gr_pn_delta(pn, diff);

	gr_log(GR_LOG_DEBUG,
		"%s: entry at %p changed value by %lld to %llu - %llu = %lld\n",
		__FUNCTION__,
		entry,
		(long long) diff,
		(long long) pn->p,
		(long long) pn->n,
		(long long) (pn->p - pn->n));

	pthread_mutex_unlock(counter->entries_lock);
}

int64_t
gr_counter_value(
	gr_counter_t* counter,
	const char* key)
{
	if (!counter || !key)
		return 0;

	int64_t value = 0;
	pthread_mutex_lock(counter->entries_lock);
	gr_entry_t* entry = zhashx_lookup(counter->entries, key);
	if (entry) {
		value = gr_entry_value(entry);
	}
	pthread_mutex_unlock(counter->entries_lock);
	return value;
}

static int
gr_pn_serialize(
	gr_pn_t* pn,
	zchunk_t* chunk,
	const char* peer_id,
	const char* target_peer_id)
{
	if (!pn || !chunk || !peer_id)
		return 0;

	if (target_peer_id) {
		gr_pn_flags_t* fp = gr_pn_flags_or_create(pn, target_peer_id);
		if ((fp->flags & (GR_PN_FLAG_SENT | GR_PN_FLAG_RECEIVED)) ==
				(GR_PN_FLAG_SENT | GR_PN_FLAG_RECEIVED)) {
			gr_log(GR_LOG_DEBUG,
				"%s: peer `%s' already has %llu - %llu = %lld and flags %d for node `%s'\n",
				__FUNCTION__, target_peer_id,
				(long long) pn->p,
				(long long) pn->n,
				(long long) (pn->p - pn->n),
				fp->flags,
				peer_id);
			return 0;
		}
		fp->flags |= GR_PN_FLAG_SENT;
		gr_log(GR_LOG_DEBUG,
			"%s: sending %llu - %llu = %lld for node `%s' to peer `%s'\n",
			__FUNCTION__,
			(long long) pn->p,
			(long long) pn->n,
			(long long) (pn->p - pn->n),
			peer_id,
			target_peer_id);
	} else {
		gr_log(GR_LOG_DEBUG,
			"%s: sending %llu - %llu = %lld for node `%s'\n",
			__FUNCTION__,
			(long long) pn->p,
			(long long) pn->n,
			(long long) (pn->p - pn->n),
			peer_id);
	}

	gr_event_append_string(chunk, peer_id, -1);
	gr_event_append_int64(chunk, pn->p);
	gr_event_append_int64(chunk, pn->n);

	return 1;
}

static int
gr_entry_serialize(
	gr_entry_t* entry,
	zchunk_t* chunk,
	const char* username,
	const char* target_peer_id)
{
	int any = 0;

	if (!entry || !chunk || !username)
		return 0;
	zchunk_t* sub_chunk = zchunk_new(NULL, 256);
	gr_pn_t* pn = zhashx_first(entry->values);
	while (pn) {
		const char* peer_id = zhashx_cursor(entry->values);
		if (gr_pn_serialize(pn, sub_chunk, peer_id, target_peer_id)) {
			if (!any) {
				gr_log(GR_LOG_DEBUG, "%s: entry `%s' has changed value\n",
					__FUNCTION__, username);
				gr_event_append_string(chunk, username, -1);
				any = 1;
			}
			zchunk_extend(chunk, zchunk_data(sub_chunk), zchunk_size(sub_chunk));
		}
		pn = zhashx_next(entry->values);
	}
	zchunk_destroy(&sub_chunk);
	if (any) {
		gr_event_append_string(chunk, NULL, -1);	// last peer_id
	}
	return any;
}

int
gr_counter_serialize(
	gr_counter_t* counter,
	zchunk_t* chunk,
	const char* counter_name,
	const char* target_peer_id)
{
	int any = 0;

	if (!counter || !chunk || !counter_name)
		return 0;
	zchunk_t* sub_chunk = zchunk_new(NULL, 256);
	pthread_mutex_lock(counter->entries_lock);
	gr_entry_t* entry = zhashx_first(counter->entries);
	while (entry) {
		const char* username = zhashx_cursor(counter->entries);
		if (gr_entry_serialize(entry, sub_chunk, username, target_peer_id)) {
			if (!any) {
				gr_log(GR_LOG_DEBUG, "%s: counter `%s' has changed value\n",
					__FUNCTION__, counter_name);
				gr_event_append_string(chunk, counter_name, -1);
				any = 1;
			}
			zchunk_extend(chunk, zchunk_data(sub_chunk), zchunk_size(sub_chunk));
		}
		entry = zhashx_next(counter->entries);
	}
	pthread_mutex_unlock(counter->entries_lock);
	zchunk_destroy(&sub_chunk);
	if (any) {
		gr_event_append_string(chunk, NULL, -1);	// last username
	}
	return any;
}

int
gr_counter_deserialize(gr_counter_t* counter, const byte** cpp, int* sp)
{
	int csize = sp ? *sp : 0;
	gr_pn_t value;
	const byte* cdata = cpp ? *cpp : NULL;
	char* username = NULL;
	char* peer_id = NULL;

	// [ username [ peer_id p/n ]* ]*

	for (;;) {
		if (gr_event_deserialize_string(&cdata, &csize, &username) < 0)
			return -1;
		if (!username)
			break;

		gr_entry_t* entry = gr_counter_lookup_or_create(counter, username);
		for (;;) {
			if (gr_event_deserialize_string(&cdata, &csize, &peer_id) < 0) {
				free(username);
				return -1;
			}
			if (!peer_id)
				break;

			memset(&value, 0, sizeof(value));
			if (gr_event_deserialize_int64(&cdata, &csize, (int64_t*) &value.p) < 0) {
				free(username);
				free(peer_id);
				return -1;
			}
			if (gr_event_deserialize_int64(&cdata, &csize, (int64_t*) &value.n) < 0) {
				free(username);
				free(peer_id);
				return -1;
			}

			gr_log(GR_LOG_DEBUG,
				"%s: found username `%s' / peer `%s' with value %llu - %llu = %lld\n",
				__FUNCTION__,
				username,
				peer_id,
				(long long) value.p,
				(long long) value.n,
				(long long) (value.p - value.n));

			gr_pn_t* pn = gr_entry_pn_or_create(entry, peer_id);
			gr_log(GR_LOG_DEBUG, "%s: pn for `%s' at %p\n", __FUNCTION__, peer_id, pn);
			pn->p = value.p;
			pn->n = value.n;
		}
		free(peer_id);
		peer_id = NULL;
	}
	free(username);

	*cpp = cdata;
	*sp = csize;
	return 0;
}

void
gr_counter_merge(
	gr_counter_t* counter,
	gr_counter_t* cinfo,
	gr_peer_t* peer,	// => nodeid
	const char* my_id)
{
	const char* nodeid = gr_peer_get_id(peer);
	gr_log(GR_LOG_DEBUG, "%s: node `%s' merges data from node `%s'\n",
		__FUNCTION__, my_id, nodeid);

	pthread_mutex_lock(counter->entries_lock);
	gr_entry_t* entry = zhashx_first(cinfo->entries);
	while (entry) {
		const char* username = zhashx_cursor(cinfo->entries);
		gr_log(GR_LOG_DEBUG, "%s: entry for user `%s' at %p\n",
			__FUNCTION__, username, entry);
		gr_entry_t* my_entry = gr_counter_lookup_or_create(counter, username);
		gr_pn_t* pn = zhashx_first(entry->values);
		while (pn) {
			const char* peer_id = zhashx_cursor(entry->values);
			gr_pn_t* my_pn = gr_entry_pn_or_create(my_entry, peer_id);
			gr_log(GR_LOG_DEBUG, "%s: merge value for node `%s' to pn at %p\n",
				__FUNCTION__, peer_id, my_pn);
			gr_pn_merge(my_pn, pn, nodeid);
			pn = zhashx_next(entry->values);
		}
		entry = zhashx_next(cinfo->entries);
	}
	pthread_mutex_unlock(counter->entries_lock);
}

