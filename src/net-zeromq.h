#ifndef _GR_NET_ZEROMQ_H
#define _GR_NET_ZEROMQ_H 1

#include "georep.h"
#include "net-base.h"

#include "czmq.h"

void
gr_zeromq_setup(gr_net_t* config);

void*
gr_zeromq_init(georep_t* queue);

void
gr_zeromq_close(void** op);

void*
gr_zeromq_peer_init(gr_peer_t* peer);

void
gr_zeromq_peer_free(void** op);

void
gr_zeromq_peer_close(void* op);

int
gr_zeromq_peer_send_chunk(void* p, zchunk_t* chunk);

#endif

