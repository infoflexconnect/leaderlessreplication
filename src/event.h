#ifndef GQ_EVENT_H
#define GQ_EVENT_H 1

#include "georep.h"

#include "czmq.h"

#include "pbuf.h"

typedef struct {
	gr_cmd_t cmd;
	int batch_id;
	char* address;
	char* id;
	zlistx_t* peers;
	zlistx_t* items;
	pbuf_t* batch_ids;
	zhashx_t* counters;
} gr_event_t;

zchunk_t*
gr_event_create_chunk(georep_t* queue, byte b);

zchunk_t*
gr_event_create_hello(georep_t* queue);

zchunk_t*
gr_event_create_heartbeat(georep_t* queue);

gr_event_t*
gr_event_create(gr_cmd_t cmd);

void
gr_event_free(gr_event_t** ep);

void
gr_event_append_byte(zchunk_t* chunk, byte value);

void
gr_event_append_int(zchunk_t* chunk, int value);

void
gr_event_append_int64(zchunk_t* chunk, int64_t value);

void
gr_event_append_string(zchunk_t* chunk, const char* str, int len);

void
gr_event_append_zchunk(zchunk_t* chunk, zchunk_t* data);

typedef void (gr_event_append_element_t)(const void* obj, zchunk_t* chunk);

void
gr_event_append_list(zchunk_t* chunk, zlistx_t* items,
	gr_event_append_element_t* append_element);

void
gr_event_append_pbuf(
	zchunk_t* chunk,
	pbuf_t* items,
	gr_event_append_element_t* append_element);

gr_event_t*
gr_event_parse(const byte** cdata_p, int* csize_p);

int
gr_event_deserialize_byte(const byte** cpp, int* sp, byte* destp);

int
gr_event_deserialize_int(const byte** cpp, int* sp, int* destp);

int
gr_event_deserialize_int64(const byte** cpp, int* sp, int64_t* destp);

int
gr_event_deserialize_string(const byte** cpp, int* sp, char** destp);

int
gr_event_deserialize_zchunk(const byte** cpp, int* sp, zchunk_t** destp);

#endif

