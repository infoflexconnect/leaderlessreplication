#ifndef GQ_COUNTER_H
#define GQ_COUNTER_H 1

#include <stdint.h>

#include "pbuf.h"
#include "czmq.h"

// to peerdef.h
typedef struct gr_pn_t gr_pn_t;
typedef struct gr_peer_t gr_peer_t;
typedef struct gr_counter_t gr_counter_t;

gr_counter_t*
gr_counter_create(void);

void
gr_counter_free(void** p);

void
gr_counter_set(
	gr_counter_t* counter,
	const char* key,
	const char* nodeid,
	uint64_t value_p,
	uint64_t value_n);

void
gr_counter_delta(
	gr_counter_t* counter,
	const char* key,
	const char* nodeid,
	int64_t diff);

int64_t
gr_counter_value(
	gr_counter_t* counter,
	const char* key);

int
gr_counter_serialize(
	gr_counter_t* counter,
	zchunk_t* chunk,
	const char* counter_name,
	const char* target_peer_id);

int
gr_counter_deserialize(
	gr_counter_t* counter,
	const byte** cpp,
	int* sp);

void
gr_counter_merge(
	gr_counter_t* counter,
	gr_counter_t* cinfo,
	gr_peer_t* peer,
	const char* name);

#endif

