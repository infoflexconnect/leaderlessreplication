#ifndef _GR_NET_BASE_H
#define _GR_NET_BASE_H 1

#include "georep.h"

#include "czmq.h"

typedef void* (*gr_net_init_t)(georep_t* queue);

typedef void (*gr_net_close_t)(void** op);

typedef void* (*gr_net_peer_init_t)(gr_peer_t* peer);

typedef void (*gr_net_peer_free_t)(void** op);

typedef void (*gr_net_peer_close_t)(void* op);

typedef int (*gr_net_peer_send_chunk_t)(void* p, zchunk_t* chunk);

struct gr_net_t {
	gr_net_init_t net_init;
	gr_net_close_t net_close;
	gr_net_peer_init_t net_peer_init;
	gr_net_peer_free_t net_peer_free;
	gr_net_peer_close_t net_peer_close;
	gr_net_peer_send_chunk_t net_peer_send_chunk;
};

#endif

