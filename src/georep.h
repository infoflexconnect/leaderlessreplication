#ifndef GEOQUEUE_H
#define GEOQUEUE_H

#include <stdint.h>

#include "czmq.h"

#include "cond.h"
#include "pbuf.h"

#include "counter.h"
#include "peer_object_info.h"


/*
 * Public interface for GeoQueue.
 *
 * https://github.com/basic70/georep
 */

// Types

typedef struct georep_t georep_t;
typedef struct gr_peer_t gr_peer_t;
typedef struct gr_net_t gr_net_t;
typedef struct gr_pn_t gr_pn_t;

typedef enum {
	GR_CMD_HELLO,
	GR_CMD_HEARTBEAT,
	GR_CMD_COUNTER,
} gr_cmd_t;

// Functions

georep_t*
gr_init(const char* address, const char* nodeid);

georep_t*
gr_init2(const char* address, const char* nodeid, gr_net_t* queueconfig);
// + basedir, callbacks

const char*
gr_my_id(georep_t* queue);

void
gr_tolerate_failures(georep_t* queue, uint_least8_t f);

void
gr_set_high_watermark(georep_t* queue, uint_least8_t n);

uint_least8_t
gr_high_watermark(georep_t* queue);

void
gr_set_min_rtt(georep_t* queue, int32_t n);

uint64_t
gr_next_seqno(georep_t* queue);

void
gr_shutdown(georep_t** queuep);

int
gr_contact(georep_t* queue, const char* address, int* clustersize);

void
georep_append_event_params(georep_t* queue, zchunk_t* chunk, gr_cmd_t cmd);

const char*
gr_get_address(georep_t* queue);

typedef void gr_adopted_cb_t(void* arg, const char* key, zchunk_t* value);

void
gr_set_adopted_cb(georep_t* queue, gr_adopted_cb_t* func, void* arg);

gr_cond_t*
gr_replicate(georep_t* queue, const char* key, const char* value, int value_len);

gr_cond_t*
gr_update(georep_t* queue, const char* key, const char* value, int value_len);

gr_cond_t*
gr_clear(georep_t* queue, const char* key);

void
gr_signal_keys(georep_t* queue, pbuf_t* keys);

void
gr_unreachable_peer(georep_t* queue, gr_peer_t* peer);

void
gr_adopt(georep_t* queue, gr_peer_object_info_t** infop);

void
gr_persist(georep_t* queue, const char* key, int keylen, const char* value, int valuelen, int create_if_new);

gr_counter_t*
gr_create_counter(georep_t* queue, const char* name);

void
gr_set_counter(
	georep_t* queue,
	const char* name,
	const char* key,
	uint64_t value_p,
	uint64_t value_n);

void
gr_delta_counter(georep_t* queue, const char* name, const char* key, int64_t diff);

int64_t
gr_get_counter_value(georep_t* queue, const char* name, const char* key);

void
gr_use_plain_pn(
	georep_t* queue,
	uint_least8_t flag);

uint_least8_t
gr_using_plain_pn(georep_t* queue);

void
gr_replicate_by_id(georep_t* queue);

// internal helpers
gr_peer_t*
gr_by_id(georep_t* queue, const char* id);

int
gr_handle_incoming(georep_t* queue, const byte** cpp, int* sp);

int
georep_sync(georep_t* queue);

const gr_net_t*
gr_net_config(georep_t* queue);

#endif

