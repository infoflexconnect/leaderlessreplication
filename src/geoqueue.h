#ifndef GEOQUEUE_H
#define GEOQUEUE_H

#include <stdint.h>

#include "czmq.h"

#include "cond.h"
#include "pbuf.h"

#include "peer_object_info.h"

/*
 * Public interface for GeoQueue.
 *
 * https://github.com/basic70/geoqueue
 */

// Types

typedef struct geoqueue_t geoqueue_t;
typedef struct gq_peer_t gq_peer_t;

typedef enum {
	GQ_CMD_HELLO,
	GQ_CMD_HEARTBEAT,
} gq_cmd_t;

// Functions

geoqueue_t*
gq_init(const char* address, const char* nodeid);
// + basedir, callbacks

const char*
gq_my_id(geoqueue_t* queue);

void
gq_tolerate_failures(geoqueue_t* queue, uint_least8_t f);

void
gq_set_high_watermark(geoqueue_t* queue, uint_least8_t n);

uint_least8_t
gq_high_watermark(geoqueue_t* queue);

uint64_t
gq_next_seqno(geoqueue_t* queue);

void
gq_shutdown(geoqueue_t** gqp);

int
gq_contact(geoqueue_t* gq, const char* address, int* clustersize);

void
geoqueue_append_event_params(geoqueue_t* queue, zchunk_t* chunk, gq_cmd_t cmd);

const char*
gq_get_address(geoqueue_t* gq);

typedef void gq_adopted_cb_t(void* arg, const char* key, zchunk_t* value);

void
gq_set_adopted_cb(geoqueue_t* queue, gq_adopted_cb_t* func, void* arg);

gq_cond_t*
gq_replicate(geoqueue_t* queue, const char* key, const char* value, int value_len);

gq_cond_t*
gq_update(geoqueue_t* queue, const char* key, const char* value, int value_len);

gq_cond_t*
gq_clear(geoqueue_t* queue, const char* key);

void
gq_signal_keys(geoqueue_t* queue, pbuf_t* keys);

void
gq_unreachable_peer(geoqueue_t* queue, gq_peer_t* peer);

void
gq_adopt(geoqueue_t* queue, gq_peer_object_info_t* info);

void
gq_persist(geoqueue_t* queue, const char* key, int keylen, const char* value, int valuelen, int create_if_new);

// a bit too internal
gq_peer_t*
gq_by_id(geoqueue_t* queue, const char* id);

#endif

