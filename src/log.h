
typedef enum {
	GR_LOG_ALL,
	GR_LOG_DEBUG,
	GR_LOG_INFO,
	GR_LOG_NOTHING,
} gr_log_level_t;

void
gr_log_init(void);

void
gr_log_level(gr_log_level_t level);

int
gr_log_enabled(gr_log_level_t level);

extern void gr_log(gr_log_level_t level, const char* , ...)
# ifdef __GNUC__
    __attribute__ ((format(printf,2,3)))
# endif
	    ;

