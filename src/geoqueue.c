
#include "geoqueue.h"
#include "peer.h"
#include "event.h"
#include "log.h"
#include "pbuf.h"

#include "zmq.h"
#include "czmq.h"

#include <leveldb/c.h>

#include <stdio.h>
#include <stdlib.h>

#define GQ_FLAG_NEW_PEER 1

struct geoqueue_t {
	int flags;
	uint_least8_t tolerated_failures;
	uint_least8_t high_watermark;
	uint64_t batch_seqno;
	char* address;
	char* nodeid;
	void* context;
	zsock_t* socket;
	zsock_t* internal_push_socket;
	zsock_t* internal_pull_socket;
	void* loop;
	void* loop_thread;

	pthread_mutex_t seqno_lock[1];

	// TODO: split into "active" and "unactive"?
	pthread_mutex_t peers_lock[1];
	zlistx_t* peers;

	pthread_mutex_t conds_lock[1];
	zhashx_t* conds;

	pthread_mutex_t replica_lock[1];
	zhashx_t* replicas;	// message id => pbuf_t with gq_peer_t (or peer id?)

	leveldb_t* ldb;
	leveldb_readoptions_t* readoptions;
	leveldb_writeoptions_t* writeoptions;

	gq_adopted_cb_t* adopted_cb;
	void* adopted_arg;
};

static geoqueue_t*
geoqueue_create(const char* address, const char* id)
{
	if (!address) {
		gq_log(GQ_LOG_INFO, "%s: address can not be NULL\n", __FUNCTION__);
		return NULL;
	}

	void* context = zmq_ctx_new();
	if (!context) {
		gq_log(GQ_LOG_INFO, "%s: zmq_ctx_new returned NULL\n", __FUNCTION__);
		return NULL;
	}

	geoqueue_t* queue = calloc(1, sizeof(*queue));
	if (id) {
		char* errptr = NULL;
		char ldb_filename[256];
		leveldb_options_t* options = leveldb_options_create();
		leveldb_options_set_create_if_missing(options, 1);
		sprintf(ldb_filename, "node-%s.ldb", id);
		queue->ldb = leveldb_open(options, ldb_filename, &errptr);
		leveldb_options_destroy(options);
		if (errptr) {
			fprintf(stderr, "node %s: could not open `%s': %s\n",
				id, ldb_filename, errptr);
			free(errptr);
			free(queue);
			return NULL;
		}
		queue->readoptions = leveldb_readoptions_create();
		queue->writeoptions = leveldb_writeoptions_create();
	} else {
		queue->ldb = NULL;
		queue->readoptions = NULL;
		queue->writeoptions = NULL;
	}

	zsock_t* socket = zsock_new_pull(address);
	if (!socket) {
		gq_log(GQ_LOG_INFO, "%s: zsock_new_pull `%s' returned NULL\n",
			__FUNCTION__, address);
		return NULL;
	}

	queue->tolerated_failures = 2;
	queue->high_watermark = 1;
	queue->address = strdup(address);
	if (id)
		queue->nodeid = strdup(id);
	queue->context = context;
	queue->socket = socket;

	queue->batch_seqno = 1;
	pthread_mutex_init(queue->seqno_lock, NULL);

	queue->peers = gq_peer_list_create();
	pthread_mutex_init(queue->peers_lock, NULL);
	queue->conds = zhashx_new();
	pthread_mutex_init(queue->conds_lock, NULL);

	queue->replicas = zhashx_new();
	//zhashx_set_destructor(queue->replicas, pbuf_free_p);
	pthread_mutex_init(queue->replica_lock, NULL);

	char inproc_name[64];
	sprintf(inproc_name, "inproc://%llx", (long long unsigned int) queue);
	queue->internal_pull_socket = zsock_new_pull(inproc_name);
	queue->internal_push_socket = zsock_new_push(inproc_name);
#if 0
	gq_log(GQ_LOG_INFO, "%s: gq at `%s' has inproc socket at `%s'\n",
		__FUNCTION__, address, inproc_name);
	gq_log(GQ_LOG_INFO,
		"%s: gq at %p has push_socket at %p and pull_socket at %p\n",
		__FUNCTION__, queue,
		queue->internal_push_socket, queue->internal_pull_socket);
#endif
	return queue;
}

static void
geoqueue_show_rtt(geoqueue_t* obj)
{
	if (!obj->peers)
		return;

	gq_peer_t* peer = zlistx_first(obj->peers);
	while (peer) {
		gq_peer_show_medians(peer, obj->nodeid);
		peer = zlistx_next(obj->peers);
	}
}

static void
geoqueue_destroy(geoqueue_t** qp)
{
	if (!qp)
		return;
	geoqueue_t* obj = *qp;
	if (!obj)
		return;
	*qp = NULL;
#if 1
	gq_log(GQ_LOG_INFO, "%s: gq at %p has push_socket at %p, sizeof(replicas) = %ld\n",
		__FUNCTION__, obj, obj->internal_push_socket, zhashx_size(obj->replicas));
#endif
	zmq_ctx_term(obj->context);
	zframe_t* frame = zframe_new("X", 1);
	/* int rc = */ zframe_send(&frame, obj->internal_push_socket, 0);
	sleep(1);

	gq_log(GQ_LOG_INFO, "%s: gq at %p deletes sockets and locks\n",
		__FUNCTION__, obj);

	if (obj->ldb)
		leveldb_close(obj->ldb);
	if (obj->readoptions)
		leveldb_readoptions_destroy(obj->readoptions);
	if (obj->writeoptions)
		leveldb_writeoptions_destroy(obj->writeoptions);

	pthread_mutex_destroy(obj->seqno_lock);

	zsock_destroy(&obj->socket);
	zsock_destroy(&obj->internal_push_socket);
	zsock_destroy(&obj->internal_pull_socket);

	pthread_mutex_lock(obj->peers_lock);
	geoqueue_show_rtt(obj);
	zlistx_destroy(&obj->peers);
	pthread_mutex_unlock(obj->peers_lock);
	pthread_mutex_destroy(obj->peers_lock);

	pthread_mutex_lock(obj->conds_lock);
	zhashx_destroy(&obj->conds);
	pthread_mutex_unlock(obj->conds_lock);
	pthread_mutex_destroy(obj->conds_lock);

	pthread_mutex_lock(obj->replica_lock);

	// TODO: explicit delete of the values
	zhashx_destroy(&obj->replicas);

	pthread_mutex_unlock(obj->replica_lock);
	pthread_mutex_destroy(obj->replica_lock);

	zmq_threadclose(obj->loop);

	free(obj->address);
	free(obj->nodeid);
	free(obj);
}

const char*
gq_my_id(geoqueue_t* queue)
{
	return queue ? queue->nodeid : NULL;
}

void
gq_tolerate_failures(geoqueue_t* queue, uint_least8_t f)
{
	queue->tolerated_failures = f;
}

void
gq_set_high_watermark(geoqueue_t* queue, uint_least8_t n)
{
	queue->high_watermark = n > 0 ? n : 1;
}

uint_least8_t
gq_high_watermark(geoqueue_t* queue)
{
	return queue->high_watermark;
}

uint64_t
gq_next_seqno(geoqueue_t* queue)
{
	if (!queue) return -1;
	pthread_mutex_lock(queue->seqno_lock);
	uint64_t v = queue->batch_seqno++;
	pthread_mutex_unlock(queue->seqno_lock);
	return v;
}

static gq_peer_t*
gq_find_or_create_peer(geoqueue_t* queue,
	const char* address, const char* id,
	int got_data)
{
	if (!queue || !address)
		return NULL;

	gq_peer_t* peer = gq_peer_by_address(queue->peers, address);

	if (peer) {
		gq_peer_set_id(peer, id);
	} else {
		peer = gq_peer_create_with_id(queue, address, id);
		if (!peer) {
			gq_log(GQ_LOG_INFO,
				"%s: queue at `%s' can not create peer at `%s'\n",
				__FUNCTION__, queue->address, address);
			return NULL;
		}
		zlistx_add_end(queue->peers, peer);
		queue->flags |= GQ_FLAG_NEW_PEER;
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' adds new peer at `%s'\n",
			__FUNCTION__, queue->address, address);
	}

	if (got_data) {
		int became_active = 0;
		gq_peer_got_data(peer, &became_active, queue->peers);
		if (became_active) {
			queue->flags |= GQ_FLAG_NEW_PEER;
			gq_log(GQ_LOG_INFO, "%s: queue at `%s' has confirmed peer at `%s'\n",
				__FUNCTION__, queue->address, address);
		}
	}

	return peer;
}

static void
geoqueue_handle_event(geoqueue_t* queue, gq_event_t* event)
{
	size_t nobjects;
	long npeers;
	gq_peer_t* peer;

	if (!queue || !event)
		return;

	pthread_mutex_lock(queue->peers_lock);
	peer = gq_find_or_create_peer(queue, event->address, event->id, 1);
	qg_peer_update_theta(peer, queue->peers);
	pthread_mutex_unlock(queue->peers_lock);

	switch (event->cmd) {
		case GQ_CMD_HELLO:
			npeers = event->peers ? zlistx_size(event->peers) : 0;
			gq_log(GQ_LOG_INFO,
				"%s: queue at `%s' got a hello from a peer at `%s' with id `%s', with %ld extra peer%s\n",
				__FUNCTION__, queue->address, event->address, event->id,
				npeers, npeers == 1 ? "" : "s");

			// update peer list
			peer = event->peers ? zlistx_first(event->peers) : NULL;
			pthread_mutex_lock(queue->peers_lock);
			while (peer) {
				const char* address = gq_peer_get_address(peer);
				if (address && (strcmp(queue->address, address) == 0)) {
					gq_log(GQ_LOG_INFO, "%s: good, peer `%s' knows about us (`%s')\n",
						__FUNCTION__, event->address, address);
				} else {
					(void) gq_find_or_create_peer(queue,
						gq_peer_get_address(peer), gq_peer_get_id(peer), 0);
				}
				peer = zlistx_next(event->peers);
			}
			pthread_mutex_unlock(queue->peers_lock);
			break;

		case GQ_CMD_HEARTBEAT:
			nobjects = event->items ? zlistx_size(event->items) : 0;
			if (nobjects > 0) {
				gq_log(GQ_LOG_INFO, "%s: got %ld objects to store as batch %d\n",
					__FUNCTION__, nobjects, event->batch_id);
				gq_peer_save_items(peer, event->batch_id, event->items);
			}
			gq_peer_confirmed_batches(peer, event->batch_ids);
			break;

		default:
			;
	}
	struct timeval now;
	gettimeofday(&now, NULL);
	gq_peer_update(peer, &now, queue->flags & GQ_FLAG_NEW_PEER, -1);
}

static int
gq_reader(zloop_t* loop, zsock_t* reader, void* arg)
{
	int rc = 0;
	geoqueue_t* queue = arg;
	gq_event_t* event;

	(void) loop;

#if 1
	gq_log(GQ_LOG_DEBUG,
		"%s: data available to node `%s' on `%s' to reader %p socket %p internal_socket %p\n",
		__FUNCTION__, queue->nodeid ? queue->nodeid : "-", queue->address,
		reader, queue->socket, queue->internal_pull_socket);
#endif

	zframe_t* frame = zframe_recv(reader);
	if (!frame)
		return 0;

	if (gq_log_enabled(GQ_LOG_DEBUG)) {
		char* hf = zframe_strhex(frame);
		gq_log(GQ_LOG_DEBUG, "%s: queue at `%s' read: %s\n", __FUNCTION__, queue->address, hf);
		free(hf);
	}
	int csize = zframe_size(frame);
	const byte* cdata = zframe_data(frame);
	if (reader == queue->socket) {
		while (csize > 0) {
			event = gq_event_parse(&cdata, &csize);
			if (!event) {
				// TODO: close socket, we can not continue reading anyway
				break;
			}
			geoqueue_handle_event(queue, event);
			gq_event_free(&event);
		}
	} else if (reader == queue->internal_pull_socket) {
		gq_log(GQ_LOG_DEBUG, "%s: csize %d cdata[0] `%c'\n",
			__FUNCTION__, csize, cdata[0]);
		if ((csize > 0) && (cdata[0] == 'X'))
			rc = -1;
	}

	zframe_destroy(&frame);

	if (rc < 0) {
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' will exit\n",
			__FUNCTION__, queue->address);
	}
	return rc;
}

void
gq_unreachable_peer(geoqueue_t* queue, gq_peer_t* peer)
{
	const char* key;
	zhashx_t* items;
	gq_peer_object_info_t* info;

	gq_log(GQ_LOG_INFO, "%s: queue at `%s' thinks peer at `%s' is dead\n",
		__FUNCTION__, queue->address, gq_peer_get_address(peer));

	if (!queue->adopted_cb)
		return;

	// possible dead, wait for majority, etc...

	// TODO: move this loop to peer.c? of course, it's not supposed to exist anyway
	//
	items = gq_peer_get_items(peer);
	info = zhashx_first(items);
	while (info) {
		key = zhashx_cursor(items);
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' adopts `%s' from peer `%s'\n",
			__FUNCTION__, queue->address, key, gq_peer_get_address(peer));
		queue->adopted_cb(queue->adopted_arg, key, info->value);
		info = zhashx_next(items);
	}
}

static int
gq_sync(zloop_t* loop, int loop_id, void* arg)
{
	struct timeval now;
	geoqueue_t* queue = arg;
	gq_peer_t* peer;
	zlistx_t* list;

	(void) loop;
	(void) loop_id;

	gettimeofday(&now, NULL);
	pthread_mutex_lock(queue->peers_lock);
	if (!queue->peers) {
		pthread_mutex_unlock(queue->peers_lock);
		return 0;
	}
	int nactive = gq_peer_nactive(queue->peers);
#if 1
	gq_log(GQ_LOG_INFO,
		"%s: enter for queue at `%s' having a total of %ld peers, %d being active\n",
		__FUNCTION__, queue->address, zlistx_size(queue->peers), nactive);
#endif
	list = zlistx_dup(queue->peers);
	pthread_mutex_unlock(queue->peers_lock);
	zlistx_set_destructor(list, NULL);
	peer = zlistx_first(list);
	while (peer) {
		// TODO: broadcast all new prospects?
		gq_peer_update(peer, &now, queue->flags & GQ_FLAG_NEW_PEER, nactive);
		peer = zlistx_next(list);
	}
	zlistx_destroy(&list);
	queue->flags &= ~GQ_FLAG_NEW_PEER;

	return 0;
}

static void
gq_loop(void* arg)
{
	int rc;
	geoqueue_t* queue = arg;
	zloop_t* loop;

	gq_log(GQ_LOG_INFO, "%s: enter for node `%s' at `%s'\n",
		__FUNCTION__, queue->nodeid ? queue->nodeid : "", queue->address);

	loop = zloop_new();

	// start readers
	rc = zloop_reader(loop, queue->socket, gq_reader, queue);
	if (rc < 0) {
		gq_log(GQ_LOG_INFO, "%s: reader on `%s', returned %d\n",
			__FUNCTION__, queue->address, rc);
	}
	rc = zloop_reader(loop, queue->internal_pull_socket, gq_reader, queue);
	if (rc < 0) {
		gq_log(GQ_LOG_INFO, "%s: internal reader returned %d\n",
			__FUNCTION__, rc);
	}

	zloop_timer(loop, 1 * 10, 0, gq_sync, queue);

	zloop_start(loop);

	zloop_destroy(&loop);
#if 1
	gq_log(GQ_LOG_INFO, "%s: leave for node `%s' at `%s'\n",
		__FUNCTION__, queue->nodeid ? queue->nodeid : "", queue->address);
#endif
}

geoqueue_t*
gq_init(const char* address, const char* nodeid)
{
	geoqueue_t* queue = geoqueue_create(address, nodeid);
	if (queue) {
		queue->loop = zmq_threadstart(gq_loop, queue);
	}

	return queue;
}

void
gq_shutdown(geoqueue_t** qp)
{
	geoqueue_destroy(qp);
}

const char*
gq_get_address(geoqueue_t* gq)
{
	return gq ? gq->address : NULL;
}

int
gq_contact(geoqueue_t* queue, const char* address, int* clustersize)
{
	if (address) {
		gq_log(GQ_LOG_INFO, "%s: node at `%s' contacts node at `%s'\n",
			__FUNCTION__, queue->address, address);
		pthread_mutex_lock(queue->peers_lock);
		(void) gq_find_or_create_peer(queue, address, NULL, 0);
		pthread_mutex_unlock(queue->peers_lock);
	}

	if (clustersize) {
		int nactive = 0;

		pthread_mutex_lock(queue->peers_lock);
		gq_peer_t* peer = zlistx_first(queue->peers);
		while (peer) {
			gq_peer_increment_if_active(peer, &nactive);
			peer = zlistx_next(queue->peers);
		}
		pthread_mutex_unlock(queue->peers_lock);

#if 1
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' has %d active peer%s\n",
			__FUNCTION__, queue->address, nactive, nactive == 1 ? "" : "s");
#endif
		*clustersize = nactive + 1;
	}

	return 0;
}

void
geoqueue_append_event_params(geoqueue_t* queue, zchunk_t* chunk, gq_cmd_t cmd)
{
	gq_event_append_string(chunk, queue->address, -1);
	switch (cmd) {
		case GQ_CMD_HELLO:
			gq_event_append_string(chunk, queue->nodeid, -1);
			pthread_mutex_lock(queue->peers_lock);
			zlistx_t* list = gq_peer_only_active(queue->peers);
			pthread_mutex_unlock(queue->peers_lock);
			gq_event_append_list(chunk, list, gq_peer_serialize);
			zlistx_destroy(&list);
			break;
		case GQ_CMD_HEARTBEAT:
			break;
	}
}

void
gq_set_adopted_cb(geoqueue_t* queue, gq_adopted_cb_t* func, void* arg)
{
	queue->adopted_cb = func;
	queue->adopted_arg = arg;
}

gq_cond_t*
gq_replicate(geoqueue_t* queue, const char* key, const char* value, int value_len)
{
	gq_peer_object_state_t state = GQ_OBJECT_STATE_NEW;
	pbuf_t* other_nodes = NULL;
	gq_peer_t* peer;

	if (!queue || !key || !value)
		return NULL;

	if (value_len < 0)
		value_len = strlen(value);
	gq_log(GQ_LOG_DEBUG, "%s: queue at `%s' replicates key `%s' value `%.*s'\n",
		__FUNCTION__, queue->address, key, value_len, value);

	pthread_mutex_lock(queue->peers_lock);
	zlistx_t* list = gq_peer_only_active(queue->peers);
	pthread_mutex_unlock(queue->peers_lock);
	size_t npeers = list ? zlistx_size(list) : 0;
	if (npeers < queue->tolerated_failures) {
		gq_log(GQ_LOG_INFO,
			"%s: queue at `%s' has only %ld confirmed peer%s of the required %d\n",
			__FUNCTION__, queue->address, npeers, npeers == 1 ? "" : "s",
			queue->tolerated_failures);
		zlistx_destroy(&list);
		return NULL;
	}
	if (npeers > 1) {
		// find a random starting position
		// TODO: properly reorder the list randomly
		long tomove = random() % npeers;
		while (tomove-- > 0) {
			void* obj = zlistx_first(list);
			if (obj) {
				zlistx_detach_cur(list);
				zlistx_add_end(list, obj);
			}
		}
	}

	npeers = queue->tolerated_failures;

	other_nodes = pbuf_create(npeers, 0, NULL);
	peer = zlistx_first(list);
	while (peer && (pbuf_size(other_nodes) < npeers)) {
		const char* id = gq_peer_get_id(peer);
		if (!id)
			continue;
		pbuf_append(other_nodes, strdup(id));
		peer = zlistx_next(list);
	}
	if (pbuf_size(other_nodes) < npeers) {
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' can not replicate key `%s', too few active peers\n",
			__FUNCTION__, queue->address, key);
		zlistx_destroy(&list);
		return NULL;
	}
	// remove the rest
	while (peer) {
		zlistx_detach_cur(list);
		peer = zlistx_next(list);
	}

	gq_cond_t* cond = gq_cond_create();
	pthread_mutex_lock(queue->conds_lock);
	gq_cond_lock(cond, npeers);
	zhashx_insert(queue->conds, key, cond);
	pthread_mutex_unlock(queue->conds_lock);
	gq_log(GQ_LOG_INFO, "%s: queue at `%s' replicates key `%s' to %ld peers with cond at %p\n",
		__FUNCTION__, queue->address, key, zlistx_size(list), cond);

	pthread_mutex_lock(queue->replica_lock);
	zhashx_insert(queue->replicas, key, other_nodes);
	pthread_mutex_unlock(queue->replica_lock);

	peer = zlistx_first(list);
	while (peer) {
		gq_peer_replicate(peer, cond, key, state, value, value_len, other_nodes);
		peer = zlistx_next(list);
	}

	zlistx_destroy(&list);

	return cond;
}

gq_peer_t*
gq_by_id(geoqueue_t* queue, const char* id)
{
	pthread_mutex_lock(queue->peers_lock);
	gq_peer_t* peer = gq_peer_by_id(queue->peers, id);
	pthread_mutex_unlock(queue->peers_lock);
	return peer;
}

gq_cond_t*
gq_update(geoqueue_t* queue, const char* key, const char* value, int value_len)
{
	if (!queue || !key || !value)
		return NULL;

	if (value_len < 0)
		value_len = strlen(value);
	gq_log(GQ_LOG_DEBUG, "%s: queue at `%s' updates key `%s' value `%.*s'\n",
		__FUNCTION__, queue->address, key, value_len, value);

	pthread_mutex_lock(queue->replica_lock);
	pbuf_t* ids = zhashx_lookup(queue->replicas, key);
	pthread_mutex_unlock(queue->replica_lock);
	gq_log(GQ_LOG_DEBUG, "%s: queue at `%s' previously sent `%s' to %ld other nodes\n",
		__FUNCTION__, queue->address, key, pbuf_size(ids));

	const char* id;
	gq_peer_t* peer;
	zlistx_t* list = zlistx_new();
	pthread_mutex_lock(queue->peers_lock);
	PBUF_FOREACH(id, ids)
		peer = gq_peer_by_id(queue->peers, id);
		if (peer)
			zlistx_add_end(list, peer);
	PBUF_FOREACH_END
	pthread_mutex_unlock(queue->peers_lock);

	size_t npeers = zlistx_size(list);
	if (npeers < 1) {
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' has no peers for key `%s'\n",
			__FUNCTION__, queue->address, key);
		zlistx_destroy(&list);
		return NULL;
	}

	gq_cond_t* cond = gq_cond_create();

	pthread_mutex_lock(queue->conds_lock);
	gq_cond_lock(cond, npeers);
	gq_cond_t* old_cond = zhashx_lookup(queue->conds, key);
	if (old_cond) {
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' already has a cond at %p for key `%s'\n",
			__FUNCTION__, queue->address, cond, key);
	} else {
		zhashx_insert(queue->conds, key, cond);
	}
	pthread_mutex_unlock(queue->conds_lock);

	gq_log(GQ_LOG_INFO,
		"%s: queue at `%s' removes item with key `%s' from %ld peers with cond at %p\n",
		__FUNCTION__, queue->address, key, zlistx_size(list), cond);
	peer = zlistx_first(list);
	while (peer) {
		gq_peer_replicate(peer, cond, key, GQ_OBJECT_STATE_UPDATED, NULL, 0, NULL);
		peer = zlistx_next(list);
	}

	zlistx_destroy(&list);

	return cond;

}

gq_cond_t*
gq_clear(geoqueue_t* queue, const char* key)
{
	if (!queue || !key)
		return NULL;

	pthread_mutex_lock(queue->replica_lock);
	pbuf_t* ids = zhashx_lookup(queue->replicas, key);
	zhashx_delete(queue->replicas, key);
	pthread_mutex_unlock(queue->replica_lock);
	gq_log(GQ_LOG_DEBUG, "%s: queue at `%s' previously sent `%s' to %ld other nodes\n",
		__FUNCTION__, queue->address, key, pbuf_size(ids));
	const char* id;
	gq_peer_t* peer;
	zlistx_t* list = zlistx_new();
	pthread_mutex_lock(queue->peers_lock);
	PBUF_FOREACH(id, ids)
		peer = gq_peer_by_id(queue->peers, id);
		if (peer)
			zlistx_add_end(list, peer);
	PBUF_FOREACH_END
	pthread_mutex_unlock(queue->peers_lock);
	pbuf_free(ids);

	size_t npeers = zlistx_size(list);
	if (npeers < 1) {
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' has no peers for key `%s'\n",
			__FUNCTION__, queue->address, key);
		zlistx_destroy(&list);
		return NULL;
	}

	gq_cond_t* cond = gq_cond_create();

	pthread_mutex_lock(queue->conds_lock);
	gq_cond_lock(cond, npeers);
	gq_cond_t* old_cond = zhashx_lookup(queue->conds, key);
	if (old_cond) {
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' already has a cond at %p for key `%s'\n",
			__FUNCTION__, queue->address, cond, key);
	} else {
		zhashx_insert(queue->conds, key, cond);
	}
	pthread_mutex_unlock(queue->conds_lock);

	gq_log(GQ_LOG_INFO,
		"%s: queue at `%s' removes item with key `%s' from %ld peers with cond at %p\n",
		__FUNCTION__, queue->address, key, zlistx_size(list), cond);
	peer = zlistx_first(list);
	while (peer) {
		gq_peer_replicate(peer, cond, key,
			GQ_OBJECT_STATE_TERMINATED, NULL, 0, NULL);
		peer = zlistx_next(list);
	}

	zlistx_destroy(&list);

	return cond;
}

void
gq_signal_keys(geoqueue_t* queue, pbuf_t* keys)
{
	const char* key;

	gq_log(GQ_LOG_DEBUG, "%s: queue at `%s' got confirmation for %ld keys\n",
		__FUNCTION__, queue->address, pbuf_size(keys));
	pthread_mutex_lock(queue->conds_lock);
	PBUF_FOREACH(key, keys)
		gq_cond_t* cond = zhashx_lookup(queue->conds, key);
		if (!cond)
			continue;
		if (gq_cond_signal(cond)) {
			gq_log(GQ_LOG_DEBUG, "%s: queue at `%s' signals cond at %p\n",
				__FUNCTION__, gq_get_address(queue), cond);
			zhashx_delete(queue->conds, key);
			pbuf_putat(keys, pbuf_i, NULL);	// will also free 'key'
		}
	PBUF_FOREACH_END
	pthread_mutex_unlock(queue->conds_lock);
}

void
gq_adopt(geoqueue_t* queue, gq_peer_object_info_t* info)
{
	gq_log(GQ_LOG_INFO, "%s: queue at `%s' adopts object `%s'\n",
		__FUNCTION__, queue->address, info->key);
	//zhashx_insert(queue->replicas, key, pbuf_dup(info->other_nodes));
	if (queue->adopted_cb) {
		queue->adopted_cb(queue->adopted_arg, info->key, info->value);
	}
	gq_peer_object_info_free(info);
}

void
gq_persist(geoqueue_t* queue, const char* key, int keylen, const char* value, int valuelen, int create_if_new)
{
	int klen = keylen;
	int vlen = valuelen;
	char* errptr = NULL;

	if (key && (klen < 0))
		klen = strlen(key);
	if (klen < 0)
		return;

	if (value) {
		if (vlen < 0)
			vlen = strlen(value);
	} else {
		vlen = -1;
	}

	if (vlen > 0) {
		if (!create_if_new) {
			size_t current_value_len = 0;
			char* current_value = leveldb_get(queue->ldb, queue->readoptions,
				key, klen, &current_value_len, &errptr);
			if (errptr) {
				fprintf(stderr, "node %s: could not find key `%s': %s\n",
					queue->nodeid, key, errptr);
				free(errptr);
			}
			if (current_value) {
				free(current_value);
			} else {
				return;
			}
		}
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' stores key `%.*s' value `%.*s'\n",
				__FUNCTION__, queue->address, klen, key, vlen, value);
		leveldb_put(queue->ldb, queue->writeoptions, key, klen, value, vlen, &errptr);
	} else {
		gq_log(GQ_LOG_INFO, "%s: queue at `%s' deletes key `%.*s'\n",
			__FUNCTION__, queue->address, klen, key);
		leveldb_delete(queue->ldb, queue->writeoptions, key, klen, &errptr);
	}
	if (errptr) {
		fprintf(stderr, "node %s: could not save/update/delete key `%s': %s\n",
			queue->nodeid, key, errptr);
		free(errptr);
	}
}

