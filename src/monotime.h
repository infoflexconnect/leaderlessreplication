#ifndef GR_MONOTIME_H
#define GR_MONOTIME_H

#include <sys/time.h>

int
gettimeofday_monotonic(struct timeval* tvp);

#endif

