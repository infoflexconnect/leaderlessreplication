#ifndef PEER_OBJECT_INFO_H
#define PEER_OBJECT_INFO_H 1

#include "cond.h"
#include "pbuf.h"

#include "czmq.h"

//typedef struct gr_peer_object_info_t gr_peer_object_info_t;

typedef enum {
	GR_OBJECT_STATE_NEW,
	GR_OBJECT_STATE_UPDATED,
	GR_OBJECT_STATE_TERMINATED,
} gr_peer_object_state_t;

// in hash, indexed by key
typedef struct {
	gr_peer_object_state_t state;
	gr_cond_t* message_cond;
	char* key;
	zchunk_t* value;
	pbuf_t other_nodes[1];
} gr_peer_object_info_t;

gr_peer_object_info_t*
gr_peer_object_info_create(gr_cond_t* message_cond,
	gr_peer_object_state_t state, const char* value, int value_len,
	const pbuf_t* other_nodes);

void
gr_peer_object_info_free_p(gr_peer_object_info_t** p);

zlistx_t*
gr_peer_object_info_list_creator(void);

zhashx_t*
gr_peer_object_info_hash_creator(void);

void
gr_peer_object_info_serialize(gr_peer_object_info_t* info, const char* key,
    zchunk_t* chunk);

int
gr_peer_object_info_deserialize(const byte** cpp, int* sp, void** objp);

#endif

