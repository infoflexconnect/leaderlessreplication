# A Resilient Message Queue

This is an implementation of the protocol described in "Superlinear and Bandwidth Friendly Geo-replication for Store-And-Forward Systems" by Brahneborg et al., ICSOFT, 2020.

As of 2021, under submission with the title "A Resilient Message Queue" to PeerJ.

## System Model

A small number of independent nodes, all being able to reach each other, possibly geographically distant.
Nodes can join, leave, and rejoin at any point.
Each node has a unique and constant identifier.

The order of enqueued records is not significant, as long as liveness is preserved.
All records are enqueued to be replicated to all other nodes.
The number of nodes that must acknowledge the replication is configurable.

## Build

Requirements:
* Libraries
  - ZeroMQ: http://zeromq.org
  - CZMQ: http://czmq.zeromq.org
* Tools
  - CMake: https://cmake.org
  - Make (or whatever CMake is configured to produce for)
  - a C compiler

```bash
cmake .
make
```

## Usage

Call `gr_init` to initialize the system, using the given TCP/IP address as the externally visible address of the current host.

Call `gr_contact` to set TCP/IP addresses of other hosts.
They do not need to be reachable when this call is made, so nodes can be started in any order.

Call `gr_set_ack_count` (no, rename this one).

Call `gr_replicate()` for new records, `gr_update()` when records are updated, and `gr_clear()` when they are deleted.
Both new and updated values require the full record.

## Algorithms

### New node

TODO: Provable using Spin?

1. An initial contact is made, in one of the following ways.
  1. A new node is informed about a node in an existing cluster. The existing node is marked as "prospect".
  1. An node in an existing cluster is informed about a new node that wants to join. The new node is marked as "active".
1. Repeat, as long as the "prospect" list is non-empty:
  1. The new node sends a "hello, my id is $id and my ip/port is $address" to all prospects, and marks them as "contacted".
  1. On connection failure, the existing node is marked as "unreachable", allowing reconnection attempts later.
  1. The existing node registers the new node as "active", and sends back its own id and its full list of peers (id and address).
  1. When the reply arrives to the new node, it marks the existing node as "active".
  1. Any new node, indicated by a new address, is added to the peer list and marked as "prospect".
1. The new node marks itself as "active".

### Heartbeats

1. Try to reconnect to all "unreachable" peers, with a time interval.
1. Send data or heartbeat after 0.1--1.0 seconds.
1. Incoming traffic from "unreachable" peers, marks them as "active".

## Testing

### Cases

#### A slow response

Each submitted message requires a new lock, as shown by the following sequence.

1. A client submits a message to be replicated, requiring one response.
1. While waiting, it hangs on a lock given by GeoRep.
1. The message is replicated to two other nodes.
1. One node replies quickly, releasing the lock for the client.
1. The client can now submit a second message, hanging on a lock again.
1. The second node now replies to the first message.
1. The second lock should not be affected this, and neither should the client.

### Valgrind

### Helgrind

## TODO

* Log API
  - queue id
* Complexity measurement
  - Cite: Adam Tornhill
  - Quota of total number of tabs, except the first one, and the number of lines.
  - Same for the number of indentation differences on adjacent lines.
  - Compared to EMG in general.
* Illustration of the node state machine.
* Visualization
  - Save I/O (from, what, to) for each test case in a separate plantuml file.
* Detect dead nodes.
  - Calculate median of round-trip times, per connection.
* Message replication
  - API: optional callback + arg, promise/future style?
  - Configurable window size for replication, by default 1.
  - What if too many nodes die, leaving the locks hanging?
  - Async callbacks after confirmations?
* Message ownership.
  - On death.
  - If RTT towards destination is lower somewhere else.
  * Security
    - Using callback hooks?
    - ZeroMQ elliptic curve stuff?
* Message metadata - routelog
  - Using CRDT mechanisms?
* User credit - callback hooks?
  - Check if message can be accepted.
  - Update with final credit.
  - Propagate using PN-counters to all other nodes.
* Send updates to a subset of nodes, as long as the result is still a connected graph?
