
#include "georep.h"
#include "log.h"
#include "helpers.h"

#include <leveldb/c.h>

#include <getopt.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

static int client_count = 1;
static int message_count = 1;
static char* nodeid = NULL;
static int duration = 10;
static int warmup = 5;
static int64_t credits = 0;
static gr_log_level_t loglevel = GR_LOG_INFO;
static georep_t* queue = NULL;

static void
upause(unsigned int us)
{
	struct timeval tv;

	if (!us) return;

	tv.tv_sec = us / (1000 * 1000);
	tv.tv_usec = us % (1000 * 1000);

	select(0, 0, 0, 0, &tv);
}

typedef struct {
	int no;
	gr_cond_t* stop_cond;
	int* tps;
} TestReplicateMany_info_t;

static void*
TestReplicateMany_client_thread(void* arg)
{
	int i;
	int prev_count = 0;
	TestReplicateMany_info_t* info = arg;
	struct timeval start[1];
	struct timeval last[1];
	struct timeval now[1];

#if 0
	if (loglevel <= GR_LOG_INFO)
		fprintf(stderr, "%s: client %d starts\n", __FUNCTION__, info->no);
#endif
	gettimeofday(start, NULL);
	last[0] = start[0];
	for (i = 0; ; ++i) {
		if ((message_count > 0) && (i >= message_count))
			break;

		// do stuff
		int64_t value = gr_get_counter_value(queue, "credits", "username");
		gr_log(GR_LOG_DEBUG, "%s: current sum is %ld\n", __FUNCTION__, value);
		int64_t diff = -1;
		if (value < 0) {
			diff += 1000;
			gr_log(GR_LOG_INFO, "%s: node %s increases the credits to %ld\n",
				__FUNCTION__, nodeid, value + diff);
		}
		gr_delta_counter(queue, "credits", "username", diff);
		upause(1);

		gettimeofday(now, NULL);
		int nsec = now->tv_sec - start->tv_sec;
		if (nsec > (warmup + duration))
			break;
		info->tps[nsec]++;

		if (!info->no && (now->tv_sec > last->tv_sec)) {
			if (loglevel <= GR_LOG_INFO) {
				struct tm tm[1];
				localtime_r(&now->tv_sec, tm);
				if (message_count > -1) {
					fprintf(stderr, "%02d:%02d:%02d node %s client %d has sent %d/%d messages, %d this second (total on node: %d)\n",
						tm->tm_hour, tm->tm_min, tm->tm_sec,
						nodeid, info->no, i, message_count, i - prev_count, (i - prev_count) * client_count);
				} else {
					fprintf(stderr, "%02d:%02d:%02d node %s client %d has sent %d messages, %d this second (total on node: %d)\n",
						tm->tm_hour, tm->tm_min, tm->tm_sec,
						nodeid, info->no, i, i - prev_count, (i - prev_count) * client_count);
				}
			}
			last[0] = now[0];
			prev_count = i;
		}
	}

	gr_cond_signal(info->stop_cond);
	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: client %d stops\n", __FUNCTION__, info->no);

	pthread_exit(NULL);
}

static int
compare_int(const void* p1, const void* p2)
{
	const int* ip1 = p1;
	const int* ip2 = p2;

	return *ip1 - *ip2;
}

int
main(int argc, char* argv[])
{
	int i;
	int j;
	int ret;
	int c;
	int option_index = 0;
	int node_count = 1;
	int* tps = NULL;
	pthread_t* clients;
	TestReplicateMany_info_t* client_info;
	pthread_attr_t attr;
	struct timeval start[1];
	leveldb_env_t* ldb_env;

	for (;;) {
		static struct option long_options[] = {
//	/* 0 */	{"no-find",  0, NULL, 0},
			{"clients",  1, 0, 'c'},
			{"messages", 1, 0, 'm'},
			{"nodes",    1, 0, 'n'},
			{"debug",    0, 0, 'd'},
			{"quiet",    0, 0, 'q'},
			{"time",     1, 0, 't'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "c:dm:n:qt:", long_options, &option_index);
		if (c == -1) break;
		switch (c) {
			case 0:
				switch (option_index) {
					case 0:
						break;
				}
				break;

			case 'c':
//				fprintf(stderr, "%s:%d: %s clients\n", argv[0], __LINE__, optarg);
				client_count = atoi(optarg);
				break;

			case 'd':
				loglevel = GR_LOG_DEBUG;
				break;

			case 'q':
				loglevel = GR_LOG_NOTHING;
				break;

			case 'm':
//				fprintf(stderr, "%s:%d: %s messages\n", argv[0], __LINE__, optarg);
				message_count = atoi(optarg);
				break;

			case 'n':
//				fprintf(stderr, "%s:%d: %s nodes\n", argv[0], __LINE__, optarg);
				node_count = atoi(optarg);
				break;

			case 't':
//				fprintf(stderr, "%s:%d: %s nodes\n", argv[0], __LINE__, optarg);
				duration = atoi(optarg);
				if (duration < 1)
					duration = 1;
				break;

			default:
				fprintf(stderr, "getopt_long returned %d / %c\n", c, c);
		}
	}

	if ((optind >= argc) || (node_count < 1)) {
		fprintf(stderr,
			"usage: %s [options] -n node_count my-id my-ip:port other-ip:port [other-ip:port...]\n",
			argv[0]);
		goto done;
	}

	nodeid = argv[optind];

	gr_log_level(loglevel);
	zsys_init();

	if (gr_log_enabled(GR_LOG_INFO)) {
		fprintf(stderr, "%s: calling gr_init(%s, %s)\n",
			argv[0], argv[optind + 1], nodeid);
	}
	ldb_env = leveldb_create_default_env();
	queue = gr_init(argv[optind + 1], nodeid);
	if (!queue) {
		fprintf(stderr, "%s: can not initialize GeoQueue(%s, %s)\n",
			argv[0], argv[optind + 1], nodeid);
		goto done;
	}
	optind += 2;
	while (optind < argc) {
		if (gr_log_enabled(GR_LOG_INFO)) {
			fprintf(stderr, "%s: calling gr_contact(%s)\n",
				argv[0], argv[optind]);
		}
		ret = gr_contact(queue, argv[optind], NULL);
		(void) ret;
		optind++;
	}
	waitForClusterSize(NULL, queue, node_count);
	gr_set_high_watermark(queue, 1);
	gr_tolerate_failures(queue, 1);

	if (strcmp(nodeid, "1") == 0) {
		gr_create_counter(queue, "credits");
		credits = 5;
	}
	gr_set_counter(queue, "credits", "username", credits, 0);

	pthread_attr_init(&attr);
	pthread_attr_setstacksize(&attr, 256 * 1024);
	clients = malloc(client_count * sizeof(clients[0]));
	client_info = malloc(client_count * sizeof(client_info[0]));

	warmup = 5;
	int ntps = warmup + duration + 1;

	for (i = 0; i < client_count; ++i) {
		client_info[i].no = i;
		client_info[i].stop_cond = gr_cond_create();
		client_info[i].tps = calloc((ntps), sizeof(int));
		gr_cond_lock(client_info[i].stop_cond, 1);
		ret = pthread_create(&clients[i], &attr, TestReplicateMany_client_thread, &client_info[i]);
	}
	if (gr_log_enabled(GR_LOG_INFO)) {
		fprintf(stderr, "node %s: %d client threads started\n", nodeid, client_count);
	}

	gettimeofday(start, NULL);

	for (i = 0; i < client_count; ++i) {
//		fprintf(stderr, "%s: wait for client %d to stop\n", __FUNCTION__, i);
		gr_cond_wait(client_info[i].stop_cond, 0);
		gr_cond_unlock(client_info[i].stop_cond);
		ret = pthread_join(clients[i], NULL);
		gr_cond_free(client_info[i].stop_cond);
	}
	if (gr_log_enabled(GR_LOG_INFO)) {
		fprintf(stderr, "node %s: all clients stopped\n", nodeid);
	}

	tps = calloc(duration, sizeof(int));
	for (i = 0; i < client_count; ++i) {
		for (j = 0; j < duration; ++j) {
#if 0
			fprintf(stderr, "node %s client %d sec %d - %d tps\n",
				nodeid, i, j, client_info[i].tps[j]);
#endif
			tps[j] += client_info[i].tps[warmup + j];
		}
	}
	qsort(tps, duration, sizeof(tps[0]), compare_int);
#if 1
	fprintf(stderr, " clients %3d node %s", client_count, nodeid);
	int last = tps[duration - 1];
	int ndigits = 1;
	if (last > 9) ndigits = 2;
	if (last > 99) ndigits = 3;
	if (last > 999) ndigits = 4;
	if (last > 9999) ndigits = 5;
	if (last > 99099) ndigits = 6;
	for (j = 0; j < duration; ++j) {
		//fprintf(stderr, "#clients %3d node %s sec %d - %d tps\n", nodeid, j, tps[j]);
		fprintf(stderr, " %*d", ndigits, tps[j]);
	}
	fprintf(stderr, "\n");
#endif
	fprintf(stderr, "#clients %3d node %s %5d - %5d [ %5d ] %5d - %5d\n",
		client_count,
		nodeid,
		tps[0],
		tps[1 * duration / 4], tps[2 * duration / 4], tps[3 * duration / 4],
		tps[duration - 1]);

	if (client_count > 1) {
		for (j = 0; j < duration; ++j) {
			tps[j] /= client_count;
		}
		fprintf(stderr, "#clients %3d node %s %5d - %5d [ %5d ] %5d - %5d  // per client\n",
			client_count,
			nodeid,
			tps[0],
			tps[1 * duration / 4], tps[2 * duration / 4], tps[3 * duration / 4],
			tps[duration - 1]);
	}

	free(tps);
	sleep(5);
	free(clients);
	for (i = 0; i < client_count; ++i) {
		free(client_info[i].tps);
	}
	free(client_info);

done:
	gr_shutdown(&queue);
	if (ldb_env)
		leveldb_env_destroy(ldb_env);

	exit(0);
}

