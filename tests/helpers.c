
#include "helpers.h"

void
waitForClusterSize(CuTest* tc, georep_t* queue, int expected)
{
	int i;
	int clustersize;

	gr_contact(queue, NULL, &clustersize);
	for (i = 0; i < 15; ++i) {
		if (clustersize == expected)
			break;
		sleep(1);
		gr_contact(queue, NULL, &clustersize);
	}
	if (tc)
		CuAssertTrue(tc, clustersize == expected);
}

