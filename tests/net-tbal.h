
#include "net-base.h"

void*
gr_tbal_net_init(georep_t* queue);

void
gr_tbal_net_close(void** op);

void*
gr_tbal_net_peer_init(gr_peer_t* peer);

void
gr_tbal_net_peer_free(void** op);

void
gr_tbal_net_peer_close(void* p);

int
gr_tbal_net_peer_send_chunk(void* p, zchunk_t* chunk);

void
gr_tbal_net_setup(gr_net_t* config);

