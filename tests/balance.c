
#include "georep.h"
#include "log.h"
#include "helpers.h"
#include "net-tbal.h"

#include "peer.h"
#include "net-zeromq.h"

#include <leveldb/c.h>

#include <getopt.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

// static int64_t credits = 0;
static int node_count = 2;
static gr_log_level_t loglevel = GR_LOG_DEBUG; // GR_LOG_INFO;
static gr_net_t tbal_config[1];
static pbuf_t queues[1];
static pbuf_t nodes[1];

typedef struct {
	int next;
	int drop_next;
	int drop_data;
	uint64_t nbytes;
	pbuf_t ops[1];
} bal_node_t;

static bal_node_t*
bal_node_create(void)
{
	bal_node_t* obj = calloc(1, sizeof(*obj));
	pbuf_init(obj->ops, 100, 0, NULL);
	return obj;
}

static void
bal_node_free(void* o)
{
	bal_node_t* obj = o;
	if (!o) return;
	pbuf_destroy(obj->ops);
	free(obj);
}

typedef struct {
	georep_t* queue;
	void* loop;
} gr_tbal_net_queue_t;

typedef struct {
	char* address;
} gr_tbal_net_peer_t;

void*
gr_tbal_net_init(georep_t* queue)
{
	gr_tbal_net_queue_t* obj;
	const char* address = gr_get_address(queue);

	if (!queue || !address)
		return NULL;

	gr_log(GR_LOG_DEBUG, "%s: enter for address `%s'\n", __FUNCTION__, address);

	obj = calloc(1, sizeof(*obj));
	obj->queue = queue;

	return obj;
}

void
gr_tbal_net_close(void** op)
{
	gr_tbal_net_queue_t* obj = op ? *op : NULL;

	if (!obj)
		return;
	*op = NULL;

	free(obj);
}

void*
gr_tbal_net_peer_init(gr_peer_t* peer)
{
	const char* address = gr_peer_get_address(peer);

	if (!peer || !address)
		return NULL;

	gr_log(GR_LOG_DEBUG, "%s: enter for address `%s'\n", __FUNCTION__, address);

	gr_tbal_net_peer_t* obj = calloc(1, sizeof(*obj));

	obj->address = malloc(strlen(address) + 1);
	strcpy(obj->address, address);

	return obj;
}

void
gr_tbal_net_peer_free(void** op)
{
	gr_tbal_net_peer_t* obj = op ? *op : NULL;

	if (!obj)
		return;
	*op = NULL;

	free(obj->address);
	free(obj);
}

void
gr_tbal_net_peer_close(void* p)
{
	gr_tbal_net_peer_t* obj = p;

	(void) obj;
}

int
gr_tbal_net_peer_send_chunk(void* p, zchunk_t* chunk)
{
	int rc = 0;
	gr_tbal_net_peer_t* obj = p;
	georep_t* queue;

	int csize = zchunk_size(chunk);
	const byte* cdata = zchunk_data(chunk);

	if (gr_log_enabled(GR_LOG_DEBUG)) {
		zframe_t* frame = zframe_new(cdata, csize);
		char* hf = zframe_strhex(frame);
		gr_log(GR_LOG_DEBUG, "%s: send to peer at `%s': %s\n", __FUNCTION__, obj->address, hf);
		free(hf);
		zframe_destroy(&frame);
	}

	int nodeid = atoi(obj->address);
	queue = pbuf_getat(queues, nodeid);
	if (!queue) {
		gr_log(GR_LOG_INFO, "%s: can not find peer at `%s'\n", __FUNCTION__, obj->address);
		return -1;
	}

	bal_node_t* node = pbuf_getat(nodes, nodeid);
	if (node) {
		node->nbytes += csize;
		if (node->drop_next) {
			gr_log(GR_LOG_DEBUG, "%s: drop packet to peer at `%s'\n", __FUNCTION__, obj->address);
			node->drop_next = node->drop_data;
			return 0;
		}
	}

	while (csize > 0) {
		if (gr_handle_incoming(queue, &cdata, &csize) < 0)
			break;
	}

	return rc;
}

void
gr_tbal_net_setup(gr_net_t* config)
{
	config->net_init = gr_tbal_net_init;
	config->net_close = gr_tbal_net_close;
	config->net_peer_init = gr_tbal_net_peer_init;
	config->net_peer_free = gr_tbal_net_peer_free;
	config->net_peer_close = gr_tbal_net_peer_close;
	config->net_peer_send_chunk = gr_tbal_net_peer_send_chunk;
}

/* unused */
void gr_zeromq_setup(gr_net_t* config) { (void) config; }

static void
sync_all(void)
{
	int i;
	georep_t* queue;

	if (node_count < 2) return;
	for (i = 0; i < node_count; ++i) {
		queue = pbuf_getat(queues, i);
		georep_sync(queue);
	}
}

typedef enum {
	OP_NonZero,
	OP_Replicate,
	OP_TooMany,
	OP_Incoming,
	OP_PeerAcks,
	OP_Different,
	OP_Same,
	OP_Drop,
} bal_op_t;

static const char*
bal_op_name(bal_op_t op)
{
	switch (op) {
		case OP_NonZero:	return "NonZero";
		case OP_Replicate:	return "Replicate";
		case OP_TooMany:	return "TooMany";
		case OP_Incoming:	return "Incoming";
		case OP_PeerAcks:	return "PeerAcks";
		case OP_Different:	return "Different";
		case OP_Same:		return "Same";
		case OP_Drop:		return "Drop";
	}
	return "unknown";
}

typedef struct {
	bal_op_t op;
	int arg;
} bal_op_arg_t;

#define ADD_NZ() do { \
	op = calloc(1, sizeof(*op)); \
	*op = (bal_op_arg_t) { OP_NonZero, (lrand48() % 7) - 2 }; \
	pbuf_append(ops, op); \
} while (0)

#define ADD_REPLICATE(mandatory) do { \
	op = calloc(1, sizeof(*op)); \
	if (!mandatory && (lrand48() & 1)) { \
		*op = (bal_op_arg_t) { OP_Drop, 0 }; \
		pbuf_append(ops, op); \
		op = calloc(1, sizeof(*op)); \
	} \
	*op = (bal_op_arg_t) { OP_Replicate, 0 }; \
	pbuf_append(ops, op); \
} while (0)

#define ADD_ACK() do { \
	op = calloc(1, sizeof(*op)); \
	*op = (bal_op_arg_t) { OP_PeerAcks, 0 }; \
	pbuf_append(ops, op); \
} while (0)

#define ADD_DIFFERENT() do { \
	op = calloc(1, sizeof(*op)); \
	*op = (bal_op_arg_t) { OP_Different, 0 }; \
	pbuf_append(ops, op); \
} while (0)

#define ADD_SAME() do { \
	op = calloc(1, sizeof(*op)); \
	*op = (bal_op_arg_t) { OP_Same, 0 }; \
	pbuf_append(ops, op); \
} while (0)

#define ADD_MANY() do { \
	op = calloc(1, sizeof(*op)); \
	*op = (bal_op_arg_t) { OP_TooMany, 0 }; \
	pbuf_append(ops, op); \
} while (0)

#define ADD_INCOMING() do { \
	op = calloc(1, sizeof(*op)); \
	*op = (bal_op_arg_t) { OP_Incoming, 0 }; \
	pbuf_append(ops, op); \
} while (0)

#if 1
static void
generate_ops_intermediate(pbuf_t* ops)
{
	int i;
	bal_op_arg_t* op;

	ADD_REPLICATE(0);
	switch (lrand48() % 3) {
		case 0:
			for (i = lrand48() % 3 + 1; i >= 0; --i) {
				ADD_NZ();
			}
			break;

		case 1:
			ADD_NZ();
			ADD_ACK();
			ADD_DIFFERENT();
			break;

		case 2:
#if 0
			ADD_MANY();
			for (i = 0; i < 10; ++i) {
				ADD_REPLICATE(0);
			}
			ADD_NZ();
			ADD_INCOMING();
#endif
			break;
	}
}
#endif

static void
generate_ops(pbuf_t* ops)
{
	bal_op_arg_t* op;
#if 1
	int i;

	ADD_NZ();
	for (i = lrand48() % 4; i >= 0; --i) {
		generate_ops_intermediate(ops);
	}
	ADD_REPLICATE(1);
	ADD_ACK();
	ADD_SAME();
#else
	ADD_NZ();
	ADD_REPLICATE(1);
#endif
}

static void
build_nodes(void)
{
	int i;
	bal_node_t* node;

	for (i = 0; i < node_count; ++i) {
		node = bal_node_create();
		generate_ops(node->ops);
		pbuf_putat(nodes, i, node);
	}
}

#define CNAME "credits"
#define UNAME "username"

static int
sum_all(int all_equal)
{
	int sum;
	int common = -1;
	georep_t* queue;

	PBUF_FOREACH(queue, queues)
		sum = gr_get_counter_value(queue, CNAME, UNAME);
		if (common < 0)
			common = sum;
		if (common != sum) {
			if (all_equal > 0) {
				gr_log(GR_LOG_DEBUG, "%s: total sum on node %s is %d, expected %d\n",
					__FUNCTION__, gr_get_address(queue), sum, common);
				exit(1);
			}
		}
		gr_log(GR_LOG_DEBUG, "%s: total sum on node %s is %d\n",
			__FUNCTION__, gr_get_address(queue), sum);
	PBUF_FOREACH_END
	return common;
}

static void
run_ops(void)
{
	int all_done;
	int common = 0;
	int expected = 0;
	int i;
	int node_id;
	bal_op_arg_t* op;
	bal_node_t* node;
	georep_t* queue;

	for (;;) {
		all_done = 1;
		for (i = 0; i < node_count; ++i) {
			node = pbuf_getat(nodes, i);
			op = pbuf_getat(node->ops, node->next);
			if (op) {
				all_done = 0;
				break;
			}
		}
		if (all_done)
			break;

		// another node, next op
		node_id = lrand48() % node_count;
		node = pbuf_getat(nodes, node_id);
		op = pbuf_getat(node->ops, node->next);
		if (!op)
			continue;
		node->next++;
		gr_log(GR_LOG_DEBUG, "%s: node %d step %d op %s arg %d expecting %d\n",
			__FUNCTION__, node_id, node->next, bal_op_name(op->op), op->arg, expected);
		queue = pbuf_getat(queues, node_id);
		switch (op->op) {
			case OP_NonZero:
				expected += op->arg;
				gr_delta_counter(queue, CNAME, UNAME, op->arg);
				break;
			case OP_Replicate:
				georep_sync(queue);
				break;
			case OP_PeerAcks:
				sync_all();
				break;
			case OP_Different:
				break;
			case OP_Same:
				break;
			case OP_Drop:
				node->drop_next = 1;
				break;
			case OP_TooMany:
				node->drop_data = 1;
				break;
			case OP_Incoming:
				node->drop_data = 0;
				node->drop_next = 0;
				break;
		}
//		sum_all(0);
	}
	for (i = 0; i < node_count; ++i) {
		node = pbuf_getat(nodes, i);
		node->drop_data = 0;
		node->drop_next = 0;
	}

	sync_all();
	common = sum_all(1);

	if (common != expected) {
		gr_log(GR_LOG_DEBUG, "%s: final sum should be %d\n", __FUNCTION__, expected);
		exit(1);
	}
}

static void
measure_network_traffic(void)
{
	int i;
	int n;
	int u;
	georep_t* queue;
	bal_node_t* node;
	char username[32];

	for (n = 0; n < node_count; ++n) {
		node = bal_node_create();
		pbuf_putat(nodes, n, node);
		queue = pbuf_getat(queues, n);
		for (u = n ; u < 100; u += (n + 1)) {
			sprintf(username, "user-%d", u);
			gr_delta_counter(queue, CNAME, username, 1);
		}
	}
	sync_all();

	for (i = 0; i < 10; ++i) {
		for (n = 0; n < node_count; ++n) {
			queue = pbuf_getat(queues, n);
			for (u = 0 ; u < 1; u++) {
				int v = lrand48() & 65535;
				int uno = 16;
				if (v < 32768) uno = 15;
				if (v < 16384) uno = 14;
				if (v < 8192) uno = 13;
				if (v < 4096) uno = 12;
				if (v < 2048) uno = 11;
				if (v < 1024) uno = 10;
				if (v < 512) uno = 9;
				if (v < 256) uno = 8;
				if (v < 128) uno = 7;
				if (v < 64) uno = 6;
				if (v < 32) uno = 5;
				if (v < 16) uno = 4;
				if (v < 8) uno = 3;
				if (v < 4) uno = 2;
				if (v < 2) uno = 1;
				sprintf(username, "user-%d", uno);
				gr_delta_counter(queue, CNAME, username, 1);
			}
		}
		sync_all();
	}

	for (n = 0; n < node_count; ++n) {
		node = pbuf_getat(nodes, n);
		printf("Node %d received %llu bytes\n", n, (long long) node->nbytes);
	}
}

int
main(int argc, char* argv[])
{
	int clustersize;
	int i;
	int c;
	int option_index = 0;
	int measure_network = 0;
	int plain_pn = 0;
	leveldb_env_t* ldb_env = NULL;
	georep_t* queue;
	char nodeid[32];

	srand48(getpid());

	for (;;) {
		static struct option long_options[] = {
			{"nodes",    1, 0, 'n'}, // 0
			{"debug",    0, 0, 'd'}, // 1
			{"quiet",    0, 0, 'q'}, // 2
			{"pn",       0, 0, 0},   // 3
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "dmn:q", long_options, &option_index);
		if (c == -1) break;
		switch (c) {
			case 0:
				switch (option_index) {
					case 3:
						plain_pn = 1;
						break;
				}
				break;

			case 'd':
				loglevel = GR_LOG_DEBUG;
				break;

			case 'q':
				loglevel = GR_LOG_NOTHING;
				break;

			case 'm':
				measure_network = 1;
				break;

			case 'n':
//				fprintf(stderr, "%s:%d: %s nodes\n", argv[0], __LINE__, optarg);
				node_count = atoi(optarg);
				break;

			default:
				fprintf(stderr, "getopt_long returned %d / %c\n", c, c);
		}
	}

#if 0
	if ((optind >= argc) || (node_count < 1)) {
		fprintf(stderr,
			"usage: %s [options] -n node_count\n",
			argv[0]);
		goto done;
	}
#endif

	gr_log_level(loglevel);
	zsys_init();
	ldb_env = leveldb_create_default_env();
	gr_tbal_net_setup(tbal_config);
	pbuf_init(queues, node_count, 0, PBUF_NONE); // => georep_free(void*)
	pbuf_init(nodes, 100, 0, bal_node_free);

	for (i = 0; i < node_count; ++i) {
		sprintf(nodeid, "%d", i);
		queue = gr_init2(nodeid, nodeid, tbal_config);
		if (!queue) {
			fprintf(stderr, "%s: can not initialize GeoQueue(%s, %s)\n",
				argv[0], nodeid, nodeid);
			goto done;
		}
		gr_use_plain_pn(queue, plain_pn);
		pbuf_putat(queues, i, queue);
	}
	if (node_count > 1) {
		for (i = 0; i < node_count; ++i) {
			sprintf(nodeid, "%d", (i + 1) % node_count);
			queue = pbuf_getat(queues, i);
			gr_contact(queue, nodeid, NULL);
		}
	}

	for (;;) {
try_again:
		sync_all();
		for (i = 0; i < node_count; ++i) {
			gr_contact(queue, NULL, &clustersize);
#if 1
			gr_log(GR_LOG_DEBUG, "%s: node %d reports cluster size %d, want %d\n",
				__FUNCTION__, i, clustersize, node_count);
#endif
			if (clustersize < node_count)
				goto try_again;
		}
		sync_all();
		break;
	}

	queue = pbuf_getat(queues, 0);
	gr_create_counter(queue, CNAME);

	if (measure_network) {
		measure_network_traffic();
	} else {
		build_nodes();
		run_ops();
	}

done:
	PBUF_FOREACH(queue, queues)
		gr_shutdown(&queue);
	PBUF_FOREACH_END
	pbuf_destroy(queues);
	pbuf_destroy(nodes);
	if (ldb_env)
		leveldb_env_destroy(ldb_env);

	exit(0);
}

