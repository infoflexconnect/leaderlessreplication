#include "georep.h"
#include "net-zeromq.h"
#include "peer.h"
#include "log.h"
#include "helpers.h"

#include <getopt.h>
#include <stdlib.h>

static int client_count = 1;
static int do_die = 0;
static char* nodeid = NULL;

static gr_log_level_t loglevel = GR_LOG_INFO;
static georep_t* queue = NULL;
static gr_net_t failover_config[1];
static gr_net_t parent_config[1];

static pbuf_t blocked_ids[1];
static int block_outgoing = 0;

typedef struct {
	georep_t* queue;
	void* parent_data;
} failover_net_queue_t;

typedef struct {
	char* address;
	void* parent_data;
	const gr_peer_t* peer;
} failover_net_peer_t;

static void*
failover_net_init(georep_t* queue)
{
	failover_net_queue_t* obj;
	const char* address = gr_get_address(queue);

	if (!queue || !address)
		return NULL;

	gr_log(GR_LOG_DEBUG, "%s: enter for address `%s'\n", __FUNCTION__, address);

	obj = calloc(1, sizeof(*obj));
	obj->queue = queue;

	gr_zeromq_setup(parent_config);
	if (parent_config->net_init)
		obj->parent_data = parent_config->net_init(queue);

	return obj;
}

static void
failover_net_close(void** op)
{
	failover_net_queue_t* obj = op ? *op : NULL;

	if (!obj)
		return;

	gr_log(GR_LOG_DEBUG, "%s: enter\n", __FUNCTION__);

	if (parent_config->net_close)
		parent_config->net_close(&obj->parent_data);

	*op = NULL;
	free(obj);
}

static void*
failover_net_peer_init(gr_peer_t* peer)
{
	const char* address = gr_peer_get_address(peer);

	if (!peer || !address)
		return NULL;

	gr_log(GR_LOG_DEBUG, "%s: enter for address `%s'\n", __FUNCTION__, address);

	failover_net_peer_t* obj = calloc(1, sizeof(*obj));
	obj->peer = peer;
	obj->address = malloc(strlen(address) + 1);
	strcpy(obj->address, address);

	if (parent_config->net_peer_init)
		obj->parent_data = parent_config->net_peer_init(peer);

	return obj;
}

static void
failover_net_peer_free(void** op)
{
	failover_net_peer_t* obj = op ? *op : NULL;

	if (!obj)
		return;

	gr_log(GR_LOG_DEBUG, "%s: enter\n", __FUNCTION__);

	if (parent_config->net_peer_free)
		parent_config->net_peer_free(&obj->parent_data);

	*op = NULL;
	free(obj->address);
	free(obj);
}

static void
failover_net_peer_close(void* p)
{
	failover_net_peer_t* obj = p;

	gr_log(GR_LOG_DEBUG, "%s: enter\n", __FUNCTION__);

	if (parent_config->net_peer_close)
		parent_config->net_peer_close(obj->parent_data);
}

static int
failover_net_peer_send_chunk(void* p, zchunk_t* chunk)
{
	int rc = 0;
	failover_net_peer_t* obj = p;
	const char* id = gr_peer_get_id(obj->peer);

	gr_log(GR_LOG_DEBUG, "%s: enter for id `%s' address `%s'\n",
		__FUNCTION__, id ? id : "", obj->address);

	if (block_outgoing) {
		const char* blocked_id;
		PBUF_FOREACH(blocked_id, blocked_ids)
			if (strcmp(id, blocked_id) == 0) {
				gr_log(GR_LOG_INFO, "%s: no, `%s' is now blocked\n",
					__FUNCTION__, blocked_id);
				return 0;
			}
		PBUF_FOREACH_END
	}

	if (parent_config->net_peer_send_chunk)
		rc = parent_config->net_peer_send_chunk(obj->parent_data, chunk);

	return rc;
}

static void
failover_net_setup(gr_net_t* config)
{
	config->net_init = failover_net_init;
	config->net_close = failover_net_close;
	config->net_peer_init = failover_net_peer_init;
	config->net_peer_free = failover_net_peer_free;
	config->net_peer_close = failover_net_peer_close;
	config->net_peer_send_chunk = failover_net_peer_send_chunk;
}

typedef struct {
	int no;
	gr_cond_t* stop_cond;
	// int* tps;
} TestReplicateMany_info_t;

static void
close_cond(gr_cond_t* cond)
{
	if (!cond)
		return;

	int ret;
	do {
		ret = gr_cond_wait(cond, 10 * 1000);
		if ((ret == ETIMEDOUT) || // helgrind bug
				((ret < 0) && (errno == ETIMEDOUT))) {
			gr_log(GR_LOG_DEBUG, "%s:%d: replicas are slow, cond at %p\n",
					__FUNCTION__, __LINE__, cond);
		}
	} while ((ret == ETIMEDOUT) || ((ret < 0) && (errno == ETIMEDOUT)));
	gr_log(GR_LOG_DEBUG, "%s:%d: wait returned %d, unlock cond at %p\n",
			__FUNCTION__, __LINE__, ret, cond);
	gr_cond_unlock(cond);
	gr_cond_free(cond);
}

static void
replicate_entry(const char* keybuf, const char* valuebuf)
{
	gr_cond_t* cond = gr_replicate(queue, keybuf, valuebuf, -1);
	close_cond(cond);
}

static void*
TestReplicateMany_client_thread(void* arg)
{
	int keylen;
	int valuelen;
	TestReplicateMany_info_t* info = arg;
	char keybuf[32];
	char valuebuf[32];

	// do stuff

	gr_log(GR_LOG_DEBUG, "%s: enter nodeid `%s' client no %d\n",
		__FUNCTION__, nodeid, info->no);

	sleep(2);
	if (strcmp(nodeid, "1") == 0) {
		int i = 0;
		keylen = sprintf(keybuf, "key-%s-%02d-%04d", nodeid, info->no, i);
		(void) keylen;
		valuelen = sprintf(valuebuf, "value-%s-%02d-%04d", nodeid, info->no, i);
		(void) valuelen;
		replicate_entry(keybuf, valuebuf);
	}

	sleep(2);
	if (do_die)
		exit(0); // oops, we died

	block_outgoing = 1;

	sleep(25);

	gr_cond_signal(info->stop_cond);
#if 0
	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: client %d stops\n", __FUNCTION__, info->no);
#endif

	pthread_exit(NULL);
}

static void
adopt_cb(void* arg, const char* key, zchunk_t* value)
{
	(void) arg;

	gr_log(GR_LOG_INFO, "%s: adopting key `%s' with value `%.*s'\n",
		__FUNCTION__, key, (int) zchunk_size(value), zchunk_data(value));
}

int
main(int argc, char* argv[])
{
	int node_count = 1;
	int ret;

	pbuf_init(blocked_ids, 4, 0, NULL);
	gr_log_init();

	for (;;) {
		static struct option long_options[] = {
			{"block",   1, 0, 'b'},
			{"clients",  1, 0, 'c'},
			// {"messages", 1, 0, 'm'},
			{"nodes",    1, 0, 'n'},
			{"debug",    0, 0, 'v'},
			{"quiet",    0, 0, 'q'},
			{"die",      0, 0, 'd'},
			// {"time",     1, 0, 't'},
			{0, 0, 0, 0}
		};

		int option_index = 0;
		int c = getopt_long(argc, argv, "b:c:dn:qv", long_options, &option_index);
		if (c == -1) break;
		switch (c) {
			case 'b':
				pbuf_append(blocked_ids, strdup(optarg));
				break;

			case 'c':
				// fprintf(stderr, "%s:%d: %s clients\n", argv[0], __LINE__, optarg);
				client_count = atoi(optarg);
				break;

			case 'd':
				do_die = 1;
				break;

			case 'v':
				loglevel = GR_LOG_DEBUG;
				break;

			case 'n':
				// fprintf(stderr, "%s:%d: %s nodes\n", argv[0], __LINE__, optarg);
				node_count = atoi(optarg);
				break;

			case 'q':
				loglevel = GR_LOG_NOTHING;
				break;

			default:
				fprintf(stderr, "getopt_long returned %d / %c\n", c, c);
		}
	}

	if ((optind >= argc) || (node_count < 1)) {
		fprintf(stderr,
				"usage: %s [options] -n node_count my-id my-ip:port other-ip:port [other-ip:port...]\n",
				argv[0]);
		goto done;
	}

	nodeid = argv[optind];

	gr_log_level(loglevel);
	zsys_init();

#if 0
	if (gr_log_enabled(GR_LOG_INFO)) {
		fprintf(stderr, "%s: calling gr_init(%s, %s)\n",
				argv[0], argv[optind + 1], nodeid);
	}
#endif

	failover_net_setup(failover_config);
	queue = gr_init2(argv[optind + 1], nodeid, failover_config);
	if (!queue) {
		fprintf(stderr, "%s: can not initialize GeoQueue(%s, %s)\n",
				argv[0], argv[optind + 1], nodeid);
		goto done;
	}
	optind += 2;
	while (optind < argc) {
#if 0
		if (gr_log_enabled(GR_LOG_INFO)) {
			fprintf(stderr, "%s: calling gr_contact(%s)\n",
					argv[0], argv[optind]);
		}
#endif
		ret = gr_contact(queue, argv[optind], NULL);
		(void) ret;
		optind++;
	}
	waitForClusterSize(NULL, queue, node_count);
	gr_set_high_watermark(queue, 1);
	gr_tolerate_failures(queue, node_count - 1);
	gr_replicate_by_id(queue);

	gr_set_adopted_cb(queue, adopt_cb, NULL);

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setstacksize(&attr, 256 * 1024);
	pthread_t* clients = malloc(client_count * sizeof(clients[0]));
	TestReplicateMany_info_t* client_info = malloc(client_count * sizeof(client_info[0]));

	// warmup = 5;
	// int ntps = warmup + duration + 1;

	int i;
	for (i = 0; i < client_count; ++i) {
		client_info[i].no = i;
		client_info[i].stop_cond = gr_cond_create();
		// client_info[i].tps = calloc((ntps), sizeof(int));
		gr_cond_lock(client_info[i].stop_cond, 1);
		ret = pthread_create(&clients[i], &attr, TestReplicateMany_client_thread, &client_info[i]);
	}
#if 0
	if (gr_log_enabled(GR_LOG_INFO)) {
		fprintf(stderr, "node %s: %d client threads started\n", nodeid, client_count);
	}
#endif

	// here the threads are running...

	for (i = 0; i < client_count; ++i) {
		// fprintf(stderr, "%s: wait for client %d to stop\n", __FUNCTION__, i);
		gr_cond_wait(client_info[i].stop_cond, 0);
		gr_cond_unlock(client_info[i].stop_cond);
		ret = pthread_join(clients[i], NULL);
		gr_cond_free(client_info[i].stop_cond);
	}
	if (gr_log_enabled(GR_LOG_INFO)) {
		fprintf(stderr, "node %s: all clients stopped\n", nodeid);
	}

	free(clients);
	for (i = 0; i < client_count; ++i) {
		// free(client_info[i].tps);
	}
	free(client_info);

done:
	gr_shutdown(&queue);
	pbuf_destroy(blocked_ids);

	exit(0);
}

