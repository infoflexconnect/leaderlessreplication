
#include "georep.h"
#include "log.h"

#include "CuTest.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#define TEST_INIT 1
#define TEST_CONTACT 1
#define TEST_THREE 1
#define TEST_ONE_ALIVE_ONE_DEAD 1
#define TEST_REPLICATE_TO_TWO 1
#define TEST_REPLICATE_AND_CLEAR 0
#define TEST_REPLICATE_TWO 0

#if TEST_INIT
static int
test_connect_to(const char* host, const char* service)
{
	int fd = -1;
	int ret;
	struct addrinfo hints;
	struct addrinfo* r;
	struct addrinfo* res = NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags =
		  AI_ADDRCONFIG             // only valid protocols (v4 or v6)
		| AI_ALL                    // return all ipv4 AND all ipv6 addresses
		| AI_NUMERICSERV            // numeric port
		;

	ret = getaddrinfo(host, service, &hints, &res);
	if (ret < 0) {
		return -1;
	}
	for (r = res; r; r = r->ai_next) {
		fd = socket(r->ai_family, r->ai_socktype, r->ai_protocol);
		if (fd < 0)
			continue;
		ret = connect(fd, r->ai_addr, r->ai_addrlen);
		if (!ret) {
//			fprintf(stderr, "%s: connected to %s:%s\n", __FUNCTION__, host, service);
			break;
		}
	}
	if (res)
		freeaddrinfo(res);
	return fd;
}
#endif

#if TEST_INIT
static
void TestInit(CuTest* tc)
{
	int fd;
	georep_t* gq = gr_init("tcp://127.0.0.1:10101", NULL);
	CuAssertPtrNotNullMsg(tc, "gq", gq);

	fd = test_connect_to("127.0.0.1", "10101");
	CuAssertTrue(tc, fd > -1);
	close(fd);

	gr_shutdown(&gq);
	CuAssertTrue(tc, gq == NULL);
}
#endif

#if TEST_CONTACT
static
void TestContact(CuTest* tc)
{
	int clustersize = 0;
	int i;
	int ret;
	georep_t* gq1 = gr_init("tcp://127.0.0.1:10201", "1");
	georep_t* gq2 = gr_init("tcp://127.0.0.1:10202", "2");

	CuAssertPtrNotNullMsg(tc, "gq1", gq1);
	CuAssertPtrNotNullMsg(tc, "gq2", gq2);

	ret = gr_contact(gq1, "tcp://127.0.0.1:10202", &clustersize);
	CuAssertTrue(tc, ret == 0);

	for (i = 0; i < 5; ++i) {
		if (clustersize == 2)
			break;
		sleep(1);
		gr_contact(gq1, NULL, &clustersize);
	}

	CuAssertTrue(tc, clustersize == 2);

	ret = gr_contact(gq2, NULL, &clustersize);
	CuAssertTrue(tc, ret == 0);

	for (i = 0; i < 5; ++i) {
		if (clustersize == 2)
			break;
		sleep(1);
		gr_contact(gq2, NULL, &clustersize);
	}

	CuAssertTrue(tc, clustersize == 2);

	gr_shutdown(&gq1);
	CuAssertTrue(tc, gq1 == NULL);
	CuAssertTrue(tc, gq2 != NULL);
	gr_shutdown(&gq2);
	CuAssertTrue(tc, gq2 == NULL);
}
#endif

#if 0 \
	|| TEST_THREE \
	|| TEST_REPLICATE_TO_TWO \
	|| TEST_REPLICATE_AND_CLEAR \
	|| TEST_REPLICATE_TWO \
	|| 0
// final "0" above gives cleaner diffs
static void
waitForClusterSize(CuTest* tc, georep_t* gq, int expected)
{
	int i;
	int clustersize;

	gr_contact(gq, NULL, &clustersize);
	for (i = 0; i < 15; ++i) {
		if (clustersize == expected)
			break;
		sleep(1);
		gr_contact(gq, NULL, &clustersize);
	}
	CuAssertTrue(tc, clustersize == expected);
}
#endif

#if TEST_THREE
static
void TestThree(CuTest* tc)
{
	int clustersize = 0;
	int ret;
	georep_t* gq1 = gr_init("tcp://127.0.0.1:10301", "1");
	georep_t* gq2 = gr_init("tcp://127.0.0.1:10302", "2");
	georep_t* gq3 = gr_init("tcp://127.0.0.1:10303", "3");

	CuAssertPtrNotNullMsg(tc, "gq1", gq1);
	CuAssertPtrNotNullMsg(tc, "gq2", gq2);
	CuAssertPtrNotNullMsg(tc, "gq3", gq3);

	ret = gr_contact(gq1, "tcp://127.0.0.1:10302", &clustersize);
	CuAssertTrue(tc, ret == 0);

	ret = gr_contact(gq2, "tcp://127.0.0.1:10303", &clustersize);
	CuAssertTrue(tc, ret == 0);

	waitForClusterSize(tc, gq1, 3);
	waitForClusterSize(tc, gq2, 3);
	waitForClusterSize(tc, gq3, 3);

	gr_shutdown(&gq1);
	gr_shutdown(&gq2);
	gr_shutdown(&gq3);
}
#endif

#if TEST_ONE_ALIVE_ONE_DEAD
static
void TestOneAliveOneDead(CuTest* tc)
{
	int clustersize = 0;
	int i;
	int ret;
	georep_t* gq1 = gr_init("tcp://127.0.0.1:10401", "1");

	CuAssertPtrNotNullMsg(tc, "gq1", gq1);

	ret = gr_contact(gq1, "tcp://127.0.0.1:10402", &clustersize);
	CuAssertTrue(tc, ret == 0);

	for (i = 0; i < 10; ++i) {
		sleep(1);
		gr_contact(gq1, NULL, &clustersize);
		CuAssertTrue(tc, clustersize == 1);
	}

	gr_shutdown(&gq1);
}
#endif

#if TEST_REPLICATE_TO_TWO
static void
TestReplicateToTwo_cb1(void* arg, const char* key, zchunk_t* value)
{
	int* cb_called = arg;

	fprintf(stderr, "%s: called for key `%s' value `%.*s'\n",
		__FUNCTION__, key,
		(int) zchunk_size(value), zchunk_data(value));
	if (strcmp(key, "key1") == 0)
		*cb_called = 1;
}

static
void TestReplicateToTwo(CuTest* tc)
{
	int clustersize = 0;
	int i;
	int ret;
	int cb2_called = 0;
	int cb3_called = 0;
	georep_t* gq1 = gr_init("tcp://127.0.0.1:10601", "1");
	georep_t* gq2 = gr_init("tcp://127.0.0.1:10602", "2");
	georep_t* gq3 = gr_init("tcp://127.0.0.1:10603", "3");
	gr_tolerate_failures(gq1, 1);
	gr_set_adopted_cb(gq2, TestReplicateToTwo_cb1, &cb2_called);
	gr_set_adopted_cb(gq3, TestReplicateToTwo_cb1, &cb3_called);

	ret = gr_contact(gq1, "tcp://127.0.0.1:10602", &clustersize);
	CuAssertTrue(tc, ret == 0);
	ret = gr_contact(gq2, "tcp://127.0.0.1:10603", &clustersize);
	CuAssertTrue(tc, ret == 0);

	waitForClusterSize(tc, gq1, 3);
	waitForClusterSize(tc, gq2, 3);
	waitForClusterSize(tc, gq3, 3);

	gr_cond_t* cond = gr_replicate(gq1, "key1", "value1", -1);
	fprintf(stderr, "%s: wait on cond at %p\n", __FUNCTION__, cond);
	ret = gr_cond_wait(cond, 4 * 1000);
	CuAssertTrue(tc, ret == 0);
	fprintf(stderr, "%s: message cond released\n", __FUNCTION__);
	gr_cond_unlock(cond);
	gr_cond_free(cond);

	sleep(1);

	gr_shutdown(&gq1);

	for (i = 0; i < 10; ++i) {
		if (cb2_called || cb3_called)
			break;
		sleep(1);
	}

	gr_shutdown(&gq2);
	gr_shutdown(&gq3);

	CuAssertTrue(tc, cb2_called != cb3_called);
}
#endif

#if TEST_REPLICATE_AND_CLEAR
static void
TestReplicateAndClear_cb1(void* arg, const char* key, zchunk_t* value)
{
	int* cb_called = arg;

	fprintf(stderr, "%s: called for key `%s' value `%.*s'\n",
		__FUNCTION__, key,
		(int) zchunk_size(value), zchunk_data(value));
	if (strcmp(key, "key1") == 0)
		*cb_called = 1;
}

static
void TestReplicateAndClear(CuTest* tc)
{
	int clustersize = 0;
	int i;
	int ret;
	int cb1_called = 0;
	gr_cond_t* cond1;
	gr_cond_t* cond2;
	georep_t* gq1 = gr_init("tcp://127.0.0.1:10701", "1");
	georep_t* gq2 = gr_init("tcp://127.0.0.1:10702", "2");
	gr_set_adopted_cb(gq2, TestReplicateAndClear_cb1, &cb1_called);

	ret = gr_contact(gq1, "tcp://127.0.0.1:10702", &clustersize);
	CuAssertTrue(tc, ret == 0);

	waitForClusterSize(tc, gq1, 2);
	waitForClusterSize(tc, gq2, 2);

	cond1 = gr_replicate(gq1, "key1", "value1", -1);
	ret = gr_cond_wait(cond1, 4 * 1000, NULL);
	gr_cond_unlock(cond1);
	CuAssertTrue(tc, ret == 0);
	gr_cond_free(cond1);

	cond2 = gr_clear(gq1, "key1");
	ret = gr_cond_wait(cond2, 4 * 1000, NULL);
	gr_cond_unlock(cond2);
	CuAssertTrue(tc, ret == 0);
	gr_cond_free(cond2);

	gr_shutdown(&gq1);

	for (i = 0; i < 10; ++i) {
		if (cb1_called)
			break;
		sleep(1);
	}
	gr_shutdown(&gq2);
	CuAssertTrue(tc, !cb1_called);
}
#endif

#if TEST_REPLICATE_TWO
static
void TestReplicateTwo(CuTest* tc)
{
	int ret;
	georep_t* gq1 = gr_init("tcp://127.0.0.1:10801", "1");
	georep_t* gq2 = gr_init("tcp://127.0.0.1:10802", "2");
	georep_t* gq3 = gr_init("tcp://127.0.0.1:10803", "3");

	ret = gr_contact(gq1, "tcp://127.0.0.1:10802", NULL);
	CuAssertTrue(tc, ret == 0);
	ret = gr_contact(gq3, "tcp://127.0.0.1:10802", NULL);
	CuAssertTrue(tc, ret == 0);

	waitForClusterSize(tc, gq1, 3);
	waitForClusterSize(tc, gq2, 3);
	waitForClusterSize(tc, gq3, 3);

	gr_cond_t* cond1 = NULL;
	gr_cond_t* cond2 = NULL;
	cond1 = gr_replicate(gq1, "key1", "value1", -1);
	ret = gr_cond_wait(cond1, 4 * 1000);
	gr_cond_unlock(cond1);
	CuAssertTrue(tc, ret == 0);
	gr_cond_free(cond1);

	cond2 = gr_replicate(gq1, "key2", "value2", -1);
	ret = gr_cond_wait(cond2, 4 * 1000);
	gr_cond_unlock(cond2);
	CuAssertTrue(tc, ret == 0);
	gr_cond_free(cond2);

	sleep(1);

	gr_shutdown(&gq1);
	gr_shutdown(&gq2);
	gr_shutdown(&gq3);
}
#endif

static
CuSuite* CuGetSuite(void)
{
	CuSuite* suite = CuSuiteNew();

#if TEST_INIT
	SUITE_ADD_TEST(suite, TestInit);
#endif

#if TEST_CONTACT
	SUITE_ADD_TEST(suite, TestContact);
#endif

#if TEST_THREE
	SUITE_ADD_TEST(suite, TestThree);
#endif

#if TEST_ONE_ALIVE_ONE_DEAD
	SUITE_ADD_TEST(suite, TestOneAliveOneDead);
#endif

#if TEST_REPLICATE_TO_TWO
	SUITE_ADD_TEST(suite, TestReplicateToTwo);
#endif

#if TEST_REPLICATE_AND_CLEAR
	SUITE_ADD_TEST(suite, TestReplicateAndClear);
#endif

#if TEST_REPLICATE_TWO
	SUITE_ADD_TEST(suite, TestReplicateTwo);
#endif

	return suite;

}

int
main(int argc, char* argv[])
{
	CuString* output = CuStringNew();
	CuSuite* suite = CuGetSuite();

	(void) argc;
	(void) argv;

	//system("rm -rf spool; mkdir spool");

	gr_log_level(GR_LOG_DEBUG);
	zsys_init();
	CuSuiteRun(suite);
	CuSuiteSummary(suite, output);
	CuSuiteDetails(suite, output);
	printf("%s\n", output->buffer);
	CuStringDelete(output);
	CuSuiteDelete(suite);

	exit(0);
}

