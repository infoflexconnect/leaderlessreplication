
#include "georep.h"
#include "log.h"
#include "helpers.h"

#include <getopt.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <mysql/mysql.h>
#include <mysql/mysqld_error.h>

#include <leveldb/c.h>

#define WITH_SPREAD 0

#if WITH_SPREAD
#include <sp.h>
#endif

static int client_count = 1;
static int message_count = 1;
static char* nodeid = NULL;
static int duration = 10;
static int warmup = 5;
static gr_log_level_t loglevel = GR_LOG_INFO;
static int do_update = 0;

// GeoQueue specifics
static georep_t* queue = NULL;

// MariaDB specifics
static int use_find = 1;
static char* host = NULL;

// LevelDB specifics
static leveldb_t* ldb = NULL;
static leveldb_readoptions_t* readoptions = NULL;
static leveldb_writeoptions_t* writeoptions = NULL;

// Spread specifics
#if WITH_SPREAD
static const char* spread_name = "4803";
static const char* spread_user = "node";
static mailbox spread_mbox;
static char Private_group[MAX_GROUP_NAME];
static char ret_groups[9][MAX_GROUP_NAME];
#define MAX_MESSLEN     1000
//static char spread_mess[MAX_MESSLEN];
static char spread_recv_mess[MAX_MESSLEN];
#endif

typedef enum {
	BACKEND_GEOQUEUE,
	BACKEND_MARIADB,
	BACKEND_LEVELDB,
#if WITH_SPREAD
	BACKEND_SPREAD,
#endif
} backend_t;

static backend_t backend = BACKEND_GEOQUEUE;

typedef struct {
	int no;
	int* tps;
} TestReplicateMany_info_t;

static void
close_cond(gr_cond_t* cond)
{
	if (!cond)
		return;

	int ret;
	do {
		ret = gr_cond_wait(cond, 10 * 1000);
		if ((ret == ETIMEDOUT) || // helgrind bug
				((ret < 0) && (errno == ETIMEDOUT))) {
			gr_log(GR_LOG_DEBUG, "%s:%d: replicas are slow, cond at %p\n",
					__FUNCTION__, __LINE__, cond);
		}
	} while ((ret == ETIMEDOUT) || ((ret < 0) && (errno == ETIMEDOUT)));
	gr_log(GR_LOG_DEBUG, "%s:%d: wait returned %d, unlock cond at %p\n",
		__FUNCTION__, __LINE__, ret, cond);
	gr_cond_free(cond);
}

static void
replicate_entry(const char* keybuf, const char* valuebuf)
{
	gr_cond_t* cond = gr_replicate(queue, keybuf, valuebuf, -1);
	close_cond(cond);
}

static void
replicate_existing_entry(const char* keybuf, const char* valuebuf)
{
	gr_cond_t* cond = gr_update(queue, keybuf, valuebuf, -1);
	close_cond(cond);
}

static void
delete_entry(const char* keybuf)
{
	gr_cond_t* cond = gr_clear(queue, keybuf);
	close_cond(cond);
}

static MYSQL_STMT*
create_prepared_statement(MYSQL* mysql, const char* sql)
{
	int rc;
	MYSQL_STMT* stmt = mysql_stmt_init(mysql);
retry:
	rc = mysql_stmt_prepare(stmt, sql, strlen(sql));
	if (!rc) {
		return stmt;
	}
	int myerrno = mysql_errno(mysql);
	switch (myerrno) {
		case ER_LOCK_DEADLOCK: // 1213
			goto retry;
	}
	fprintf(stderr,
		"%s: Could not prepare statement `%s', MySQL error %d errno %d: %s\n",
		__FUNCTION__, sql, rc, mysql_errno(mysql), mysql_error(mysql));
	mysql_stmt_close(stmt);
	return NULL;
}

static int
start_transaction(MYSQL* mysql)
{
	int err;
	int n;
	char sql[256];

	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: enter\n", __FUNCTION__);
	n = sprintf(sql, "START TRANSACTION");
retry:
	err = mysql_real_query(mysql, sql, n);
	if (err != 0) {
		int myerrno = mysql_errno(mysql);
		switch (myerrno) {
			case ER_LOCK_DEADLOCK: // 1213
				goto retry;
		}
		fprintf(stderr, "%s: SQL `%s' returned %d errno %d error: %s\n",
			__FUNCTION__, sql, err, mysql_errno(mysql), mysql_error(mysql));
	}
	return err;
}

static int
commit_or_rollback(MYSQL* mysql, int err)
{
	int err2;
	static __thread MYSQL_STMT* commit_stmt = NULL;
	static __thread MYSQL_STMT* rollback_stmt = NULL;

	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: enter\n", __FUNCTION__);

	if (!commit_stmt) {
		commit_stmt = create_prepared_statement(mysql, "COMMIT");
	}

	if (!rollback_stmt) {
		rollback_stmt = create_prepared_statement(mysql, "ROLLBACK");
	}

retry:
	if (err == 0) {
		err2 = mysql_stmt_execute(commit_stmt);
	} else {
		err2 = mysql_stmt_execute(rollback_stmt);
	}
	if (err2 != 0) {
		int myerrno = mysql_errno(mysql);
		switch (myerrno) {
			case ER_ERROR_DURING_COMMIT: // 1180
				return 0;
			case ER_LOCK_DEADLOCK: // 1213
				goto retry;
		}
		fprintf(stderr,
			"%s: COMMIT/ROLLBACK returned %d errno %d error: %s\n",
			__FUNCTION__, err2, myerrno, mysql_error(mysql));
	}
	return err2;
}

static int
store_entry(MYSQL* mysql, const char* keybuf, const char* valuebuf)
{
	int err;
	static __thread MYSQL_STMT* insert_stmt = NULL;
	MYSQL_BIND params[2];

	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: enter\n", __FUNCTION__);

	if (!insert_stmt) {
		insert_stmt = create_prepared_statement(mysql,
			"INSERT INTO entries (holduntil, msgid, value) VALUES (CURRENT_TIMESTAMP(), ?, ?)");
	}

	err = start_transaction(mysql);
	if (err != 0) {
		return err;
	}

	memset(params, 0, sizeof(params));

	params[0].buffer_type = MYSQL_TYPE_VAR_STRING;
	params[0].buffer_length = strlen(keybuf);
	params[0].buffer = (char*) keybuf;

	params[1].buffer_type = MYSQL_TYPE_VAR_STRING;
	params[1].buffer_length = strlen(valuebuf);
	params[1].buffer = (char*) valuebuf;

	err = mysql_stmt_bind_param(insert_stmt, params);
	if (err != 0) {
		fprintf(stderr, "%s: mysql_stmt_bind_param returned %d errno %d error: %s\n",
			__FUNCTION__, err, mysql_errno(mysql), mysql_error(mysql));
	}

	err = mysql_stmt_execute(insert_stmt);
	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: insert key `%s' value `%s'\n", __FUNCTION__, keybuf, valuebuf);
	if (err != 0) {
		// ER_QUERY_INTERRUPTED / 1317
		fprintf(stderr, "%s: insert_stmt returned %d errno %d error: %s\n",
			__FUNCTION__, err, mysql_errno(mysql), mysql_error(mysql));
	}

	err = commit_or_rollback(mysql, err);

	return err;
}

static int
find_first_entry(MYSQL* mysql, char* keybuf, char* valuebuf)
{
	int err;
	static __thread MYSQL_STMT* find_first_stmt = NULL;
	static __thread MYSQL_STMT* update_stmt = NULL;
	unsigned long lengths[2];
    MYSQL_BIND result[2];

	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: enter\n", __FUNCTION__);

	if (!find_first_stmt) {
		find_first_stmt = create_prepared_statement(mysql,
			"SELECT msgid, value"
			" FROM entries"
			" WHERE holduntil<=NOW()"
			" ORDER BY holduntil"
			" LIMIT 1"
			" FOR UPDATE");
	}
	if (!update_stmt) {
		update_stmt = create_prepared_statement(mysql,
			"UPDATE entries"
			" SET holduntil=DATE_ADD(NOW(), INTERVAL 1 MINUTE)"
			" WHERE msgid=?");
	}

	keybuf[0] = 0;
	valuebuf[0] = 0;

	err = start_transaction(mysql);
	if (err != 0) {
		goto done;
	}

	memset(result, 0, sizeof(result));
	memset(lengths, 0, sizeof(lengths));
	result[0].buffer_type = MYSQL_TYPE_VAR_STRING;
	result[0].buffer = keybuf;
	result[0].buffer_length = 256;
	result[0].length = &lengths[0];
	result[1].buffer_type = MYSQL_TYPE_VAR_STRING;
	result[1].buffer = valuebuf;
	result[1].buffer_length = 256;
	result[1].length = &lengths[1];
	mysql_stmt_bind_result(find_first_stmt, result);

	err = mysql_stmt_execute(find_first_stmt);
	if (err != 0) {
		fprintf(stderr, "%s: find_first_stmt returned %d errno %d error: %s\n",
			__FUNCTION__, err, mysql_errno(mysql), mysql_error(mysql));
		goto done;
	}

retry:
	err = mysql_stmt_store_result(find_first_stmt);
	if (err != 0) {
		int myerrno = mysql_errno(mysql);
		switch (myerrno) {
			case ER_LOCK_DEADLOCK: // 1213
				goto retry;
			case ER_QUERY_INTERRUPTED: // 1317
				// now what?
				break;
		}
		if (loglevel <= GR_LOG_DEBUG) {
			fprintf(stderr, "%s: mysql_stmt_store_result returned %d errno %d error: %s\n",
				__FUNCTION__, err, mysql_errno(mysql), mysql_error(mysql));
		}
		goto done;
	}

	err = mysql_stmt_fetch(find_first_stmt);
	if (err != 0) {
		if (err == MYSQL_NO_DATA) {
			err = 0;
			goto done;
		}
		fprintf(stderr, "%s: mysql_stmt_fetch returned %d errno %d error: %s\n",
			__FUNCTION__, err, mysql_errno(mysql), mysql_error(mysql));
		goto done;
	}

	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: found entry `%s'\n", __FUNCTION__, keybuf);

	err = mysql_stmt_bind_param(update_stmt, result);
	if (err != 0) {
		fprintf(stderr, "%s: mysql_stmt_bind_param returned %d errno %d error: %s\n",
			__FUNCTION__, err, mysql_errno(mysql), mysql_error(mysql));
	}
retry_update:
	err = mysql_stmt_execute(update_stmt);
	if (err != 0) {
		int myerrno = mysql_errno(mysql);
		switch (myerrno) {
			case ER_LOCK_DEADLOCK: // 1213
				goto retry_update;
		}
		fprintf(stderr, "%s: update_stmt (%s) returned %d errno %d error: %s\n",
			__FUNCTION__, keybuf, err, mysql_errno(mysql), mysql_error(mysql));
		goto done;
	}

done:
	err = commit_or_rollback(mysql, err);

	return err;
}

static int
update_entry(MYSQL* mysql, const char* keybuf, const char* valuebuf)
{
	int err;
	static __thread MYSQL_STMT* update_stmt = NULL;
	MYSQL_BIND params[2];

	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: enter\n", __FUNCTION__);

	if (!update_stmt) {
		update_stmt = create_prepared_statement(mysql,
			"UPDATE entries SET value=? WHERE msgid=?");
	}

	err = start_transaction(mysql);
	if (err != 0) {
		return err;
	}

	memset(params, 0, sizeof(params));

	params[0].buffer_type = MYSQL_TYPE_VAR_STRING;
	params[0].buffer_length = strlen(valuebuf);
	params[0].buffer = (char*) valuebuf;

	params[1].buffer_type = MYSQL_TYPE_VAR_STRING;
	params[1].buffer_length = strlen(keybuf);
	params[1].buffer = (char*) keybuf;

	err = mysql_stmt_bind_param(update_stmt, params);
	if (err != 0) {
		fprintf(stderr, "%s: mysql_stmt_bind_param returned %d errno %d error: %s\n",
			__FUNCTION__, err, mysql_errno(mysql), mysql_error(mysql));
	}

	err = mysql_stmt_execute(update_stmt);
	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: update key `%s' with value `%s'\n", __FUNCTION__, keybuf, valuebuf);
	if (err != 0) {
		// ER_QUERY_INTERRUPTED / 1317
		fprintf(stderr, "%s: stmt returned %d errno %d error: %s\n",
			__FUNCTION__, err, mysql_errno(mysql), mysql_error(mysql));
	}

	err = commit_or_rollback(mysql, err);

	return err;
}

static int
remove_entry(MYSQL* mysql, const char* keybuf)
{
	int err;
	static __thread MYSQL_STMT* delete_stmt = NULL;
	MYSQL_BIND params[1];

	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: enter\n", __FUNCTION__);

	if (!delete_stmt) {
		delete_stmt = create_prepared_statement(mysql,
			"DELETE FROM entries WHERE msgid=?");
	}

	err = start_transaction(mysql);
	if (err != 0)
		return err;

	memset(params, 0, sizeof(params));
	params[0].buffer_type = MYSQL_TYPE_VAR_STRING;
	params[0].buffer_length = strlen(keybuf);
	params[0].buffer = (char*) keybuf;
	err = mysql_stmt_bind_param(delete_stmt, params);
	if (err != 0) {
		fprintf(stderr, "%s: mysql_stmt_bind_param returned %d errno %d error: %s\n",
			__FUNCTION__, err, mysql_errno(mysql), mysql_error(mysql));
	}

	for (;;) {
		err = mysql_stmt_execute(delete_stmt);
		if (err == 0)
			break;
		int men = mysql_errno(mysql);
		if ((err == 1) && (men == ER_LOCK_DEADLOCK))
			continue;
		if (err != 0) {
			fprintf(stderr, "%s: delete_stmt returned %d errno %d error: %s\n",
				__FUNCTION__, err, men, mysql_error(mysql));
		}
		break;
	}

	err = commit_or_rollback(mysql, err);

	return err;
}

static void*
TestReplicateMany_client_thread(void* arg)
{
	int err;
	int i;
	int keylen;
	int valuelen;
	int prev_count = 0;
//	int count_diff;
	int ret;
	TestReplicateMany_info_t* info = arg;
	MYSQL* mysql = NULL;
	char* errptr = NULL;
	char keybuf[32];
	char valuebuf[32];
	struct timeval start[1];
	struct timeval last[1];
	struct timeval now[1];

#if 0
	if (loglevel <= GR_LOG_INFO)
		fprintf(stderr, "%s: client %d starts\n", __FUNCTION__, info->no);
#endif
	switch (backend) {
		case BACKEND_GEOQUEUE:
		break;

		case BACKEND_MARIADB:
		mysql_thread_init();
		mysql = mysql_init(NULL);
		if (mysql_real_connect(mysql, host, "user", "pw", "queue", 0, NULL, 0) == NULL) {
			fprintf(stderr, "%s: can not connect to MySQL: errno=%d, error=%s\n",
					__FUNCTION__, mysql_errno(mysql), mysql_error(mysql));
			goto done;
		}

		err = mysql_autocommit(mysql, 0);
		if (err != 0) {
			fprintf(stderr, "%s: mysql_autocommit returned %d\n", __FUNCTION__, err);
			goto done;
		}
		break;

		case BACKEND_LEVELDB:
		break;

#if WITH_SPREAD
		case BACKEND_SPREAD:
		break;
#endif
	}

	gettimeofday(start, NULL);
	last[0] = start[0];
	for (i = 0; ; ++i) {
		if ((message_count > 0) && (i >= message_count))
			break;

		keylen = sprintf(keybuf, "key-%s-%02d-%04d", nodeid, info->no, i);
		valuelen = sprintf(valuebuf, "value-%s-%02d-%04d", nodeid, info->no, i);

		// store
		switch (backend) {
			case BACKEND_GEOQUEUE:
				replicate_entry(keybuf, valuebuf);
				break;
			case BACKEND_MARIADB:
				store_entry(mysql, keybuf, valuebuf);
				break;
			case BACKEND_LEVELDB:
				errptr = NULL;
				leveldb_put(ldb, writeoptions, keybuf, keylen, valuebuf, valuelen, &errptr);
				if (errptr) {
					fprintf(stderr, "%s: could not store record `%s' in LevelDB: %s\n",
						__FUNCTION__, keybuf, errptr);
				}
				break;
#if WITH_SPREAD
			case BACKEND_SPREAD:
				break;
#endif
		}

		// find
		switch (backend) {
			case BACKEND_GEOQUEUE:
				break;
			case BACKEND_MARIADB:
				if (use_find) {
					ret = find_first_entry(mysql, keybuf, valuebuf);
					if (ret != 0)
						continue;
				}
				break;
			case BACKEND_LEVELDB:
				break;
#if WITH_SPREAD
			case BACKEND_SPREAD:
				break;
#endif
		}

		// update
		if (do_update) {
			valuelen = sprintf(valuebuf, "update-%s-%02d-%04d", nodeid, info->no, i);
			switch (backend) {
				case BACKEND_GEOQUEUE:
					replicate_existing_entry(keybuf, valuebuf);
					break;
				case BACKEND_MARIADB:
					if (use_find) {
						ret = update_entry(mysql, keybuf, valuebuf);
						if (ret != 0)
							continue;
					}
					break;
				case BACKEND_LEVELDB:
					errptr = NULL;
					leveldb_put(ldb, writeoptions, keybuf, keylen, valuebuf, valuelen, &errptr);
					if (errptr) {
						fprintf(stderr, "%s: could not update record `%s' in LevelDB: %s\n",
							__FUNCTION__, keybuf, errptr);
					}
					break;
#if WITH_SPREAD
				case BACKEND_SPREAD:
					break;
#endif
			}
		}

		// delete
		switch (backend) {
			case BACKEND_GEOQUEUE:
				delete_entry(keybuf);
				break;
			case BACKEND_MARIADB:
				if (keybuf[0])
					remove_entry(mysql, keybuf);
				break;
			case BACKEND_LEVELDB:
				errptr = NULL;
				leveldb_delete(ldb, writeoptions, keybuf, keylen, &errptr);
				if (errptr) {
					fprintf(stderr, "%s: could not delete record `%s' in LevelDB: %s\n",
						__FUNCTION__, keybuf, errptr);
				}
				break;
#if WITH_SPREAD
			case BACKEND_SPREAD:
				break;
#endif
		}

		gettimeofday(now, NULL);
		int nsec = now->tv_sec - start->tv_sec;
		if (nsec > (warmup + duration))
			break;
		info->tps[nsec]++;

		if (!info->no && (now->tv_sec > last->tv_sec)) {
			if (loglevel <= GR_LOG_INFO) {
				struct tm tm[1];
				localtime_r(&now->tv_sec, tm);
				if (message_count > -1) {
					fprintf(stderr, "%02d:%02d:%02d node %s client %d has sent %d/%d messages, %d this second (total on node: %d)\n",
						tm->tm_hour, tm->tm_min, tm->tm_sec,
						nodeid, info->no, i, message_count, i - prev_count, (i - prev_count) * client_count);
				} else {
					fprintf(stderr, "%02d:%02d:%02d node %s client %d has sent %d messages, %d this second (total on node: %d)\n",
						tm->tm_hour, tm->tm_min, tm->tm_sec,
						nodeid, info->no, i, i - prev_count, (i - prev_count) * client_count);
				}
			}
			last[0] = now[0];
			prev_count = i;
		}
	}

done:

	switch (backend) {
		case BACKEND_GEOQUEUE:
		break;

		case BACKEND_MARIADB:
		if (use_find) {
			for (;;) {
				ret = find_first_entry(mysql, keybuf, valuebuf);
				if ((ret != 0) || !keybuf[0])
					break;
				remove_entry(mysql, keybuf);
			}
		}
		mysql_close(mysql);
		mysql_thread_end();
		break;

		case BACKEND_LEVELDB:
		break;

#if WITH_SPREAD
		case BACKEND_SPREAD:
		break;
#endif
	}

	if (loglevel <= GR_LOG_DEBUG)
		fprintf(stderr, "%s: client %d stops\n", __FUNCTION__, info->no);

	pthread_exit(NULL);
}

static int
compare_int(const void* p1, const void* p2)
{
	const int* ip1 = p1;
	const int* ip2 = p2;

	return *ip1 - *ip2;
}

int
main(int argc, char* argv[])
{
	int err;
	int i;
	int j;
	int ret;
	int c;
	int option_index = 0;
	int node_count = 1;
	int tolerated_failures = 2;
	int truncate = 0;
	int min_rtt = -1;
#if WITH_SPREAD
	int joined_members = 0;
#endif
	const char* sql;
	char* errptr = NULL;
	int* tps = NULL;
	MYSQL* mysql;
	pthread_attr_t attr;
	struct timeval start[1];
	leveldb_env_t* ldb_env = NULL;

	host = strdup("localhost");

	gr_log_init();

	for (;;) {
		static struct option long_options[] = {
	/* 0 */	{"no-find",  0, NULL, 0},
	/* 1 */	{"mariadb",  0, NULL, 0},
	/* 2 */	{"leveldb",  0, NULL, 0},
	/* 3 */	{"minrtt",   1, NULL, 0},
#if WITH_SPREAD
	/* 4 */	{"spread",   0, NULL, 0},
#endif
			{"clients",  1, 0, 'c'},
			{"messages", 1, 0, 'm'},
			{"nodes",    1, 0, 'n'},
			{"debug",    0, 0, 'd'},
			{"quiet",    0, 0, 'q'},
			{"truncate", 0, NULL, 'T'},
			{"host",     1, NULL, 'H'},
			{"failures", 1, 0, 'F'},
			{"time",     1, 0, 't'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "c:dm:n:qt:", long_options, &option_index);
		if (c == -1) break;
		switch (c) {
			case 0:
				switch (option_index) {
					case 0:
						use_find = 0;
						break;
					case 1:
						backend = BACKEND_MARIADB;
						break;
					case 2:
						backend = BACKEND_LEVELDB;
						break;
					case 3:
						min_rtt = atoi(optarg);
						break;
#if WITH_SPREAD
					case 4:
						backend = BACKEND_SPREAD;
						break;
#endif
				}
				break;

			case 'c':
//				fprintf(stderr, "%s:%d: %s clients\n", argv[0], __LINE__, optarg);
				client_count = atoi(optarg);
				break;

			case 'd':
				loglevel = GR_LOG_DEBUG;
				break;

			case 'q':
				loglevel = GR_LOG_NOTHING;
				break;

			case 'm':
//				fprintf(stderr, "%s:%d: %s messages\n", argv[0], __LINE__, optarg);
				message_count = atoi(optarg);
				break;

			case 'n':
//				fprintf(stderr, "%s:%d: %s nodes\n", argv[0], __LINE__, optarg);
				node_count = atoi(optarg);
				break;

			case 't':
//				fprintf(stderr, "%s:%d: %s nodes\n", argv[0], __LINE__, optarg);
				duration = atoi(optarg);
				if (duration < 1)
					duration = 1;
				break;

			case 'F':
				tolerated_failures = atoi(optarg);
				break;

			case 'H':
				free(host);
				host = strdup(optarg);
				break;

			case 'T':
				truncate = 1;
				break;

			default:
				fprintf(stderr, "getopt_long returned %d / %c\n", c, c);
		}
	}

	pthread_t clients[client_count];
	TestReplicateMany_info_t client_info[client_count];

	if ((optind >= argc) || (node_count < 1)) {
		fprintf(stderr,
			"usage: %s [options] -n node_count my-id my-ip:port other-ip:port [other-ip:port...]\n",
			argv[0]);
		goto done;
	}

	nodeid = argv[optind];

	switch (backend) {
		case BACKEND_MARIADB:
		mysql_library_init(0, NULL, NULL);
		mysql = mysql_init(NULL);
		if (mysql_real_connect(mysql, host, "user", "pw", "queue", 0, NULL, 0) == NULL) {
			fprintf(stderr, "%s: can not connect to MySQL: errno=%d, error=%s\n",
					argv[0], mysql_errno(mysql), mysql_error(mysql));
			goto done;
		}

		if (truncate) {
			sql = "TRUNCATE entries";
			err = mysql_real_query(mysql, sql, strlen(sql));
			if (loglevel <= GR_LOG_DEBUG)
				fprintf(stderr, "%s: query `%s' returned %d\n", argv[0], sql, err);
		}
		break;

		case BACKEND_GEOQUEUE:
		gr_log_level(loglevel);
		zsys_init();

		if (gr_log_enabled(GR_LOG_INFO)) {
			fprintf(stderr, "%s: calling gr_init(%s, %s)\n",
				argv[0], argv[optind + 1], nodeid);
		}
		ldb_env = leveldb_create_default_env();
		queue = gr_init(argv[optind + 1], nodeid);
		if (!queue) {
			fprintf(stderr, "%s: can not initialize GeoQueue(%s, %s)\n",
				argv[0], argv[optind + 1], nodeid);
			goto done;
		}
		optind += 2;
		while (optind < argc) {
			if (gr_log_enabled(GR_LOG_INFO)) {
				fprintf(stderr, "%s: calling gr_contact(%s)\n",
					argv[0], argv[optind]);
			}
			ret = gr_contact(queue, argv[optind], NULL);
			(void) ret;
			optind++;
		}
		waitForClusterSize(NULL, queue, node_count);
		if (tolerated_failures > node_count - 1)
			tolerated_failures = node_count - 1;
		// gr_set_high_watermark(queue, 1);
		gr_tolerate_failures(queue, tolerated_failures);
		if (min_rtt > -1)
			gr_set_min_rtt(queue, min_rtt);
		break;

		case BACKEND_LEVELDB:
#define LDB_FILENAME "node-test.ldb"
		system("rm -rf " LDB_FILENAME);
		ldb_env = leveldb_create_default_env();
		leveldb_options_t* options = leveldb_options_create();
		leveldb_options_set_env(options, ldb_env);
		leveldb_options_set_create_if_missing(options, 1);
		ldb = leveldb_open(options, LDB_FILENAME, &errptr);
	    leveldb_options_destroy(options);
		if (errptr) {
			fprintf(stderr, "node %s: could not open `%s': %s\n",
				nodeid, LDB_FILENAME, errptr);
			free(errptr);
			goto done;
		}
		readoptions = leveldb_readoptions_create();
		writeoptions = leveldb_writeoptions_create();
		break;

#if WITH_SPREAD
		case BACKEND_SPREAD:
		ret = SP_connect(spread_name, spread_user, 0, 1, &spread_mbox, Private_group);
		if (ret != ACCEPT_SESSION) {
			fprintf(stderr, "node %s: SP_connect returned %d: %s\n",
				nodeid, ret, SP_strerror(ret));
			goto done;
		}

		SP_join(spread_mbox, "node");

		while (joined_members < node_count) {
			int16 mess_type = 0;
			int dummy_endian_mismatch = 0;
			int service_type = 0;
			int num_groups = 0;
			ret = SP_receive(spread_mbox, &service_type, nodeid,
				9, /* max groups */
				&num_groups, ret_groups,
				&mess_type, &dummy_endian_mismatch, sizeof(spread_recv_mess), spread_recv_mess);
			if (ret < 0) {
				fprintf(stderr, "node %s: SP_receive returned %d: %s\n",
					nodeid, ret, SP_strerror(ret));
				goto done;
			}
			if (!Is_membership_mess(service_type)) {
				continue;
			}
			if (Is_reg_memb_mess(service_type)) {
				fprintf(stderr,
					"node %s: Received REGULAR membership for group with %d members, where I am member %d:\n",
					nodeid, num_groups, mess_type);
				for (i = 0; i < num_groups; i++)
					printf("\t%s\n", &ret_groups[i][0]);
				/* update count of joined members */
				joined_members = num_groups;
			}
		}
		break;
#endif

	}

	pthread_attr_init(&attr);
	pthread_attr_setstacksize(&attr, 256 * 1024);

	warmup = 5;
	int ntps = warmup + duration + 1;

	for (i = 0; i < client_count; ++i) {
		client_info[i].no = i;
		client_info[i].tps = calloc((ntps), sizeof(int));
		ret = pthread_create(&clients[i], &attr, TestReplicateMany_client_thread, &client_info[i]);
	}
	gr_log(GR_LOG_INFO, "node %s: %d client threads started\n", nodeid, client_count);

	gettimeofday(start, NULL);

	sleep(ntps + 5);

	gr_log(GR_LOG_INFO, "node %s: wait for clients to stop\n", nodeid);
	for (i = 0; i < client_count; ++i) {
		gr_log(GR_LOG_DEBUG, "node %s: wait for client %d to stop\n", nodeid, i);
		pthread_cancel(clients[i]); // brutal, but whatever
		ret = pthread_join(clients[i], NULL);
	}
	gr_log(GR_LOG_INFO, "node %s: all clients stopped\n", nodeid);

	tps = calloc(duration, sizeof(int));
	for (i = 0; i < client_count; ++i) {
		for (j = 0; j < duration; ++j) {
#if 0
			fprintf(stderr, "node %s client %d sec %d - %d tps\n",
				nodeid, i, j, client_info[i].tps[j]);
#endif
			tps[j] += client_info[i].tps[warmup + j];
		}
	}
	qsort(tps, duration, sizeof(tps[0]), compare_int);
#if 1
	fprintf(stderr, " clients %3d node %s", client_count, nodeid);
	int last = tps[duration - 1];
	int ndigits = 1;
	if (last > 9) ndigits = 2;
	if (last > 99) ndigits = 3;
	if (last > 999) ndigits = 4;
	if (last > 9999) ndigits = 5;
	for (j = 0; j < duration; ++j) {
		//fprintf(stderr, "#clients %3d node %s sec %d - %d tps\n", nodeid, j, tps[j]);
		fprintf(stderr, " %*d", ndigits, tps[j]);
	}
	fprintf(stderr, "\n");
#endif
	fprintf(stderr, "#clients %3d node %s %*d - %*d [ %*d ] %*d - %*d\n",
		client_count,
		nodeid,
		ndigits, tps[0],
		ndigits, tps[1 * duration / 4],
		ndigits, tps[2 * duration / 4],
		ndigits, tps[3 * duration / 4],
		ndigits, tps[duration - 1]);

	if (client_count > 1) {
		for (j = 0; j < duration; ++j) {
			tps[j] /= client_count;
		}
		fprintf(stderr, "#clients %3d node %s %*d - %*d [ %*d ] %*d - %*d  // per client\n",
			client_count,
			nodeid,
			ndigits, tps[0],
			ndigits, tps[1 * duration / 4],
			ndigits, tps[2 * duration / 4],
			ndigits, tps[3 * duration / 4],
			ndigits, tps[duration - 1]);
	}

	free(tps);

	if (backend == BACKEND_GEOQUEUE) {
		sleep(5);
	}

	for (i = 0; i < client_count; ++i) {
		free(client_info[i].tps);
	}

done:
	switch (backend) {
		case BACKEND_MARIADB:
		if (mysql)
			mysql_close(mysql);
		mysql_library_end();
		break;

		case BACKEND_GEOQUEUE:
		gr_shutdown(&queue);
		if (ldb_env)
			leveldb_env_destroy(ldb_env);
		break;

		case BACKEND_LEVELDB:
		if (ldb)
			leveldb_close(ldb);
		if (readoptions)
			leveldb_readoptions_destroy(readoptions);
		if (writeoptions)
			leveldb_writeoptions_destroy(writeoptions);
		if (ldb_env)
			leveldb_env_destroy(ldb_env);
		break;

#if WITH_SPREAD
		case BACKEND_SPREAD:
		SP_disconnect(spread_mbox);
		break;
#endif
	}

	free(host);

	exit(0);
}

