
#include "georep.h"
#include "log.h"
#include "helpers.h"

#include <getopt.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

static int node_count = 3; /* max 15, otherwise zeromq runs out of sockets */
static int client_count = 1;
static int message_count = 3;

typedef struct {
	int no;
	gr_cond_t* start_cond;
	gr_cond_t* stop_cond;
	georep_t* queue;
} TestReplicateMany_info_t;

static void*
TestReplicateMany_thread(void* arg)
{
	int clustersize;
	int ret;
	TestReplicateMany_info_t* info = arg;
	char idbuf[16];
	char addrbuf[32];
	char nextbuf[32];

	sprintf(idbuf, "%d", info->no);
	sprintf(addrbuf, "tcp://127.0.0.1:109%02d", info->no);
	sprintf(nextbuf, "tcp://127.0.0.1:109%02d", (info->no + 1) % node_count);

	info->queue = gr_init(addrbuf, idbuf);
	if (!info->queue) {
		gr_cond_signal(info->start_cond);
		pthread_exit(NULL);
		return NULL;
	}

	gr_log(GR_LOG_DEBUG, "%s: node %d sets high watermark to %d\n",
		__FUNCTION__, info->no, client_count);
	gr_set_high_watermark(info->queue, client_count / (node_count * 2));
	if (node_count < 3)
		gr_tolerate_failures(info->queue, 1);

	ret = gr_contact(info->queue, nextbuf, NULL);
	(void) ret;
	waitForClusterSize(NULL, info->queue, node_count);
	gr_contact(info->queue, NULL, &clustersize);
#if 0
	fprintf(stderr, "%s: thread %d has cluster size %d\n",
		__FUNCTION__, info->no, clustersize);
#endif

	gr_cond_signal(info->start_cond);

#if 0
	fprintf(stderr, "%s: thread %d has started, waiting for stop signal\n",
		__FUNCTION__, info->no);
#endif
	gr_cond_lock(info->stop_cond, 1);
	ret = gr_cond_wait(info->stop_cond, 0);
	gr_log(GR_LOG_DEBUG, "%s: wait for cond at %p returns %d\n",
		__FUNCTION__, info->stop_cond, ret);
	gr_cond_unlock(info->stop_cond);

	sleep(5);
#if 1
	gr_log(GR_LOG_DEBUG, "%s: thread %d shutting down\n",
		__FUNCTION__, info->no);
#endif
	gr_shutdown(&info->queue);

	pthread_exit(NULL);
}

static void*
TestReplicateMany_client_thread(void* arg)
{
	int i;
	int ret;
	gr_cond_t* cond = NULL;
	TestReplicateMany_info_t* info = arg;
	char keybuf[32];
	char valuebuf[32];

	// fprintf(stderr, "%s: client %d starts\n", __FUNCTION__, info->no);
	for (i = 0; i < message_count; ++i) {
		sprintf(keybuf, "key-%02d-%04d", info->no, i);
		sprintf(valuebuf, "value-%02d-%04d", info->no, i);

		cond = gr_replicate(info->queue, keybuf, valuebuf, -1);
		if (!cond)
			continue;
		do {
			ret = gr_cond_wait(cond, 10 * 1000);
			if ((ret == ETIMEDOUT) || // helgrind bug
				((ret < 0) && (errno == ETIMEDOUT))) {
				gr_log(GR_LOG_DEBUG, "%s:%d: replicas are slow, cond at %p\n",
					__FUNCTION__, __LINE__, cond);
			}
		} while ((ret == ETIMEDOUT) || ((ret < 0) && (errno == ETIMEDOUT)));
		gr_log(GR_LOG_DEBUG, "%s:%d: unlock cond at %p\n",
			__FUNCTION__, __LINE__, cond);
		gr_cond_unlock(cond);
		gr_log(GR_LOG_DEBUG, "%s:%d: free cond at %p\n", __FUNCTION__, __LINE__, cond);
		gr_cond_free(cond);

		cond = gr_clear(info->queue, keybuf);
		do {
			ret = gr_cond_wait(cond, 10 * 1000);
			if ((ret == ETIMEDOUT) || // helgrind bug
				((ret < 0) && (errno == ETIMEDOUT))) {
				gr_log(GR_LOG_DEBUG, "%s:%d: replicas are slow, cond at %p\n",
					__FUNCTION__, __LINE__, cond);
			}
		} while ((ret == ETIMEDOUT) || ((ret < 0) && (errno == ETIMEDOUT)));
		gr_log(GR_LOG_DEBUG, "%s:%d: unlock cond at %p\n", __FUNCTION__, __LINE__, cond);
		gr_cond_unlock(cond);
		gr_log(GR_LOG_DEBUG, "%s:%d: unlocked cond at %p\n",
			__FUNCTION__, __LINE__, cond);
		gr_cond_free(cond);
		gr_log(GR_LOG_DEBUG, "%s:%d: free cond at %p\n", __FUNCTION__, __LINE__, cond);
	}

	sleep(1);

	gr_cond_signal(info->stop_cond);
//	fprintf(stderr, "%s: client %d stops\n", __FUNCTION__, info->no);

	pthread_exit(NULL);
}

int
main(int argc, char* argv[])
{
	gr_log_level_t loglevel = GR_LOG_INFO;
	int i;
	int ret;
	int c;
	int option_index = 0;
	pthread_t* threads;
	pthread_t* clients;
	gr_cond_t* start_cond;
	TestReplicateMany_info_t* info;
	TestReplicateMany_info_t* client_info;
	pthread_attr_t attr;
	struct timeval start[1];
	struct timeval stop[1];
	struct timeval elapsed[1];

	for (;;) {
		static struct option long_options[] = {
			{"clients",  1, 0, 'c'},
			{"messages", 1, 0, 'm'},
			{"nodes",    1, 0, 'n'},
			{"debug",    0, 0, 'd'},
			{"quiet",    0, 0, 'q'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "c:dm:n:q", long_options, &option_index);
		if (c == -1) break;
		switch (c) {
			case 0:
#if 0
				switch (option_index) {
					case 0:
						fprintf(stderr, "%s:%d: %s messages\n", argv[0], __LINE__, optarg);
						message_count = atoi(optarg);
						break;
				}
				break;
#endif

			case 'c':
//				fprintf(stderr, "%s:%d: %s clients\n", argv[0], __LINE__, optarg);
				client_count = atoi(optarg);
				break;

			case 'd':
				loglevel = GR_LOG_DEBUG;
				break;

			case 'q':
				loglevel = GR_LOG_NOTHING;
				break;

			case 'm':
//				fprintf(stderr, "%s:%d: %s messages\n", argv[0], __LINE__, optarg);
				message_count = atoi(optarg);
				break;

			case 'n':
//				fprintf(stderr, "%s:%d: %s nodes\n", argv[0], __LINE__, optarg);
				node_count = atoi(optarg);
				break;

			default:
				fprintf(stderr, "getopt_long returned %d / %c\n", c, c);
		}
	}

	gr_log_level(loglevel);
	threads = malloc(node_count * sizeof(threads[0]));
	info = malloc(node_count * sizeof(info[0]));

	zsys_init();

	pthread_attr_init(&attr);
	pthread_attr_setstacksize(&attr, 256 * 1024);
	start_cond = gr_cond_create();
	gr_cond_lock(start_cond, node_count);
	for (i = 0; i < node_count; ++i) {
		info[i].no = i;
		info[i].start_cond = start_cond;
		info[i].stop_cond = gr_cond_create();
		ret = pthread_create(&threads[i], &attr, TestReplicateMany_thread, &info[i]);
		(void) ret;
	}
//	fprintf(stderr, "%s: created %d threads\n", __FUNCTION__, node_count);
	gr_cond_wait(start_cond, 0);
	gr_cond_unlock(start_cond);
	fprintf(stderr, "%s: all %d threads running\n", __FUNCTION__, node_count);

	sleep(1);

	clients = malloc(client_count * sizeof(clients[0]));
	client_info = malloc(client_count * sizeof(client_info[0]));

	for (i = 0; i < client_count; ++i) {
		client_info[i].no = i;
		client_info[i].queue = info[i % node_count].queue;
		client_info[i].stop_cond = gr_cond_create();
		gr_cond_lock(client_info[i].stop_cond, 1);
		ret = pthread_create(&clients[i], &attr, TestReplicateMany_client_thread, &client_info[i]);
	}
	fprintf(stderr, "%s: %d client threads started\n", __FUNCTION__, client_count);

	gettimeofday(start, NULL);

	sleep(1);

	for (i = 0; i < client_count; ++i) {
//		fprintf(stderr, "%s: wait for client %d to stop\n", __FUNCTION__, i);
		gr_cond_wait(client_info[i].stop_cond, 0);
		gr_cond_unlock(client_info[i].stop_cond);
		ret = pthread_join(clients[i], NULL);
		gr_cond_free(client_info[i].stop_cond);
	}
	fprintf(stderr, "%s: all clients stopped\n", __FUNCTION__);

	gettimeofday(stop, NULL);
	timersub(stop, start, elapsed);
	elapsed->tv_sec -= 1;	// the sleep at the end of the client thread
	if (elapsed->tv_sec < 1)
		elapsed->tv_sec = 1;

	fprintf(stderr, "%s: spent %d seconds - about %d mps\n",
		argv[0],
		(int) elapsed->tv_sec,
		(client_count * message_count) / (int) elapsed->tv_sec);

	for (i = 0; i < node_count; ++i) {
		gr_cond_signal(info[i].stop_cond);
	}
	for (i = 0; i < node_count; ++i) {
		pthread_join(threads[i], NULL);
		gr_cond_free(info[i].stop_cond);
	}
//	fprintf(stderr, "%s: all threads stopped\n", __FUNCTION__);

	gr_cond_free(start_cond);

	free(threads);
	free(info);
	free(clients);
	free(client_info);

	exit(0);
}

